(function(){

     'use strict'
     
  function splasheditorCtrl($cookies,Fabric, FabricConstants, Keypress, $scope,splashpagesService,$stateParams,$state ){
         
	var ctrl = this;
	ctrl.user_data = $cookies.getObject('usr_dta');
	$scope.dynamicPopover = {
	content: 'Hello, World!',
	templateUrl: 'myPopoverTemplate.html',
	title: 'Title'
	};
	$scope.fabric = {};
	$scope.FabricConstants = FabricConstants;
	$scope.image_src = "app/assets/image-preview.png";
	$scope.texto_panel = "Navega gratis aqui";
	$scope.scopeVariable = {};
	$scope.options = 1;
	$scope.panel_text_size = 16;
	$scope.tipo_registro_facebook = false;
	$scope.tipo_registro_formulario = true;

	$scope.scopeVariable.options = {
		label: "Elige el color",
		icon: "brush",
		genericPalette: false,
		history: false
	};
	$scope.scopeVariable.color ="#9E271E";

	$scope.scopeVariable2 = {};
	$scope.scopeVariable2.options = {
		label: "Elige el color",
		icon: "brush",
		color: "#ffffff",
		default: "rgb(255,255,255)"	,
		genericPalette: false,
		history: false,
		hsl:false,
		hex:false

	};
	$scope.scopeVariable2.color ="rgb(255,255,255)";

	$scope.scopeVariable3 = {};
	$scope.scopeVariable3.options = {
		label: "Elige el color",
		icon: "brush",
		color: "#ffffff",
		default: "rgb(255,255,255)"	,
		genericPalette: false,
		history: false,
		hsl:false,
		hex:false

	};
	$scope.scopeVariable3.color ="#1B492E";

	$scope.scopeVariable4 = {};
	$scope.scopeVariable4.options = {
		label: "Elige el color",
		icon: "brush",
		color: "#ffffff",
		default: "rgb(255,255,255)"	,
		genericPalette: false,
		history: false,
		hsl:false,
		hex:false

	};
	$scope.scopeVariable4.color ="rgb(255,255,255)";

	$scope.scopeVariable5 = {};
	$scope.scopeVariable5.options = {
		label: "Elige el color",
		icon: "brush",
		color: "#ffffff",
		default: "rgb(255,255,255)"	,
		genericPalette: false,
		history: false,
		hsl:false,
		hex:false

	};
	$scope.scopeVariable5.color ="rgb(255,255,255)";


	$scope.scopeVariable6 = {};
	$scope.scopeVariable6.options = {
		label: "Elige el color",
		icon: "brush",
		color: "#ffffff",
		default: "rgb(255,255,255)"	,
		genericPalette: false,
		history: false,
		hsl:false,
		hex:false

	};
	$scope.scopeVariable6.color ="rgb(255,255,255)";

	$scope.slider = {
	value: 14,
	options: {
		floor: 8,
		ceil: 18,
	},
	}
	$scope.tipo_registro = 'facebook';

	$scope.imprimir = function(as){
		$scope.tipo_registro = as;
		console.log($scope.tipo_registro);
	}
	//
	// Creating Canvas Objects
	// ================================================================
	
      
    $scope.addImage = function(image) {
		$scope.selectedImageUrl = $('#preview-image').attr('src');
		$scope.fabric.addImage($scope.selectedImageUrl);
		$('#btn-agregar').hide();
	};

	$scope.remove = function(){
		$scope.fabric.deleteActiveObject(); 
		$scope.fabric.setDirty(true);
	}

      
      $scope.file_changed = function(input, $scope) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                 reader.onload = function (e) {
					 $('#preview-image').attr('src',  e.target.result );
					 $('#btn-agregar').show();
                     $('#preview-image:not([src=""])').show();
                  };
                 reader.readAsDataURL(input.files[0]);

            
                }
                
        } ;

	//
	// Editing Canvas Size
	// ================================================================
	$scope.selectCanvas = function() {
		$scope.canvasCopy = {
			width: $scope.fabric.canvasOriginalWidth,
			height: $scope.fabric.canvasOriginalHeight
		};
	};

	$scope.setCanvasSize = function() {
		$scope.fabric.setCanvasSize(246, $scope.canvasCopy.height);
		$scope.fabric.setDirty(true);
		delete $scope.canvasCopy;
	};

	ctrl.obtener = function(){
		if($stateParams.id_splash != null){
			splashpagesService.obtenerSplashpage()
			.then(function(response){
				ctrl.splashpage = response.data.splash;
				$scope.fabric.loadJSON(ctrl.splashpage.configuracion.imagen.json);
				$scope.scopeVariable.color = ctrl.splashpage.configuracion.panel.color_fondo;
				$scope.scopeVariable2.color = ctrl.splashpage.configuracion.panel.color_texto;
				$scope.scopeVariable3.color = ctrl.splashpage.configuracion.footer.color_fondo;
				$scope.scopeVariable4.color = ctrl.splashpage.configuracion.footer.color_texto;
				$scope.scopeVariable5.color = ctrl.splashpage.configuracion.footer.color_texto2;
				console.log(ctrl.splashpage.configuracion.footer.color_texto2);
			
			}).catch(function(response){   
				toastr.options = {
					"positionClass": "toast-top-right",
					"showDuration": "200",
				};
				toastr.error(response.data.message, "");
			});
		}else{
			ctrl.splashpage =
			{
				"configuracion" : {
					"footer" : {
						"color_fondo" : "#F66",
						"color_texto" : "#444",
						"color_texto2" : "#0FF"
					},
					"panel" : {
						"color_fondo" : "#F55",
						"color_texto" : "#333"
					},
					"imagen" : {
						"json" : {
							"a" : "b",
							"b" : "c"
						},
						"imagen" : "imagen"
					}
				},
				"redes_sociales" : {
					"facebook" : true,
					"mail" : false
				},
				"usuario" : "",
				"nombre" : "",
				"url" : ""
			}
		}

	

	}

	ctrl.obtener();

	ctrl.guardar = function(){
		
		if(ctrl.splashpage.nombre == "" || ctrl.splashpage.url == ""){
			toastr.options = {
				"positionClass": "toast-top-right",
				"showDuration": "200",
			};
			toastr.error("El nombre y la url de redireccionamiento son obligatorios", "");
		}else{

			if($stateParams.id_splash != null){
				ctrl.splashpage.configuracion.panel.color_fondo = $scope.scopeVariable.color;
				ctrl.splashpage.configuracion.panel.color_texto = $scope.scopeVariable2.color;
				ctrl.splashpage.configuracion.footer.color_fondo = $scope.scopeVariable3.color;
				ctrl.splashpage.configuracion.footer.color_texto = $scope.scopeVariable4.color;
				ctrl.splashpage.configuracion.footer.color_texto2 = $scope.scopeVariable5.color;
				ctrl.splashpage.configuracion.imagen.json = $scope.fabric.getJSON();
				$scope.fabric.deactivateAll();
				$scope.fabric.scaleExport();
				var canvas = document.getElementById($scope.fabric.canvasId);
				ctrl.splashpage.configuracion.imagen.imagen = canvas.toDataURL();
				var splashpage = ctrl.splashpage;
				splashpagesService.editarSplashpage(splashpage)
				.then(function(){
					toastr.options = {
						"positionClass": "toast-top-right",
						"showDuration": "200",
					};
					toastr.success("El splashpage ha sido actualizado correctamente", "");
					//$state.go('splashpages');

				}).catch(function(response){   
					toastr.options = {
						"positionClass": "toast-top-right",
						"showDuration": "200",
					};
					toastr.error(response.data.message, "");
				});
			}else{
				ctrl.splashpage.configuracion.panel.color_fondo = $scope.scopeVariable.color;
				ctrl.splashpage.configuracion.panel.color_texto = $scope.scopeVariable2.color;
				ctrl.splashpage.configuracion.footer.color_fondo = $scope.scopeVariable3.color;
				ctrl.splashpage.configuracion.footer.color_texto = $scope.scopeVariable4.color;
				ctrl.splashpage.configuracion.footer.color_texto2 = $scope.scopeVariable5.color;
				ctrl.splashpage.configuracion.imagen.json = $scope.fabric.getJSON();
				ctrl.splashpage.usuario = $stateParams.id_cliente;
				$scope.fabric.deactivateAll();
				$scope.fabric.scaleExport();
				var canvas = document.getElementById($scope.fabric.canvasId);
				ctrl.splashpage.configuracion.imagen.imagen = canvas.toDataURL();
				var splashpage = ctrl.splashpage;
				splashpagesService.agregarSplashpage(splashpage)
				.then(function(){
					toastr.options = {
						"positionClass": "toast-top-right",
						"showDuration": "200",
					};
					toastr.success("El splashpage ha sido agregado correctamente", "");
					$state.go('splashpages');
					

				}).catch(function(response){   
					toastr.options = {
						"positionClass": "toast-top-right",
						"showDuration": "200",
					};
					toastr.error(response.data.message, "");
				});
			}
		}
	}
	


	//
	// Init
	// ================================================================
	$scope.init = function() {

		$('#btn-agregar').hide();
		$scope.fabric = new Fabric({
			JSONExportProperties: FabricConstants.JSONExportProperties,
			textDefaults: FabricConstants.textDefaults,
			shapeDefaults: FabricConstants.shapeDefaults,
			json: {}
		});

		
	};

	$scope.$on('canvas:created', $scope.init);

	Keypress.onSave(function() {
		$scope.updatePage();
      });
      
   
  }
        
    angular
        .module('hicomunity.panel')
        .controller('splasheditorCtrl',splasheditorCtrl);

 }());
 