(function(){

     'use strict'

     function requestService(){
         
       var pending = [];

        this.add = function(request){
            pending.push(request);
        }

        this.remove = function(request) {
            pending = pending.filter(function(p) {
              return p.url !== request;
            });
          };

        this.cancelAll = function(){
            angular.forEach(pending, function (p) {
                console.log(p.canceller);
                console.log(p.canceller.promise);
                //p.canceller.promise.cancel.resolve();
            })
            pending.length = 0;
        }

     }

     angular
        .module('hicomunity.panel')
        .service('requestService', requestService);

 }());