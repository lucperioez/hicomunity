(function(){

     'use strict'

     function indexService(config,$http,$cookies,requestService,$q){
         

        this.getMenu = function(type){
            if(type == '1'){
                type = "admin";
            }else if(type == '2'){
                type = "agencia";
            }else if(type == '3'){
                type = "cliente";
            }
            var url = 'app/menu/' + type + '.json';

            return $http({
                    method: 'GET',
                    url: url,
                    headers: {'Content-Type': 'application/json'}
                });
        }

     }

     angular
        .module('hicomunity.panel')
        .service('indexService', indexService);

 }());