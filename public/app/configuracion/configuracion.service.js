(function(){

    'use strict'

    function configuracionService(config,$http,$cookies,$q){
        

       /*this.get = function(){
           var token = $cookies.get('tkn')   
           var url = '/users';
           return $http({
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'token' : token
            }    
            });
       }*/

        this.update = function(user){
            var token = $cookies.get('tkn')   
            var url = '/users/'+user._id;

            return $http({
                method: 'PUT',
                url: url,
                data : user,
                headers: {
                    'Content-Type': 'application/json',
                    'token' : token
                }
            });
        }

        this.changePassword = function(user){
            var token = $cookies.get('tkn')   
            var url = '/users/cambiarPassword/'+user._id;

            return $http({
                method: 'PUT',
                url: url,
                data : user,
                headers: {
                    'Content-Type': 'application/json',
                    'token' : token
                }
            });
        }

    }

    angular
       .module('hicomunity.panel')
       .service('configuracionService', configuracionService);

}());