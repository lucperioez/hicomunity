var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var passport = require('passport');
var auth= require('./config/passport');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var encuestasRouter = require('./routes/encuestas');
var splashpagesRouter = require('./routes/splashpages');
var modemsRouter = require('./routes/modems');
var impactosRouter = require('./routes/impactos');
var conexionesRouter = require('./routes/conexiones');
var leadsRouter = require('./routes/leads');
var campainRouter = require('./routes/campain');
var blockedMacsRouter = require('./routes/blockedMacs');
var templatesRouter = require('./routes/templates');

var app = express();

app.use(logger('short'));
app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({ extended: false ,limit: '50mb'}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use((req, res, next) => {
	res.header("Access-Control-Allow-Origin", "*");    
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");    
	next();
});


  
app.use('/', indexRouter);
app.use('/users',auth.authenticationAPI ,usersRouter);
app.use('/encuesta',encuestasRouter);
app.use('/splashpage',auth.authenticationAPI , splashpagesRouter);
app.use('/modem',auth.authenticationAPI , modemsRouter);
app.use('/impacto',impactosRouter);
app.use('/conexion',conexionesRouter);
app.use('/lead',leadsRouter);
app.use('/campain',campainRouter);
app.use('/blocked',blockedMacsRouter);
app.use('/template',auth.authenticationAPI, templatesRouter);

module.exports = app;
