var express = require('express');
var router = express.Router();
const db = require('../config/db');
require("../models/M_Modems");
const mongoose = require('mongoose');
mongoose.connect(db.getConection(), { useNewUrlParser: true });
const Modems = mongoose.model("Modems");


router.get('/:id',function(req, res, next) {
    Modems.findById(req.params.id).populate('user','-password').exec(function(err,modem){
        if(err){
            res.json({succes : false,mensaje:'A ocurrido un error',error:err});
        }else{
            if(modem){
                res.json({succes : true,mensaje:'Modem encontrado',modem});
            }else{
                res.json({succes : false,mensaje:'El Modem no se ha encontrado'});
            } 
        }
    });
});

router.post('/',function(req,res,next){
  let nModem = new Modems(req.body);
  nModem.save(function(error,modem){
    if(error){
      res.json({succes : false,mensaje:'A ocurrido un error',error});
    }else{
      res.json({succes : true,mensaje:'Modem creado con exito',modem});
    }
  });
});

router.put('/:id',function(req,res,next){
    Modems.update({ _id: req.params.id }, { $set: req.body },function(err,result){
        if(err){
          res.json({success: false, message:'An error has occurred',error:err});
        }else{
          res.json({success: true, message:'Modem actualizado con exito',result});
        } 
    });
});

router.delete('/:id',function(req,res,next){
    Modems.deleteOne({ _id: req.params.id }, function (err) {
        if(err){
            res.json({success: false, message:'A ocurrido con error', error:err});
        }else{
            res.json({success: true, message:'Modem eliminado con exito'});
        } 
    });
});

module.exports = router;
