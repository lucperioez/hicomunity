(function(){

     'use strict'
     
  function calendarCtrl(calendarService,$cookies,usuariosService,$state,panelService){
         
    var ctrl = this;
    ctrl.user_data = $cookies.getObject('usr_dta');
    toastr.options = {
        "positionClass": "toast-top-right",
        "showDuration": "200",
    };
    ctrl.evento = "Nuevo";

    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right:''
        },
        lang: 'es',
        buttonText: {

            today: 'hoy',
            week: 'week',
            day: 'day'
        },
        //Random events
        /*events: [{
            title: "Nuevo evento",
            start: "08/21/2018 14:55",
            end: "08/30/2018 14:55",
            backgroundColor: "#6f66ff",
            id:1
        }],*/
        

        editable: true,
        droppable: false, // this allows things to be dropped onto the calendar !!!
        eventClick: function(event,date) {
            abrirModal(event.title);
            ctrl.id_campaing = event.id;
        }
    });

    function abrirModal(title){
        ctrl.evento = title;
        console.log(ctrl.evento);
        $("#m_modal_1").modal(); 
        
    }

    ////////////////
    ctrl.agencias = [];
    ctrl.clientes = [];
    ctrl.usuarios = [];
    ctrl.agencia = '';
    ctrl.cliente = '';
    ctrl.modems_cliente = [];
    ctrl.plaza = '';
    ctrl.admin = ctrl.user_data.tipo == 1;
    
      if(ctrl.user_data.tipo == 3){
          //obtener mis campañas
          /*calendarService.getCampains(ctrl.user_data._id).then(function(response){
            console.log(response);
            if(response.data.succes){
                let campains = [];
                response.data.campains.forEach((campain,index) => {
                    campains.push({
                        title: campain.nombre,
                        start: campain.fecha_inicio,
                        end: campain.fecha_fin,
                        backgroundColor: "#6f66ff",
                        id : campain._id
                    });
                });
                $('#calendar').fullCalendar('removeEvents');
                $('#calendar').fullCalendar('addEventSource', campains );
                console.log(campains);
            }*/
            panelService.getPlazas(ctrl.user_data._id).then(function(response){
                console.log(response);
                ctrl.modems_cliente = response.data.modems;
          }).catch(function(response){   
                toastr.options = {
                "positionClass": "toast-top-right",
                "showDuration": "200",
                };
                toastr.error(response.data.message, "");
          });
      }else{
        usuariosService.get().then(function(response){
            console.log(response);
            let data = response.data.users;
            ctrl.usuarios = data;
          if(ctrl.user_data.tipo == 2){
                ctrl.clientes = data;
          }else{  
                ctrl.agencias = data.filter((user)=>user.tipo==2);
          }
        }).catch(function(response){   
            toastr.options = {
            "positionClass": "toast-top-right",
            "showDuration": "200",
            };
            toastr.error(response.data.message, "");
        });
    }

    ctrl.getCliente = function(){
        if(ctrl.agencia != ''){
            ctrl.clientes = ctrl.usuarios.filter((user)=> (user.tipo==3) && (user.agencia._id==ctrl.agencia) );
        }else{
            ctrl.cliente = '';
            ctrl.cliente_nombre = '';
            ctrl.clientes = [];
        }
        
    }
  
    ctrl.getPlazas = function(){
        if(ctrl.cliente == ''){
              ctrl.modems_cliente = [];
        }else{
              panelService.getPlazas(ctrl.cliente).then(function(response){
                    console.log(response);
                    ctrl.modems_cliente = response.data.modems;
              }).catch(function(response){   
                    toastr.options = {
                    "positionClass": "toast-top-right",
                    "showDuration": "200",
                    };
                    toastr.error(response.data.message, "");
              });
        }
    }
    ctrl.getCampains = function(){
        /*calendarService.getCampains(ctrl.cliente).then(function(response){
            console.log(response);
            if(response.data.succes){
                let campains = [];
                response.data.campains.forEach((campain,index) => {
                    campains.push({
                        title: campain.nombre,
                        start: campain.fecha_inicio,
                        end: campain.fecha_fin,
                        backgroundColor: "#6f66ff",
                        id : campain._id
                    });
                });
                $('#calendar').fullCalendar('removeEvents');
                $('#calendar').fullCalendar('addEventSource', campains );
                console.log(campains);
            }
        }).catch(function(response){
            console.log(response);
        });*/
        if(ctrl.user_data.tipo == 3){
            calendarService.getCampainsModems(ctrl.user_data._id,ctrl.plaza).then(function(response){
                console.log(response);
                if(response.data.succes){
                    let campains = [];
                    response.data.campains.forEach((campain,index) => {
                        campains.push({
                            title: campain.nombre,
                            start: campain.fecha_inicio,
                            end: campain.fecha_fin,
                            backgroundColor: "#6f66ff",
                            id : campain._id
                        });
                    });
                    $('#calendar').fullCalendar('removeEvents');
                    $('#calendar').fullCalendar('addEventSource', campains );
                    console.log(campains);
                }
            }).catch(function(response){
                console.log(response);
            });
        }else{
            calendarService.getCampainsModems(ctrl.cliente,ctrl.plaza).then(function(response){
                console.log(response);
                if(response.data.succes){
                    let campains = [];
                    response.data.campains.forEach((campain,index) => {
                        campains.push({
                            title: campain.nombre,
                            start: campain.fecha_inicio,
                            end: campain.fecha_fin,
                            backgroundColor: "#6f66ff",
                            id : campain._id
                        });
                    });
                    $('#calendar').fullCalendar('removeEvents');
                    $('#calendar').fullCalendar('addEventSource', campains );
                    console.log(campains);
                }
            }).catch(function(response){
                console.log(response);
            });
        }
    }
    
    ctrl.delete = function (){
        calendarService.deleteCampain(ctrl.id_campaing).then(function(response){
            console.log(response);
            if(response.data.success){
                toastr.success(response.data.message, "");
                ctrl.getCampains();
                ctrl.id_campaing = "";
            }else{
                toastr.error(response.data.message, "");
            }
        }).catch(function(response){
            console.log(response);
            toastr.error(response.data.message, "");
        });
    }

    ctrl.editar = function(){
        $state.go('campain',{ id_campain : ctrl.id_campaing, id_cliente : ctrl.cliente, modem : null});

    }
  }
        
    angular
        .module('hicomunity.panel')
        .controller('calendarCtrl',calendarCtrl)
 }());
