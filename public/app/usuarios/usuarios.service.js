(function(){

    'use strict'

    function usuariosService(config,$http,$cookies,$q){
        

       this.get = function(){
           var token = $cookies.get('tkn')   
           var url = '/users';

           return $http({
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'token' : token
            }    
            });
       }

       this.getUsuario = function(id){
        var token = $cookies.get('tkn')   
        var url = '/users/'+id;

        return $http({
         method: 'GET',
         url: url,
         headers: {
             'Content-Type': 'application/json',
             'token' : token
         }    
         });
        }

       this.add = function(user){
        
            var token = $cookies.get('tkn')   
            var url = '/users';

            return $http({
                method: 'POST',
                url: url,
                data : user,
                headers: {
                    'Content-Type': 'application/json',
                    'token' : token
                }
            });
        }

        this.update = function(user){
        
            var token = $cookies.get('tkn')   
            var url = '/users/'+user._id;

            return $http({
                method: 'PUT',
                url: url,
                data : user,
                headers: {
                    'Content-Type': 'application/json',
                    'token' : token
                }
            });
        }

        this.delete = function(user){
        
            var token = $cookies.get('tkn')   
            var url = '/users/'+user._id;

            return $http({
                method: 'DELETE',
                url: url,
                headers: {
                    'Content-Type': 'application/json',
                    'token' : token
                }
            });
        }
    }

    angular
       .module('hicomunity.panel')
       .service('usuariosService', usuariosService);

}());