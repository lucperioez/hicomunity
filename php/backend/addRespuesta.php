<?php

$data = json_encode($_POST);
$idRespuesta = $_POST['idRespuesta'];
$curl = curl_init();
curl_setopt_array($curl, array(
  CURLOPT_URL => "http://54.193.66.241:3000/encuesta/$idRespuesta/respuesta",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => $data,
  CURLOPT_HTTPHEADER => array(
    "Cache-Control: no-cache",
    "Content-Type: application/json",
    "Access-Control-Allow-Origin : *",
    "Access-Control-Allow-Headers : X-Requested-With",
    "Access-Control-Allow-Methods : GET, POST,PUT"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}
?>
