(function(){

     'use strict'
     
  function panelCtrl($cookies,panelService,usuariosService){
         
      var ctrl = this;
      ctrl.user_data = $cookies.getObject('usr_dta');
      ctrl.agencias = [];
      ctrl.clientes = [];
      ctrl.usuarios = [];
      ctrl.modems_clientes = [];
      ctrl.agencia = '';
      ctrl.cliente = '';
      ctrl.plaza = '';
      ctrl.admin = ctrl.user_data.tipo == 1;
      ctrl.impactos = 0;
      ctrl.conexiones = 0;
      ctrl.leads = 0;
      ctrl.encuestas = 0;
      ctrl.graficas_data = {};

      usuariosService.get().then(function(response){
            console.log(response);
            let data = response.data.users;
            ctrl.usuarios = data;
            if(ctrl.user_data.tipo == 2){
                ctrl.clientes = data;
            }else{  
                ctrl.agencias = data.filter((user)=>user.tipo==2);
            }
        }).catch(function(response){   
            toastr.options = {
            "positionClass": "toast-top-right",
            "showDuration": "200",
            };
            toastr.error(response.data.message, "");
        });
    
        ctrl.getCliente = function(){
            if(ctrl.agencia != ''){
                ctrl.clientes = ctrl.usuarios.filter((user)=> (user.tipo==3) && (user.agencia._id==ctrl.agencia) );
            }else{
                ctrl.cliente = '';
                ctrl.cliente_nombre = '';
                ctrl.clientes = [];
                ctrl.modems_clientes = [];
            }
            
        }
        ctrl.getPlazas = function(){
            if(ctrl.cliente == ''){
                  ctrl.modems_cliente = [];
            }else{
                  panelService.getPlazas(ctrl.cliente).then(function(response){
                        console.log(response);
                        ctrl.modems_cliente = response.data.modems;
                  }).catch(function(response){   
                        toastr.options = {
                        "positionClass": "toast-top-right",
                        "showDuration": "200",
                        };
                        toastr.error(response.data.message, "");
                  });
            }
            
        }
      ctrl.getData = function(){
            if(ctrl.agencia != '' && ctrl.cliente == ''){
                  panelService.getDataAgencia(ctrl.agencia).then(function(response){
                        console.log(response);
                        ctrl.impactos = response.data.impactos;
                        ctrl.conexiones = response.data.conexiones;
                        ctrl.leads = response.data.leads;
                        ctrl.encuestas = response.data.encuestas;
                  }).catch(function(response){   
                        toastr.options = {
                        "positionClass": "toast-top-right",
                        "showDuration": "200",
                        };
                        toastr.error(response.data.message, "");
                  });
                  panelService.getGraficasAgencia(ctrl.agencia).then(function(response){
                        console.log(response);
                        ctrl.graficas_data = response.data;
                        ctrl.charts();
                  }).catch(function(response){   
                        toastr.options = {
                        "positionClass": "toast-top-right",
                        "showDuration": "200",
                        };
                        toastr.error(response.data.message, "");
                  });
            }else if(ctrl.agencia != '' && ctrl.cliente != '' && ctrl.plaza == ''){
                  panelService.getDataCliente(ctrl.cliente).then(function(response){
                        console.log(response);
                        ctrl.impactos = response.data.impactos;
                        ctrl.conexiones = response.data.conexiones;
                        ctrl.leads = response.data.leads;
                        ctrl.encuestas = response.data.encuestas;
                  }).catch(function(response){   
                        toastr.options = {
                        "positionClass": "toast-top-right",
                        "showDuration": "200",
                        };
                        toastr.error(response.data.message, "");
                  });
                  panelService.getGraficasCliente(ctrl.cliente).then(function(response){
                        console.log(response);
                        ctrl.graficas_data = response.data;
                        ctrl.charts();
                  }).catch(function(response){   
                        toastr.options = {
                        "positionClass": "toast-top-right",
                        "showDuration": "200",
                        };
                        toastr.error(response.data.message, "");
                  });
            }else if(ctrl.agencia != '' && ctrl.cliente != '' && ctrl.plaza != ''){
                  panelService.getDataClienteModem(ctrl.cliente,ctrl.plaza).then(function(response){
                        console.log(response);
                        ctrl.impactos = response.data.impactos;
                        ctrl.conexiones = response.data.conexiones;
                        ctrl.leads = response.data.leads;
                        ctrl.encuestas = response.data.encuestas;
                  }).catch(function(response){   
                        toastr.options = {
                        "positionClass": "toast-top-right",
                        "showDuration": "200",
                        };
                        toastr.error(response.data.message, "");
                  });
                  panelService.getGraficasCliente(ctrl.cliente).then(function(response){
                        console.log(response);
                        ctrl.graficas_data = response.data;
                        ctrl.charts();
                  }).catch(function(response){   
                        toastr.options = {
                        "positionClass": "toast-top-right",
                        "showDuration": "200",
                        };
                        toastr.error(response.data.message, "");
                  });
            }
      }

      ctrl.charts = function(){
            // Make monochrome colors
            var pieColors = ['#2980b9','#8e44ad','#e8eaf6'];
                  
            // Build the chart
            Highcharts.chart('container', {
                  chart: {
                  plotBackgroundColor: null,
                  plotBorderWidth: null,
                  plotShadow: false,
                  type: 'pie'
                  },
                  title: {
                  text: 'GENÉRO',
                  style: {
                        display: 'none'
                  }
                  },
                  tooltip: {
                  pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                  },
                  plotOptions: {
                  pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        colors: pieColors,
                        dataLabels: {
                              enabled: true,
                              format: '<b>{point.name}</b><br>{point.percentage:.1f} %',
                              distance: -50,
                              filter: {
                              property: 'percentage',
                              operator: '>',
                              value: 4
                              }
                        }
                  }
                  },
                  series: [{
                  name: 'Share',
                  data: [
                        { name: 'Hombres', y: ctrl.graficas_data.genero.masculino || 0 },
                        { name: 'Mujeres', y: ctrl.graficas_data.genero.femenino || 0 }
                  ]
                  }]
            });


            // Build the chart
            Highcharts.chart('container2', {
                  chart: {
                  plotBackgroundColor: null,
                  plotBorderWidth: null,
                  plotShadow: false,
                  type: 'pie'
                  },
                  title: {
                  text: 'GENÉRO',
                  style: {
                        display: 'none'
                  }
                  },
                  tooltip: {
                  pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                  },
                  plotOptions: {
                  pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        colors: ['#16a085','#27ae60','#bdc3c7','#8e44ad','#e67e22'],
                        dataLabels: {
                              enabled: true,
                              format: '<b>{point.name}</b><br>{point.percentage:.1f} %',
                              distance: -50,
                              filter: {
                              property: 'percentage',
                              operator: '>',
                              value: 4
                              }
                        }
                  }
                  },
                  series: [{
                  name: 'Share',
                  data: [
                        { name: '-18', y:  ctrl.graficas_data.edad['0-18'] || 0 },
                        { name: '19 - 25', y: ctrl.graficas_data.edad['19-25'] || 0 },
                        { name: '26 - 45', y: ctrl.graficas_data.edad['26-45'] || 0 },
                        { name: '46+', y: ctrl.graficas_data.edad['46+'] || 0 },
                  ]
                  }]
            });

            // Build the chart
            Highcharts.chart('container3', {
                  chart: {
                  plotBackgroundColor: null,
                  plotBorderWidth: null,
                  plotShadow: false,
                  type: 'pie'
                  },
                  title: {
                  text: 'GENÉRO',
                  style: {
                        display: 'none'
                        }
                  },
                  tooltip: {
                  pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                  },
                  plotOptions: {
                  pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        colors: ['#16a085','#27ae60','#bdc3c7','#8e44ad','#e67e22'],
                        dataLabels: {
                              enabled: true,
                              format: '<b>{point.name}</b><br>{point.percentage:.1f} %',
                              distance: -50,
                              filter: {
                              property: 'percentage',
                              operator: '>',
                              value: 4
                              }
                        }
                  }
                  },
                  series: [{
                  name: 'Share',
                  data: [
                        { name: '1 Conexion', y: 45 },
                        { name: '1 - 10', y: 20 },
                        { name: '11 - 30', y: 10},
                        { name: '30+', y: 25},
                  ]
                  }]
            });
      }
      
 
         
 

           



  }
        
    angular
        .module('hicomunity.panel')
        .controller('panelCtrl',panelCtrl)
 }());
 