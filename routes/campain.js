var express = require('express');
var router = express.Router();
const db = require('../config/db');
require("../models/M_Campain");
require("../models/M_Encuestas");
require("../models/M_Leads");
const mongoose = require('mongoose');
mongoose.connect(db.getConection(), { useNewUrlParser: true });
const Campain = mongoose.model("Campain");
const Leads = mongoose.model("Leads");
const Encuesta = mongoose.model("Encuestas");
var auth= require('../config/passport');

router.get('/:modem/:idCliente/:mac',function(req, res, next) {
    let query = {
        folio_modem : req.params.modem,
        cliente :
         req.params.idCliente,
        fecha_inicio : { 
            $lte : new Date()
        },
        fecha_fin : { 
            $gte : new Date()
        },
    }
    Campain.findOne(query).populate('splash_actual').populate('encuesta_actual').exec(async function(err,campain){
        if(err){
            res.json({succes : false,mensaje:'A ocurrido un error',error:err});
        }else{
            if(campain){
                let lead = await Leads.findOne({mac:req.params.mac}).exec();
                let is_lead = (lead) ? true : false;
                let telefono_validado = (lead) ? lead.telefono_validado : false;
                let encuesta_respondida = false;
                if(campain.encuesta_actual){
                    for (const resEnc of campain.encuesta_actual.respuestas) {
                        if(resEnc.mac == req.params.mac){
                            encuesta_respondida = true;
                            break;
                        }   
                    }
                }
                res.json({succes : true,mensaje:'Campaña encontrada',campain,is_lead,encuesta_respondida,telefono_validado});
                if(campain.turno_splash >= (campain.splashs.length)){
                    campain.turno_splash = 0;
                }
                campain.splash_actual = campain.splashs[campain.turno_splash];
                if(campain.turno_encuesta >= (campain.encuestas.length)){
                    campain.turno_encuesta = 0;
                }
                campain.encuesta_actual = campain.encuestas[campain.turno_encuesta];
                campain.turno_splash += 1;
                campain.turno_encuesta += 1;
                campain.save();
            }else{
                res.json({succes : false,mensaje:'No se ha encontrado campaña para hoy'});
            }
        }
    });
});

router.get('/:id',auth.authenticationAPI,function(req, res, next) {
    Campain.findById(req.params.id).populate('splashs').populate('encuestas').exec(function(err,campain){
        if(err){
            res.json({succes : false,mensaje:'A ocurrido un error',error:err});
        }else{
            if(campain){
                res.json({succes : true,mensaje:'Campaña encontrada',campain});
            }else{
                res.json({succes : false,mensaje:'La campaña no se ha encontrado'});
            }
        }
    });
});

router.post('/',auth.authenticationAPI,function(req,res,next){

  let nCampain = new Campain(req.body);
  console.log(nCampain);
  nCampain.splash_actual = nCampain.splashs[Number(nCampain.turno_splash)];
  nCampain.turno_splash++;
  nCampain.encuesta_actual = nCampain.encuestas[Number(nCampain.turno_encuesta)];
  nCampain.turno_encuesta++;
  nCampain.save(function(error,campain){
    if(error){
      res.json({succes : false,mensaje:'A ocurrido un error',error});
    }else{
      res.json({succes : true,mensaje:'Campaña creada con exito',campain});
    }
  });
});

router.put('/:id',auth.authenticationAPI,function(req,res,next){
    req.body.turno_splash = 1;
    req.body.turno_encuesta = 1;
    req.body.splash_actual = req.body.splashs[0];
    req.body.encuesta_actual = req.body.encuestas[0];
    Campain.update({ _id: req.params.id }, { $set: req.body },function(err,result){
        if(err){
          res.json({success: false, message:'An error has occurred',error:err});
        }else{
          res.json({success: true, message:'Campaña actualizada con exito',result});
        } 
    });
});

router.delete('/:id',auth.authenticationAPI,function(req,res,next){
    Campain.deleteOne({ _id: req.params.id }, function (err) {
        if(err){
        res.json({success: false, message:'A ocurrido con error', error:err});
        }else{
        res.json({success: true, message:'Camapaña eliminada con exito'});
        } 
    });
});

module.exports = router;
