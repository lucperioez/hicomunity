var express = require('express');
var router = express.Router();
const db = require('../config/db');
require("../models/M_Templates");
const mongoose = require('mongoose');
mongoose.connect(db.getConection(), { useNewUrlParser: true });
const Templates = mongoose.model("Templates");

router.get('/',function(req, res, next) {
    Templates.find().exec(function(err,templates){
        if(err){
            res.json({succes : false,mensaje:'A ocurrido un error',error:err});
        }else{
            res.json({succes : true,mensaje:'Templates',templates});
        }
    });
});

router.get('/:id',function(req, res, next) {
    Templates.findById(req.params.id).exec(function(err,template){
        if(err){
            res.json({succes : false,mensaje:'A ocurrido un error',error:err});
        }else{
            if(template){
                res.json({succes : true,mensaje:'Template found',template});
            }else{
                res.json({succes : true,mensaje:'Template not found'});
            }
        }
    });
});

router.post('/',function(req,res,next){  
  let nTemplates = new Templates(req.body);
  nTemplates.save(function(error,template){
    if(error){
      res.json({succes : false,mensaje:'A ocurrido un error',error});
    }else{
      res.json({succes : true,mensaje:'Template creado con exito',template});
    }
  });
});


router.put('/:id',function(req,res,next){
    Templates.update({ _id: req.params.id }, { $set: req.body },function(err,result){
        if(err){
          res.json({succes: false, mensaje:'An error has occurred',error:err});
        }else{
          res.json({succes: true, mensaje:'Template actualizado con exito',result});
        } 
    });
});

router.delete('/:id',function(req,res,next){
    Templates.deleteOne({ _id: req.params.id },function(err,result){
        if(err){
          res.json({succes: false, mensaje:'An error has occurred',error:err});
        }else{
          res.json({succes: true, mensaje:'Template eliminado con exito',result});
        } 
    });
});


module.exports = router;
