var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var ImpactosSchema = new Schema({
    splash : {type: Schema.ObjectId, ref: "SplashPages" },
    cliente : {type: Schema.ObjectId, ref: "Usuarios" },
    agencia : {type: Schema.ObjectId, ref: "Usuarios" },
    navegador : String,
    dispositivo : String,
    mac : String,
    wifi_name : String,
    folio_modem : {type: String}
}, {timestamps: true});

mongoose.model("Impactos", ImpactosSchema,"Impactos");
