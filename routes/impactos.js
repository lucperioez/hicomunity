var express = require('express');
var router = express.Router();
const db = require('../config/db');
require("../models/M_Impactos");
require("../models/M_Users");
const mongoose = require('mongoose');
mongoose.connect(db.getConection(), { useNewUrlParser: true });
const Impactos = mongoose.model("Impactos");
const Usuarios = mongoose.model("Usuarios");
var passport = require('passport');
var auth= require('../config/passport');

//por cliente y por modem

router.get('/cliente/:id',auth.authenticationAPI,function(req, res, next) {
    let query = {cliente:req.params.id};
    if(req.query.fechaInicio && req.query.fechaFin){
        query.createdAt = { 
            '$lte' : new Date(req.query.fechaFin),
            '$gte' : new Date(req.query.fechaInicio)
        }
    }
    Impactos.countDocuments(query).exec(function(err,impactos){
        if(err){
            res.json({succes : false,mensaje:'A ocurrido un error',error:err});
        }else{
            res.json({succes : true,mensaje:'impactos por cliente',impactos});
        }
    });
});
router.get('/modem/:folio',auth.authenticationAPI,function(req, res, next) {
    let query = {folio_modem:req.params.folio};
    if(req.query.fechaInicio && req.query.fechaFin){
        query.createdAt = { 
            '$lte' : new Date(req.query.fechaFin),
            '$gte' : new Date(req.query.fechaInicio)
        }
    }
    Impactos.countDocuments(query).exec(function(err,impactos){
        if(err){
            res.json({succes : false,mensaje:'A ocurrido un error',error:err});
        }else{
            res.json({succes : true,mensaje:'Impactos por modem',impactos});
        }
    });
});

router.post('/', function(req,res,next){
  const user =  Usuarios.findById(req.body.cliente).exec();
  req.body.agencia = user.agencia;
  let nImpacto = new Impactos(req.body);
  nImpacto.save(function(error,impacto){
    if(error){
      res.json({succes : false,mensaje:'A ocurrido un error',error});
    }else{
      res.json({succes : true,mensaje:'Impacto creado con exito',impacto});
    }
  });
});

module.exports = router;
