(function(){

     'use strict'

     function loginService(config,$http,$cookies){
         
        this.singIn = function(user,password){
            
            var url = '/login';

            return $http({
                    method: 'POST',
                    url: url,
                    data: {'usuario':user,'password':password},
                    headers: {
                        'Content-Type': 'application/json'
                    }
                });
        }
     
     }

     angular
        .module('hicomunity.login')
        .service('loginService', loginService);

 }());
