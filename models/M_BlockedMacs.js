var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var BlockedMacsSchema = new Schema({
    mac : String,
}, {timestamps : true});

mongoose.model("BlockedMacs", BlockedMacsSchema,"BlockedMacs");