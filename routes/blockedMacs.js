var express = require('express');
var router = express.Router();
const db = require('../config/db');
require("../models/M_BlockedMacs");
const mongoose = require('mongoose');
mongoose.connect(db.getConection(), { useNewUrlParser: true });
const BlockedMacs = mongoose.model("BlockedMacs");

var passport = require('passport');
var auth= require('../config/passport');

router.get('/',auth.authenticationAPI,function(req, res, next) {
    BlockedMacs.find({}).exec(function(err,macs){
        if(err){
            res.json({succes : false,message:'A ocurrido un error',error:err});
        }else{
            res.json({succes : true,message:'MAC\'s bloqueadas',macs});
        }
    });
});
router.get('/:mac',function(req, res, next) {
    BlockedMacs.findOne({mac:req.params.mac}).exec(function(err,mac){
        if(err){
            res.json({succes : false,message:'A ocurrido un error',error:err});
        }else{
            if(mac){
                res.json({succes : true,message:'La MAC se encuentra bloqueada',mac});
            }else{
                res.json({succes : false,message:'La MAC no se encuentra bloqueada',mac:req.body.mac});
            }
        }
    });
});

router.post('/',auth.authenticationAPI,function(req,res,next){
    new BlockedMacs(req.body).save(function(error,mac){
        if(error){
            res.json({succes : false,message:'A ocurrido un error',error});
        }else{
            res.json({succes : true,message:'MAC Bloqueada',mac});
        }
    });
});

router.delete('/:mac',auth.authenticationAPI,function(req,res,next){
    BlockedMacs.deleteMany({ mac: req.params.mac }, function (err) {
        if(err){
            res.json({success: false, message:'A ocurrido con error', error:err});
        }else{
            res.json({success: true, message:'MAC Desbloqueada'});
        } 
    });
});

module.exports = router;
