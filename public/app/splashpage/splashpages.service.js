(function(){

    'use strict'

    function splashpagesService(config,$http,$cookies,$q,$stateParams){
        

       this.obtenerSplashpages = function(_id){
           var token = $cookies.get('tkn') 
           var user_data = $cookies.getObject('usr_dta');  
           var url = '/splashpages/'+_id;

           return $http({
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'token' : token
            }    
            });
       }

       this.obtenerSplashpage = function(){
            var token = $cookies.get('tkn') 
            var url = '/splashpage/'+$stateParams.id_splash;

            return $http({
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'token' : token
            }    
            });
        }

        this.editarSplashpage = function(splashpage){
            var token = $cookies.get('tkn') 
            var url = '/splashpage/'+splashpage._id;

            return $http({
                method: 'PUT',
                url: url,
                data : splashpage,
                headers: {
                    'Content-Type': 'application/json',
                    'token' : token
                }    
            });
        }

        this.agregarSplashpage = function(splashpage){
            var token = $cookies.get('tkn') 
            var url = '/splashpage/';

            return $http({
                method: 'POST',
                url: url,
                data : splashpage,
                headers: {
                    'Content-Type': 'application/json',
                    'token' : token
                }    
            });
        }

        this.eliminarSplashpage = function(_id){
            var token = $cookies.get('tkn') 
            var url = '/splashpage/'+_id;

            return $http({
                method: 'DELETE',
                url: url,
                headers: {
                    'Content-Type': 'application/json',
                    'token' : token
                }    
            });
        }

    }

    angular
       .module('hicomunity.panel')
       .service('splashpagesService', splashpagesService);

}());