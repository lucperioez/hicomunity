var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var TemplatesSchema = new Schema({
    cliente : {type: Schema.ObjectId, ref: "Usuarios" },
    json : {},
    html : String
}, {timestamps: true});

mongoose.model("Templates", TemplatesSchema,"Templates");
