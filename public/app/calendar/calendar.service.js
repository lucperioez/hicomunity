(function(){

    'use strict'

    function calendarService(config,$http,$cookies,$q,$stateParams){
        
        this.getCampain = function(id){
            var token = $cookies.get('tkn')   
            var url = '/campain/'+id;
            return $http({
             method: 'GET',
             url: url,
             headers: {
                 'Content-Type': 'application/json',
                 'token' : token
             }    
             });
        }
        this.getCampains = function(id){
            var token = $cookies.get('tkn')   
            var url = '/campains/'+id;
            return $http({
             method: 'GET',
             url: url,
             headers: {
                 'Content-Type': 'application/json',
                 'token' : token
             }    
             });
        }

        this.getCampainsModems = function(id,modem){
            var token = $cookies.get('tkn')   
            var url = '/campains/'+id+'/'+modem;
            return $http({
             method: 'GET',
             url: url,
             headers: {
                 'Content-Type': 'application/json',
                 'token' : token
             }    
             });
        }
        this.guardarCampain = function(campain){
            var token = $cookies.get('tkn')   
            var url = '/campain';
            return $http({
                method: 'POST',
                url: url,
                data: campain,
                headers: {
                    'Content-Type': 'application/json',
                    'token' : token
                }    
            });
        }

        this.deleteCampain = function(id){
            var token = $cookies.get('tkn')   
            var url = '/campain/'+id;
            return $http({
                method: 'DELETE',
                url: url,
                headers: {
                    'Content-Type': 'application/json',
                    'token' : token
                }    
            });
        }

        this.editarCampain = function(campain){
            var token = $cookies.get('tkn')   
            var url = '/campain/'+campain._id;
            return $http({
                method: 'PUT',
                url: url,
                data: campain,
                headers: {
                    'Content-Type': 'application/json',
                    'token' : token
                }    
            });
        }
    }

    angular
       .module('hicomunity.panel')
       .service('calendarService', calendarService);

}());