var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var SplashPagesSchema = new Schema({
    usuario : {type: Schema.ObjectId, ref: "Usuarios" },
    nombre : String,
    configuracion : {
        color_logo : String,
        footer : {
            color_fondo : String,
            color_texto : String,
            color_texto2 : String
        },
        panel : {
            color_fondo : String,
            color_texto : String
        },
        imagen : {
            json : {},
            imagen : String
        }
    },
    url : String,    
    redes_sociales : {
        facebook : { type : Boolean , default : true},
        mail : { type : Boolean , default : true},
        gmail : { type : Boolean , default : false},
        twitter : { type : Boolean , default : false},
        sms : { type : Boolean , default : false},
    }
}, {timestamps: true});

mongoose.model("SplashPages", SplashPagesSchema,"SplashPages");
