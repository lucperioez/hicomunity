(function(){

    'use strict'
    
 function configuracionCtrl($scope,configuracionService,$cookies,$state){
    
    $('a').click(function(e){e.preventDefault()});
     
    var ctrl = this;
    ctrl.user = $cookies.getObject('usr_dta');
    //console.log(ctrl.user);
    ctrl.admin = ctrl.user.tipo == 1;

    ctrl.user.telefono = Number(ctrl.user.telefono);
    ctrl.user.pagos.telefono = Number(ctrl.user.pagos.telefono);
    
    ctrl.update = function(){
        console.log($state);
        //console.log(ctrl.user);     
        configuracionService.update(ctrl.user).then(function(response){
            console.log(response);
            toastr.options = {
                "positionClass": "toast-top-right",
                "showDuration": "200",
                };
            if(response.data.succes){
                toastr.success(response.data.mensaje, "");
                ctrl.user = response.data.user;
                $cookies.putObject('usr_dta', ctrl.user);
                ctrl.user.telefono = Number(ctrl.user.telefono);
                ctrl.user.pagos.telefono = Number(ctrl.user.pagos.telefono);
                window.location.reload(true);
            }else{
                toastr.error(response.data.mensaje, "");
            }
        }).catch(function(response){
            console.log(response);
        });
    
    }
    ctrl.updatePassword = function(){
        configuracionService.changePassword(ctrl.user).then(function(response){
            //console.log(response);
            toastr.options = {
                "positionClass": "toast-top-right",
                "showDuration": "200",
                };
            if(response.data.succes){
                toastr.success(response.data.mensaje, "");
                $('#modal-password').modal('hide');
            }else{
                toastr.error(response.data.mensaje, "");
            }
        }).catch(function(response){
            console.log(response);
        });
    }
    ctrl.checkPasswords = function(password){
        let input = document.querySelector('#repeat-password');
        if(password != ctrl.user.password2){
            input.setCustomValidity('Las contraseñas no coinciden');
        } else {
            input.setCustomValidity('');
        }
    }

     $scope.logo_changed= function(input, $scope) {
       if (input.files && input.files[0]) {
           var reader = new FileReader();
            reader.onload = function (e) {
                $('#logo').attr('src',  e.target.result );
                ctrl.user.logo_data = e.target.result;
                ctrl.user.logo = input.files[0].name;
             };
            reader.readAsDataURL(input.files[0]);
           }
     }

 }
       
   angular
       .module('hicomunity.panel')
       .controller('configuracionCtrl',configuracionCtrl)
}());
