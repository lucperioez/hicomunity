var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var ModemSchema = new Schema({
    
    usuario : {type: Schema.ObjectId, ref: "Usuarios" },
    agencia : {type: Schema.ObjectId, ref: "Usuarios" },
    folio : String,
    wifi_name : String,
    mac : String ,
    direccion : String,
    nombre : String

}, {timestamps: true});

mongoose.model("Modems", ModemSchema,"Modems");
