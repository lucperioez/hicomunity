(function(){

     'use strict'
  
  function splashCtrl(splashService,$stateParams,$location){
    var ctrl = this;
    ctrl.height = window.innerHeight;
    ctrl.image_height = ctrl.height - 75;
    ctrl.img_height = ctrl.height - (107 + 75);
    ctrl.panel_height = 107;
    ctrl.footer_height = 75;
    ctrl.step_wizard = 0;
    ctrl.encuesta_image = ctrl.height * 0.20;
    ctrl.encuesta_pregunta = ctrl.height * 0.10;
    ctrl.encuesta_opciones = ctrl.height * 0.70;
    ctrl.step = 0;
    console.log(ctrl.height);
    ctrl.paramsObject = {};
    ctrl.data_url = $location.search();
    ctrl.pgwBrowser = $.pgwBrowser();
    ctrl.block = false;
    ctrl.respuestas = [];
    ctrl.respuesta = {
      opciones : [],
      numero_pregunta : 0 ,
      tipo : 0
    };
    ctrl.id_encuesta = '';
    ctrl.splash = [];
    ctrl.splashloading = true;
    ctrl.phoneValidation = false;
    ctrl.codeValidation = false;
    ctrl.is_lead = "";
    ctrl.telefono_validado = "";
    ctrl.code = "";
    ctrl.phone = "";

    splashService.getSplash($stateParams.id_modem,$stateParams.id_cliente,ctrl.data_url.user_mac).then(function(response){
          ctrl.splash = response.data.campain.splash_actual;
          Cookies.set('redirect_url', response.data.campain.splash_actual.url);
          ctrl.is_lead = response.data.is_lead;
          ctrl.telefono_validado = response.data.telefono_validado;
          if(response.data.campain.encuesta_actual == undefined){
            ctrl.step_wizard = 1;
          }else{
            if(response.data.encuesta_respondida == true){
              ctrl.step_wizard = 1;
            }else{
              ctrl.preguntas = response.data.campain.encuesta_actual.preguntas;
              ctrl.estilo_encuesta = response.data.campain.encuesta_actual.estilo;
              ctrl.id_encuesta = response.data.campain.encuesta_actual._id;
            }
           
          }

          splashService.impacto({
            splash : ctrl.splash._id,
            cliente : $stateParams.id_cliente,
            navegador : ctrl.pgwBrowser.browser.name,
            dispositivo : ctrl.pgwBrowser.os.name,
            mac : ctrl.data_url.user_mac,
            wifi_name : ctrl.data_url.ap_group,
            folio_modem : $stateParams.id_modem
    
          }).then(function(response){
            ctrl.splashloading = false;
            
          
          }).catch(function(response){   
            console.log(response);
          });
        
    }).catch(function(response){   
        
    });
    
    splashService.getMacBloqued(ctrl.data_url.user_mac).then(function(response){
      console.log(response);
      if(response.data.succes){
        ctrl.block = true;
      }
      console.log(ctrl.block);
    }).catch(function(response){   
      console.log(response);
    });

    ctrl.acceder = function(){
      if(ctrl.is_lead == true && ctrl.telefono_validado == true){
        ctrl.abrirConexion();
      }else if(ctrl.is_lead == true && ctrl.telefono_validado == false){
        ctrl.phoneValidation = true;
      }else{
        ctrl.step_wizard = 2;
      }
    }

    ctrl.validateCode = function(){

      splashService.validatePhone(ctrl.data_url.user_mac,ctrl.code,ctrl.phone).then(function(response){
        if(response.data.succes == true){
          ctrl.codeValidation = false;
          ctrl.abrirConexion();
        }else{
          alert("Codigo incorrecto");
        }
      }).catch(function(response){   
        console.log(response);
      });
    }

    ctrl.validatePhone = function(){
      console.log("entro");
      splashService.sendSms(ctrl.data_url.user_mac,ctrl.phone).then(function(response){
        console.log(response);
        ctrl.codeValidation = true;
        ctrl.phoneValidation = false;
      }).catch(function(response){   
        console.log(response);
      });
    }

    ctrl.addRespuesta = function(pregunta){
      ctrl.step = ctrl.step + 1;
      ctrl.respuesta.tipo = pregunta.tipo;
      ctrl.respuesta.numero_pregunta = pregunta.numero_pregunta;
      ctrl.respuestas.push(ctrl.respuesta);
      ctrl.respuesta = {
        opciones : [],
        numero_pregunta : 0 ,
        tipo : 0
      };
    }

    ctrl.addRespuestas = function(pregunta){
      ctrl.step_wizard = ctrl.step_wizard + 1;
      ctrl.respuesta.tipo = pregunta.tipo;
      ctrl.respuesta.numero_pregunta = pregunta.numero_pregunta;
      ctrl.respuesta.opciones = ctrl.respuesta.opciones.filter((op)=>op!=null || op!=false || op!=undefined);
      ctrl.respuestas.push(ctrl.respuesta);
      console.log(ctrl.respuestas);
      ctrl.respuesta = {
        opciones : [],
        numero_pregunta : 0 ,
        tipo : 0
      };

      

      splashService.addRespuesta(ctrl.id_encuesta,{
        mac : ctrl.data_url.user_mac,
        respuesta : ctrl.respuestas,
        modem : $stateParams.id_modem
      }).then(function(response){
        console.log(response);
      }).catch(function(response){   
        console.log(response);
      });

    }

    ctrl.addConexion = function(tipo){
      splashService.conexion({
        tipo_conexion : tipo,
        splash : ctrl.splash._id,
        cliente : $stateParams.id_cliente,
        navegador : ctrl.pgwBrowser.browser.name,
        dispositivo : ctrl.pgwBrowser.os.name,
        mac : ctrl.data_url.user_mac,
        wifi_name : ctrl.data_url.ap_group,
        folio_modem : $stateParams.id_modem
      }).then(function(response){
        console.log(response);
      }).catch(function(response){   
        console.log(response);
      });
    }

    ctrl.leadForm = function(){
      
      splashService.lead({
        mac : ctrl.data_url.user_mac,
        modem : $stateParams.id_modem,
        cliente : $stateParams.id_cliente,
        nombre : ctrl.data_form.nombre,
        sexo : ctrl.data_form.sexo,
        edad : ctrl.data_form.edad,
        correo : ctrl.data_form.correo,
        telefono : ctrl.data_form.telefono,
      }).then(function(response){
        ctrl.abrirConexion();
        console.log(response);
        ctrl.addConexion(2);
      }).catch(function(response){   
        console.log(response);
      });

    }

    ctrl.me = function() {

            FB.api('/me', function(fbUser) {
              alert(fbUser.name);
              alert("Open the pod bay doors, " + fbUser.name + ".");
                
            });
    
    };

    ctrl.abrirConexion = function(){
      ctrl.paramsObject = $location.search();
      console.log(ctrl.paramsObject);
      var urlFBRedirect =  "http://"+ctrl.paramsObject.ap_ip+":"+ctrl.paramsObject.ap_port+"/logon"+"?user=123&user_mac="+ctrl.paramsObject.user_mac+"&ap_id="+ctrl.paramsObject.user_mac.replace(':','')+"&token=passwd";	
      window.location.href = urlFBRedirect;
    
    }
    

      /*Facebook.api('/me?fields=name,id,gender,birthday,picture,email', function(response) {
        console.log(response);
        splashService.lead({
          identificador : response.id,
          mac : ctrl.data_url.user_mac,
          modem : $stateParams.id_modem,
          cliente : $stateParams.id_cliente,
          nombre : response.name,
          url_imagen : response.picture.data.url
        }).then(function(response){
          console.log(response);
          ctrl.addConexion(1);
        }).catch(function(response){   
          console.log(response);
        });
      });*/
  

  }
  
    angular
        .module('hicomunity.splashpage')
        .controller('splashCtrl',splashCtrl)
        .filter('range', function() {
          return function(input, min, max) {
            min = parseInt(min); //Make string input int
            max = parseInt(max);
            for (var i=min; i<max; i++)
              input.push(i);
            return input;
          };
        })
  
 }());
 
