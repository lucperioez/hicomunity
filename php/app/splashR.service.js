(function(){

    'use strict'

    function splashService($http){
        
        var host = "http://54.193.66.241:3000";;

        this.getSplash = function(modem,cliente){

       
            var url = host+'/campain/'+modem+'/'+cliente;

            return $http({
                method: 'GET',
                url: url,
                headers: {
                        'Content-Type': 'application/json'
                    }
            });
        }
       
        this.impacto = function(data){
            var url = host+'/impacto';

            return $http({
                method: 'POST',
                url: url,
                data:data,
                headers: {
                        'Content-Type': 'application/json'
                    }
            });
        }
        
        this.conexion = function(data){
            var url = host+'/conexion';

            return $http({
                method: 'POST',
                url: url,
                data:data,
                headers: {
                        'Content-Type': 'application/json'
                    }
            });
        }

        this.lead = function(data){
            var url = host+'/lead';
           
            return $http({
                method: 'POST',
                url: url,
                data:data,
                headers: {
                        'Content-Type': 'application/json'
                    }
            });
        }

        this.addRespuesta = function(id,data){
            var url = host+'/encuesta/'+id+'/respuesta';

            return $http({
                method: 'POST',
                url: url,
                data:data,
                headers: {
                        'Content-Type': 'application/json',
                        "Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Headers": "X-Requested-With",
                        "Access-Control-Allow-Methods": "GET, POST,PUT"

                    }
            });
        }
        this.getMacBloqued = function(mac){
            var url = host+'/blocked/'+mac;

            return $http({
                method: 'GEt',
                url: url,
                headers: {
                        'Content-Type': 'application/json'
                    }
            });
        }
    }

    angular
       .module('hicomunity.splashpage')
       .service('splashService', splashService);

}());