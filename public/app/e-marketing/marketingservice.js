(function(){

    'use strict'

    function marketingService(config,$http,$cookies,$q,$stateParams){
        

       this.obtenerTemplates = function(cliente){
           var token = $cookies.get('tkn') 
           var user_data = $cookies.getObject('usr_dta');  
           var url = '/templates/'+cliente;

           return $http({
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'token' : token
            }    
            });
       }

       this.getLeads = function(cliente,params){
        var token = $cookies.get('tkn') 
        if(params.edad1 && params.edad2){
            var url = '/leads/marketing/'+cliente+'?sexo='+params.sexo+'&edad1='+params.edad1+'&edad2='+params.edad2;
        }else{
            var url = '/leads/marketing/'+cliente+'?sexo='+params.sexo;
        }

        return $http({
                method: 'GET',
                url: url,
                headers: {
                    'Content-Type': 'application/json',
                    'token' : token
                }    
            });
       }
    
       this.sendMail = function(data){
        var token = $cookies.get('tkn') 
        var user_data = $cookies.getObject('usr_dta');  
        var url = '/sendMail';

        return $http({
         method: 'POST',
         url: url,
         data : data,
         headers: {
             'Content-Type': 'application/json',
             'token' : token
         }    
         });
       }

       this.sendSms = function(message,number){
        var token = $cookies.get('tkn') 
        var user_data = $cookies.getObject('usr_dta');  
        var url = '/sendSMS';

        return $http({
         method: 'POST',
         url: url,
         data : message,
         headers: {
             'Content-Type': 'application/json',
             'token' : token
         }    
         });
       }

    }

    angular
       .module('hicomunity.panel')
       .service('marketingService', marketingService);

}());