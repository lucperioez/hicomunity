(function(){

     'use strict'
     
  function usuariosCtrl(usuariosService,NgTableParams,$scope){
         
      
      var ctrl = this;
      $scope.show_filter = false;
      var data = [{name: "Moroni", age: 50},{name: "Moroni", age: 50},{name: "Moroni", age: 50},{name: "Moroni", age: 50} ];
      ctrl.tableParams = new NgTableParams({}, { dataset: data});
      ctrl.agencias = [];
      ctrl.usuarios = [];
      ctrl.user_d = {};

      usuariosService.get()
      .then(function(response){
        ctrl.usuarios = response.data.users;
        ctrl.tableParams = new NgTableParams({}, { dataset:  ctrl.usuarios});
        ctrl.agencias = response.data.users.filter((user)=>user.tipo==2);
      })
      .catch(function(response){   
          toastr.options = {
          "positionClass": "toast-top-right",
          "showDuration": "200",
          };
          toastr.error(response.data.message, "");
      });

      ctrl.editar = function(user = null){

        if(user == null){
          ctrl.accion = "insert";
          ctrl.userSelected = 
          {
            "pagos" : {
                "contacto" : "",
                "telefono" : "",
                "email" : ""
            },
            "nombre" : "",
            "contacto" : "",
            "direccion" : "",
            "telefono" : "",
            "email" : "",
            "usuario" : "",
            "password" : "",
            "tipo" : "3",
            "estatus" : "1",
            "agencia" : "",
            "logo" : "assets/app/media/img/users/default-user.png", 
        };

        }else{
          ctrl.accion = "update";
          user.telefono = Number(user.telefono);
          user.pagos.telefono = Number(user.pagos.telefono);
          user.estatus = user.estatus.toString();
          user.tipo = user.tipo.toString();
          ctrl.userSelected = user;
          if(user.tipo == '3'){
            ctrl.userSelected.agencia = user.agencia._id;
          }
        }
        $('#m_modal_4').modal();
      }


      ctrl.singIn = function(){        
        if(ctrl.accion == "insert"){
          if(ctrl.userSelected.tipo == '2'){
            ctrl.userSelected.agencia = undefined;
          }
          usuariosService.add(ctrl.userSelected).then(function(response) {
            console.log(response);
            if(response.data.succes){
              ctrl.usuarios.push(response.data.user);             
              ctrl.tableParams = new NgTableParams({}, { dataset: ctrl.usuarios });
              ctrl.tableParams.reload();
              toastr.options = {
                "positionClass": "toast-top-right",
                "showDuration": "200",
                };
                toastr.success(response.data.mensaje, "");
              $('#m_modal_4').modal('hide');
              ctrl.userSelected = 
                  {
                    "pagos" : {
                        "contacto" : "",
                        "telefono" : "",
                        "email" : ""
                    },
                    "nombre" : "",
                    "contacto" : "",
                    "direccion" : "",
                    "telefono" : "",
                    "email" : "",
                    "usuario" : "",
                    "password" : "",
                    "tipo" : "3",
                    "estatus" : "1",
                    "agencia" : "",
                    "logo" : "assets/app/media/img/users/default-user.png"
                };
            }
          }).catch(function(response){
            console.log(response);
          });
        }else if(ctrl.accion == "update"){
          usuariosService.update(ctrl.userSelected).then(function(response) {
            console.log(response);
            if(response.data.succes){
              ctrl.usuarios = ctrl.usuarios.filter((user)=>user._id!=ctrl.userSelected._id);
              ctrl.usuarios.push(ctrl.userSelected);             
              ctrl.tableParams = new NgTableParams({}, { dataset: ctrl.usuarios });
              ctrl.tableParams.reload();
              toastr.options = {
                "positionClass": "toast-top-right",
                "showDuration": "200",
                };
                toastr.success(response.data.mensaje, "");
              $('#m_modal_4').modal('hide');
              ctrl.userSelected = 
                  {
                    "pagos" : {
                        "contacto" : "",
                        "telefono" : "",
                        "email" : ""
                    },
                    "nombre" : "",
                    "contacto" : "",
                    "direccion" : "",
                    "telefono" : "",
                    "email" : "",
                    "usuario" : "",
                    "password" : "",
                    "tipo" : "3",
                    "estatus" : "1",
                    "agencia" : "",
                    "logo" : "assets/app/media/img/users/default-user.png"
                };
            }
          }).catch(function(response){
            console.log(response);
          });
        }
      }
      ctrl.checkPasswords = function(password){
        let input = document.querySelector('#repeat-password');
        if(password != ctrl.userSelected.password){
          input.setCustomValidity('Las contraseñas no coinciden');
        } else {
          input.setCustomValidity('');
        }
      }
 
      $scope.logo_changed= function(input, $scope) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
             reader.onload = function (e) {
                 $('#logo').attr('src',  e.target.result );
                 ctrl.userSelected.logo_data = e.target.result;
                 ctrl.userSelected.logo = input.files[0].name;
              };
             reader.readAsDataURL(input.files[0]);
            }
      }
      ctrl.checkStatus = function(estatus){
        if(estatus == '1'){
          return "Activo";
        }else if(estatus == '2'){
          return "Inactivo";
        }
      }

      ctrl.eliminar = function(user){
        ctrl.user_d = user;
        $('#modal-delete').modal('show');
      }
      ctrl.delete = function(){

        usuariosService.delete(ctrl.user_d).then(function(response) {
          console.log(response);
          if(response.data.success){
            ctrl.usuarios = ctrl.usuarios.filter((user)=>user._id!=ctrl.user_d._id);
            ctrl.tableParams = new NgTableParams({}, { dataset: ctrl.usuarios });
            ctrl.tableParams.reload();
            ctrl.user_d = {};
            toastr.options = {
              "positionClass": "toast-top-right",
              "showDuration": "200",
            };
            toastr.success(response.data.mensaje || response.data.message, "");
          }
        }).catch(function(response){
          console.log(response);
        });
      }
  }
        
    angular
        .module('hicomunity.panel')
        .controller('usuariosCtrl',usuariosCtrl)
 }());
 