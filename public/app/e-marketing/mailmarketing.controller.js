(function(){

     'use strict'
     
  function mailmarketingCtrl(marketingService,NgTableParams,$state,usuariosService,$cookies,$sce){
         
      var ctrl = this;
      ctrl.selectedTemplate = 0;
      ctrl.leadsClient = [];
      ctrl.enviar = 0;
      ctrl.seleccionarTodas = 0;
      ctrl.plantilla_selecionada = "";
      ctrl.filtro_edad = 0;
      ctrl.valor1 = 0;
      ctrl.valor2 = 0;
      ctrl.sexo = "";
      ctrl.busqueda = 0;
      ctrl.step = 1;
      ctrl.plantillas = [];
      ctrl.template = -1;

        
      toastr.options = {
        "positionClass": "toast-top-right",
        "showDuration": "200",
      };

      ctrl.leadsStep = function(){
        ctrl.step ++;
        if(ctrl.step == 2){
          ctrl.getTemplates();
        }
      }
      ctrl.setStep = function(step){
        ctrl.step = step;
        if(ctrl.step == 2){
          ctrl.getTemplates();
        }
      }

      ctrl.params = {};
      ctrl.user_data = $cookies.getObject('usr_dta');
      ctrl.admin = ctrl.user_data.tipo == 1;  
      ctrl.agencia = '';
      ctrl.cliente = '';
      ctrl.edad = 0;
      ctrl.params.edad1 = 15;
      ctrl.params.edad2 = 65;
      ctrl.leads = [];


      if(ctrl.user_data.tipo != 3){
        //ctrl.user_data._id;
              //console.log(response);
          usuariosService.get().then(function(response){
              console.log(response);
              let data = response.data.users;
              ctrl.usuarios = data;
            if(ctrl.user_data.tipo == 2){
                  ctrl.clientes = data;
            }else{  
                  ctrl.agencias = data.filter((user)=>user.tipo==2);
            }
          }).catch(function(response){ 
              toastr.error(response.data.message, "");
          });
    }
    ctrl.getCliente = function(){
        if(ctrl.agencia != ''){
            ctrl.clientes = ctrl.usuarios.filter((user)=> (user.tipo==3) && (user.agencia._id==ctrl.agencia) );
        }else{
            ctrl.cliente = '';
            ctrl.cliente_nombre = '';
        }    
    }
    ctrl.getData = function(){
      let cParams = ctrl.params;
      if(ctrl.edad == 0){
        cParams.edad1 = undefined;
        cParams.edad2 = undefined;
      }
      marketingService.getLeads(ctrl.cliente,cParams).then(function(response){
        console.log(response);
        if(response.data.succes){
          ctrl.leads = response.data.leads;
          ctrl.tableParams = new NgTableParams({}, { dataset: response.data.leads});
        }
      }).catch(function(response){

      });
    }
    
    ctrl.checkSexo = function(s){
      if(s == 'male'){
        return 'Masculino';
      }else if(s == 'female'){
        return 'Femenino';
      }
      return s;
    }
    ctrl.getTemplates = function(){
      if(ctrl.user_data.tipo != 3){
        marketingService.obtenerTemplates(ctrl.cliente).then(function(response){
          console.log(response);
          if(response.data.succes){
            ctrl.plantillas = response.data.templates;  
          }
        }).catch(function(response){
          console.log(response);
        });
      }else{
        marketingService.obtenerTemplates(ctrl.user_data._id).then(function(response){
          console.log(response);
          if(response.data.succes){
            ctrl.plantillas = response.data.templates;  
          }
        }).catch(function(response){
          console.log(response);
        });
      }
    }
    ctrl.sendMails = function(){
      let data = {
        asunto : ctrl.asunto,
        html : ctrl.plantillas[ctrl.template].html,
        to : ctrl.leads.map(function(l){return l.correo})
      }
      marketingService.sendMail(data).then(function(response){
        console.log(response);
        if(response.data.succes){
          toastr.success(response.data.message, "");
          ctrl.setStep(4);
        }
      }).catch(function(response){
        console.log(response);
      });
    }
    ctrl.trustAsHtml = function(html) {
      return $sce.trustAsHtml(html);
    }
  }
        
    angular
        .module('hicomunity.panel')
        .controller('mailmarketingCtrl',mailmarketingCtrl)
        .directive('myIframe', ['$compile', 
        function($compile) {
          return {
            restrict: 'E',
            template: `
              <iframe></iframe>
            `,
            scope: {
              iframeTemplate: '=',
              iframeContext: '='
            },
            link: function($scope, $element, $attrs) {
              
              let setScope = () => {
                angular.forEach($scope.iframeContext, (value, key) => {
                  $scope[key] = value;
                });
                setContextWatchers();
              };
              
              let render = () => {
                $compile($element
                  .find('iframe').contents()
                  .find('body').html($scope.iframeTemplate)
                  .contents()
                )($scope);
              };
              
              let setContextWatchers = () => {
                angular.forEach($scope.iframeContext, (value, key) => {
                  $scope.$watch(
                    ($scope) => {
                      return $scope.iframeContext[key];
                    },
                    () => {
                      $scope[key] = $scope.iframeContext[key];
                    });
                });  
              };
              
              $scope.$watch('iframeTemplate', () => {
                setScope();
                render('template');
              });
            }
          }
        }
        ])
        .directive('myDynamicHtml', ['$compile', 
        function($compile) {
          return {
            restrict: 'E',
            scope: {
              contents: '='
            },
            link: function($scope, $element, $attrs) {
              let element = $element;
              $scope.$watch('contents', () => {
                let newElement = $compile($scope.contents)($scope);
                element.replaceWith(newElement);
                element = newElement;
              });
            }
          };
        }
        ])
 }());
 