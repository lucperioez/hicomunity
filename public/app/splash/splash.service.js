(function(){

    'use strict'

    function splashService($http){
        

        this.getSplash = function(modem,cliente,mac){

       
            var url = '/campain/'+modem+'/'+cliente+'/'+mac;

            return $http({
                method: 'GET',
                url: url,
                headers: {
                        'Content-Type': 'application/json'
                    }
            });
        }

        this.validatePhone = function(mac,code,phone){

       
            var url = '/lead/'+mac+'/'+code+'/'+phone;

            return $http({
                method: 'GET',
                url: url,
                headers: {
                        'Content-Type': 'application/json'
                    }
            });
        }
       
    
        this.sendSms = function(mac,phone){ 
            var url = '/sendCode';
    
            return $http({
             method: 'POST',
             url: url,
             data : { mac: mac, telefono: phone },
             headers: {
                 'Content-Type': 'application/json',
             }    
             });
           }

        this.impacto = function(data){
            var url = '/impacto';

            return $http({
                method: 'POST',
                url: url,
                data:data,
                headers: {
                        'Content-Type': 'application/json'
                    }
            });
        }
        
        this.conexion = function(data){
            var url = '/conexion';

            return $http({
                method: 'POST',
                url: url,
                data:data,
                headers: {
                        'Content-Type': 'application/json'
                    }
            });
        }

        this.lead = function(data){
            var url = '/lead';

            return $http({
                method: 'POST',
                url: url,
                data:data,
                headers: {
                        'Content-Type': 'application/json'
                    }
            });
        }

        this.addRespuesta = function(id,data){
            var url = '/encuesta/'+id+'/respuesta';

            return $http({
                method: 'POST',
                url: url,
                data:data,
                headers: {
                        'Content-Type': 'application/json'
                    }
            });
        }
        this.getMacBloqued = function(mac){
            var url = '/blocked/'+mac;

            return $http({
                method: 'GEt',
                url: url,
                headers: {
                        'Content-Type': 'application/json'
                    }
            });
        }
    }

    angular
       .module('hicomunity.splashpage')
       .service('splashService', splashService);

}());