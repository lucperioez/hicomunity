(function(){

     'use strict'
     
  function whatsappmarketingCtrl(marketingService,NgTableParams,$state,usuariosService,$cookies,$sce){
         
      var ctrl = this;
      ctrl.selectedTemplate = 0;
      ctrl.leadsClient = [];
      ctrl.enviar = 0;
      ctrl.seleccionarTodas = 0;
      ctrl.plantilla_selecionada = "";
      ctrl.filtro_edad = 0;
      ctrl.valor1 = 0;
      ctrl.valor2 = 0;
      ctrl.sexo = "";
      ctrl.busqueda = 0;
      ctrl.step = 1;
      ctrl.plantillas = [];
      ctrl.template = -1;

      ctrl.mensaje = "";
        
      toastr.options = {
        "positionClass": "toast-top-right",
        "showDuration": "200",
      };

      ctrl.leadsStep = function(){
        ctrl.step ++;
      }
      ctrl.setStep = function(step){
        ctrl.step = step;
      }

      ctrl.params = {};
      ctrl.user_data = $cookies.getObject('usr_dta');
      ctrl.admin = ctrl.user_data.tipo == 1;  
      ctrl.agencia = '';
      ctrl.cliente = '';
      ctrl.edad = 0;
      ctrl.params.edad1 = 15;
      ctrl.params.edad2 = 65;
      ctrl.leads = [];


      if(ctrl.user_data.tipo != 3){
        //ctrl.user_data._id;
              //console.log(response);
          usuariosService.get().then(function(response){
              console.log(response);
              let data = response.data.users;
              ctrl.usuarios = data;
            if(ctrl.user_data.tipo == 2){
                  ctrl.clientes = data;
            }else{  
                  ctrl.agencias = data.filter((user)=>user.tipo==2);
            }
          }).catch(function(response){ 
              toastr.error(response.data.message, "");
          });
    }
    ctrl.getCliente = function(){
        if(ctrl.agencia != ''){
            ctrl.clientes = ctrl.usuarios.filter((user)=> (user.tipo==3) && (user.agencia._id==ctrl.agencia) );
        }else{
            ctrl.cliente = '';
            ctrl.cliente_nombre = '';
        }    
    }
    ctrl.getData = function(){
      let cParams = ctrl.params;
      if(ctrl.edad == 0){
        cParams.edad1 = undefined;
        cParams.edad2 = undefined;
      }
      marketingService.getLeads(ctrl.cliente,cParams).then(function(response){
        console.log(response);
        if(response.data.succes){
          ctrl.leads = response.data.leads;
          ctrl.tableParams = new NgTableParams({}, { dataset: response.data.leads});
        }
      }).catch(function(response){

      });
    }
    
    ctrl.checkSexo = function(s){
      if(s == 'male'){
        return 'Masculino';
      }else if(s == 'female'){
        return 'Femenino';
      }
      return s;
    }
  }
        
    angular
        .module('hicomunity.panel')
        .controller('whatsappmarketingCtrl',whatsappmarketingCtrl)
       
    
 }());
 