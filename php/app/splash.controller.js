(function(){

     'use strict'

  function splashCtrl(splashService,$location){
    var ctrl = this;

    ctrl.height = window.innerHeight;
    ctrl.image_height = ctrl.height - 75;
    ctrl.img_height = ctrl.height - (107 + 75);
    ctrl.panel_height = 107;
    ctrl.footer_height = 75;
    ctrl.step_wizard = 0;
    ctrl.encuesta_image = ctrl.height * 0.20;
    ctrl.encuesta_pregunta = ctrl.height * 0.10;
    ctrl.encuesta_opciones = ctrl.height * 0.70;
    ctrl.step = 0;
    ctrl.url = "";
//    console.log(ctrl.height);

    ctrl.data_url = $location.search()
    ctrl.pgwBrowser = $.pgwBrowser();
    ctrl.block = false;
    ctrl.respuestas = [];
    ctrl.respuestax = 0;
    ctrl.respuesta = {
      opciones : [],
      numero_pregunta : 0 ,
      tipo : 0
    };
    ctrl.id_encuesta = '';
    ctrl.existe_encuesta = 0;
    splashService.getSplash(ctrl.data_url.m,ctrl.data_url.c).then(function(response){
      
      
        if(response.data.succes){
          
          if(response.data.campain.encuestas.length == 0){
            ctrl.splash = response.data.campain.splash_actual;
            ctrl.step_wizard =1;
            ctrl.url = response.data.campain.splash_actual.url;
          
          
          }else{
            ctrl.splash = response.data.campain.splash_actual;
            ctrl.preguntas = response.data.campain.encuesta_actual.preguntas;
            
            ctrl.estilo_encuesta = response.data.campain.encuesta_actual.estilo;
            ctrl.id_encuesta = response.data.campain.encuesta_actual._id;
            ctrl.url = response.data.campain.splash_actual.url;

           
          }
          ctrl.tanaza_url = "http://"+ctrl.data_url.ap_ip+":"+ctrl.data_url.ap_port+"/logon"+"?user=123&user_mac="+ctrl.data_url.user_mac+"&ap_id="+ctrl.data_url.user_mac.replace(/:/g, '')+"&token=passwd&user_url="+ctrl.url;
          console.log(ctrl.tanaza_url);
  
            //addImpacto
            splashService.impacto({
              splash : ctrl.splash._id,
              cliente : ctrl.data_url.c,
              navegador : ctrl.pgwBrowser.browser.name,
              dispositivo : ctrl.pgwBrowser.os.name,
              mac : ctrl.data_url.user_mac,
              wifi_name : ctrl.data_url.ap_group,
              folio_modem : ctrl.data_url.m
      
            }).then(function(response){
            
            }).catch(function(response){   
              console.log(response);
            });
        }
    }).catch(function(response){   
        
    });
    
    splashService.getMacBloqued(ctrl.data_url.user_mac).then(function(response){
      console.log(response);
      if(response.data.succes){
        ctrl.block = true;
      }
      console.log(ctrl.block);
    }).catch(function(response){   
      console.log(response);
    });

    ctrl.addRespuesta = function(pregunta){
      ctrl.step = ctrl.step + 1;
      ctrl.respuesta.tipo = pregunta.tipo;
      ctrl.respuesta.numero_pregunta = pregunta.numero_pregunta;
      ctrl.respuestas.push(ctrl.respuesta);
      ctrl.respuesta = {
        opciones : [],
        numero_pregunta : 0 ,
        tipo : 0
      };
    }

    ctrl.accion = function(){

        if(ctrl.respuesta == 1){
            ctrl.step ++;
        }else if(ctrl.respuesta == 2){
            ctrl.step = ctrl.preguntas.length;
        }
    }
  

    ctrl.addRespuestas = function(pregunta){
      ctrl.step_wizard = ctrl.step_wizard + 1;
      ctrl.respuesta.tipo = pregunta.tipo;
      ctrl.respuesta.numero_pregunta = pregunta.numero_pregunta;
      ctrl.respuesta.opciones = ctrl.respuesta.opciones.filter((op)=>op!=null || op!=false || op!=undefined);
      ctrl.respuestas.push(ctrl.respuesta);
      console.log(ctrl.respuestas);
      ctrl.respuesta = {
        opciones : [],
        numero_pregunta : 0 ,
        tipo : 0
      };

      splashService.addRespuesta(ctrl.id_encuesta,{
        mac : ctrl.data_url.user_mac,
        respuesta : ctrl.respuestas,
        modem : ctrl.data_url.m
      }).then(function(response){
        console.log(response);
      }).catch(function(response){   
        console.log(response);
      });

    }

    ctrl.addConexion = function(tipo){
      splashService.conexion({
        tipo_conexion : tipo,
        splash : ctrl.splash._id,
        cliente : ctrl.data_url.c,
        navegador : ctrl.pgwBrowser.browser.name,
        dispositivo : ctrl.pgwBrowser.os.name,
        mac : ctrl.data_url.user_mac,
        wifi_name : ctrl.data_url.ap_group,
        folio_modem : ctrl.data_url.m
      }).then(function(response){
        console.log(response);
      }).catch(function(response){   
        console.log(response);
      });
    }

    ctrl.leadForm = function(){
      
      splashService.lead({
        mac : ctrl.data_url.user_mac,
        modem : ctrl.data_url.m,
        cliente : ctrl.data_url.c,
        nombre : ctrl.data_form.nombre,
        sexo : ctrl.data_form.sexo,
        edad : ctrl.data_form.edad,
        correo : ctrl.data_form.correo,
        telefono : ctrl.data_form.telefono,
      }).then(function(response){
        console.log(response);
        ctrl.addConexion(2);
       // ctrl.abrirConexion();
      }).catch(function(response){   
        console.log(response);
      });

    }

    ctrl.abrirConexion = function(){
      alert(ctrl.tanaza_url);
      window.location.href = ctrl.tanaza_url;
  
    }

    ctrl.facebook = function(){
      window.location.href = 'fblogin.php?mac='+ctrl.data_url.user_mac+'&modem='+ctrl.data_url.m+'&cliente='+ctrl.data_url.c;
    }

  }
  
    angular
        .module('hicomunity.splashpage')
        .controller('splashCtrl',splashCtrl);
 }());
 