<?php
require_once('hybridauth/src/autoload.php');
use Hybridauth\Hybridauth;
use Hybridauth\HttpClient;

header("Access-Control-Allow-Origin: *");

//Get Session-Request Info
function abrirConexion(){
	$_SESSION['REQUEST2'] = $_SESSION['REQUEST'];
	$urlFBRedirect = $urlFBRedirect = "http://".$_SESSION['REQUEST']['ap_ip'].":".$_SESSION['REQUEST']['ap_port']."/logon"
	."?user=123&user_mac=".$_SESSION['REQUEST']['user_mac']."&ap_id=".str_replace(':','',$_SESSION['REQUEST']['user_mac'])
	."&token=passwd";	
	$_SESSION['redirecURL'] = $urlFBRedirect;
	//$_COOKIE['url'] = $urlFBRedirect;
	//header("Location: " . "http://" . $_SERVER['HTTP_HOST'] ."/conectalia/frontend/splash_page_v2/splash_page/splash.php?sp=0&code=123&tanaza_url=".urlencode($urlFBRedirect));
	header("Location: " . "http://" . $_SERVER['HTTP_HOST'] ."/cnt_v2/frontend/splash_page_v2/splash_page/splash.php?sp=0&code=123");

}

if(isset($_REQUEST['phone'])){
	abrirConexion();
}else{
			/* 
			$config = [
				'callback' => HttpClient\Util::getCurrentUrl(),
				'providers' => [	
					'Facebook' => [ 
						'enabled' => true,
						'keys'    => [ 'key' => '331662914278195', 'secret' => 'd4113a20f917e0695cbd8dcff831ef76'],
						"scope"   => [], 
					]
				],
			];
			$hybridauth = new Hybridauth($config);
			$social = $hybridauth->authenticate('Facebook');
			//echo(print_r($social));
			
			// Get the User Profile
			$user_profile = $social->getUserProfile();


	if (FB_APP_ID === '' || FB_APP_SECRET === '') {
		echo 'Enter Facebook APP ID';
		exit;
    }//
    Send Authentication */
}
?>
<!DOCTYPE html>

<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en" ng-app="hicomunity.splashpage">

	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>Splashpage</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
		<!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">-->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
		<link rel="stylesheet" href="app/libs/bootstrap/bootstrap.min.css">
		<link href="assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

		<!--ANGULAR JS -->
		<script src="app/libs/angular/angular.min.js"></script>
		<script src="app/libs/angular//angular-animate.min.js"></script>
  		<script src="app/libs/angular//angular-aria.min.js"></script>
  		<script src="app/libs/angular//angular-messages.min.js"></script>
		<script src="app/libs/angular/angular-material.min.js"></script>
		<script src="app/libs/angular/angular-cookies.min.js"></script>
		<script src="app/hicommunity.app.js"></script>

		<script src="app/libs/jquery/jquery.min.js"></script>
		<script src="app/libs/PgwBrowser/pgwbrowser.min.js"></script>

		<!--SPLASH JS -->
		<script src="app/splash.controller.js"></script>
        <script src="app/splash.service.js"></script>
        
        <style>
        body {
            overflow:hidden !important;
        }
        .img-responsive{
        height: 100% !important;
        width: 100% !important;
        }
        .logo{
            height: auto !important;
            width: 45% !important;
        }
        .splash-image{
            background-size:100% 100%  !important;

        }
        .splash-panel{
            
        
        }
        .terminos-container{
            text-align: center;
            height: 50% !important; 
            padding-top: 10px;
        }
        .logo-container{
            text-align: center;
            height: 50% !important; 
        }
        .logo-container .logo{
            margin: 0 auto; 
        }
        .btns-social{
            margin-top: 2px;
            text-align: center !important;
        }
        .btn-social{
            color: white;
            border-radius: 50%;
            border: 2px white solid;
            width: 55px !important; 
            height: 55px !important; 
            margin-left: 4px;
            margin-right: 4px;
            background-color: transparent;

        }
        .btn-social i{
            font-size: 25px;
        }

        .panel-label{
            text-align: center;
        }


        .action-select{
            margin-bottom: 0.4rem;
        }

        .logo-encuesta{
        
            width: 100% !important;
            background-color: aquamarine;
            text-align: center;
        }

        .logo-encuesta img{
            max-width: 80%;
            max-height: 80%;
            margin-top: 10px;
        }

        .encuesta{

            background-color: #cceaec;
        }
        .pregunta{

            background-color: #6f6f6f;
            padding: 8px;
            text-align: center;
        }

        .contenido{
            height: 85%;
            background-color: burlywood;
            padding: 30px;
        }

        .btn-container{
            text-align: center;
        }

        .btn-container button{
            margin-top: 15px;
        }

        .super-container{
            padding: 0;
        }

        .md-color-picker-input{
            display:none !important;
        }
        md-input-container{
            display:none !important;
        }

        md-color-picker .md-color-picker-input-container .md-color-picker-preview, [md-color-picker] .md-color-picker-input-container .md-color-picker-preview
        {
            width: 60px;
            height: 20px;
            border: 2px solid #fff;
            border-radius: 10px;
            margin-top: -27px;

        }

        .md-color-picker-clear{
            display: none;
        }

        </style>

	</head>

	<body ng-controller="splashCtrl as vm">

        <div class="input_container" ng-show="vm.step_wizard == 0" ng-style="{'background-color': vm.colorFondoEncuesta }">
            <div class="super-container col-md-12" ng-style="{'color': vm.colorTexto,'font-size':vm.medidaTexto}" class="m-scrollable m-scrollable--track m-scroller ps ps--active-y" data-scrollable="true" >
                <div class="logo-encuesta" ng-style="{'height': vm.encuesta_image+'px','background-color':vm.estilo_encuesta.color_fondo_logo}">
                    <img src="{{vm.estilo_encuesta.imagen}}" class="image-responsive">
                </div>
                <div class="encuesta"  ng-repeat="pregunta in vm.preguntas" ng-show="vm.step == $index">

                <div class="pregunta" ng-style="{'background-color':vm.estilo_encuesta.color_fondo_pregunta,'height': vm.encuesta_pregunta+'px'}">
                <b><span style="color:white;" ng-style="{'color':vm.estilo_encuesta.color_texto_pregunta,'font-size':'22px'}"> {{pregunta.pregunta}}</span></b> 
                </div>
                <div class="contenido" ng-style="{'height': vm.encuesta_opciones+'px','background-color': vm.estilo_encuesta.color_fondo_opciones}">
                        
                        <div ng-switch="pregunta.tipo">

                        <div class="m-portlet__body" ng-switch-when="1">
                            <div class="form-group m-form__group">
                                <label >{{opcion}}</label>
                                <input type="text" ng-model="vm.respuesta.opciones[0]" class="form-control m-input m-input--air m-input--pill"  >
                            </div>
                        </div>
                        <div class="m-radio-list" ng-switch-when="2">
                            <label class="m-radio m-radio--bold m-radio--state-brand" ng-repeat="opcion in pregunta.opciones" ng-style="{'color': vm.estilo_encuesta.color_texto_opciones,'font-size':'18px'}">
                                <input type="radio" ng-model="vm.respuesta.opciones[0]" name ="{{pregunta.pregunta | lowercase}}"  value="{{opcion.opcion}}" ng-click="vm.respuestax = opcion.accion"> {{opcion.opcion}}
                                <span ng-style="{'border': '1px solid '+ vm.estilo_encuesta.color_texto_opciones}"></span>
                            </label>
                        </div>
                        <div class="m-checkbox-list" ng-switch-when="3">
                            <label class="m-checkbox m-checkbox--bold m-checkbox--state-brand" ng-repeat="opcion in pregunta.opciones">
                                <input type="checkbox" ng-model="vm.respuesta.opciones[$index]" ng-true-value="'{{opcion.opcion}}'"> {{opcion.opcion}}
                                <span ng-style="{'border': '1px solid '+ vm.estilo_encuesta.color_texto_opciones}"></span>
                            </label>
                        </div>

                        </div>
                        <div class="btn-container">
                            <button type="button" ng-style="{'background-color':  vm.estilo_encuesta.color_boton}" ng-click="vm.addRespuesta(pregunta);vm.accion()" ng-show="vm.step+1 < vm.preguntas.length" class="btn m-btn--square  btn-primary m-btn m-btn--custom m-btn--bolder m-btn--uppercase" ><i class="fa fa-plus-circle"></i> Continuar</button>
                            <button type="button" ng-style="{'background-color':  vm.estilo_encuesta.color_boton}" ng-click="vm.addRespuestas(pregunta);vm.accion()" ng-show="vm.step+1 == vm.preguntas.length" class="btn m-btn--square  btn-primary m-btn m-btn--custom m-btn--bolder m-btn--uppercase" ><i class="fa fa-plus-circle"></i> Finalizar</button>
                        </div>
                </div>
                </div>
                                                    
            </div>
        </div>
        <div class="main-container" ng-show="vm.step_wizard == 1" >
            <div class=" splash-image" ng-style="{'height': vm.image_height+'px', 'background': 'url(' +vm.splash.configuracion.imagen.imagen+ ') no-repeat '}">
                <div  ng-style="{'height': vm.img_height+'px'}">

                </div>
                
                <div class=" splash-panel" ng-style="{'background-color': vm.splash.configuracion.panel.color_fondo,'height': vm.panel_height+'px'}">
                    <div class="col-md-12">
                        <b><label class="panel-label" ng-style="{'color': vm.splash.configuracion.panel.color_texto}">Accede a internet con una de las siguientes opciones</label></b>
                        <div class="btns-social" ng-switch="tipo_registro" ng-if="vm.block == false">
                            <button class="button btn-social" ng-click="vm.step_wizard = vm.step_wizard + 1"><i class="fa fa-envelope"></i></button>
                            <button class="button btn-social" ng-click="vm.facebook()"><i class="fab fa-facebook-f"></i></button>
                        </div>        
                    </div>
                </div>
            </div>

            <div class=" splash-footer" ng-style="{'background-color': vm.splash.configuracion.footer.color_fondo ,'height': vm.footer_height+'px'}">
                <div class="col-md-12 terminos-container">
                    <label ng-style="{'color': vm.splash.configuracion.footer.color_texto}">Si entras estaras aceptando los  <ins>terminos y condiciones</ins></label>
                </div>
                <div class="col-md-12 logo-container">
                    <img src="app/assets/conectalia.png" class="img-responsive logo">
                </div>
            </div>
        </div>
        <div class="container" ng-show="vm.step_wizard == 2">
            <div class="form-area">  
                <form role="form" ng-submit="vm.leadForm()">
                <br style="clear:both">
                    <h3 style="margin-bottom: 25px; text-align: center;">Registrate para navegar gratis</h3>
                    <div class="form-group">
                        <label >Nombre:</label>
                        <input type="text" class="form-control" ng-model="vm.data_form.nombre" required>
                    </div>
                    <div class="form-group">
                        <label >Sexo:</label>
                        <br>
                        <label class="radio-inline ">
                        <input type="radio" ng-model="vm.data_form.sexo" value="Masculino" name="sexo" checked>Masculino
                        </label>
                        <label class="radio-inline">
                        <input type="radio" ng-model="vm.data_form.sexo" value="Femenino" name="sexo">Femenino
                        </label>
                    </div>
                    <div class="form-group">
                        <label >Edad:</label>
                    
                        <input type="number" class="form-control" ng-model="vm.data_form.edad"  required>
                        <!--<select ng-model="vm.data_form.edad" class="form-control" ng-options="n for n in [] | range:5:80"></select>-->
                    </div>
                    <div class="form-group">
                        <label >Correo electrónico:</label>
                        <input type="email" class="form-control" ng-model="vm.data_form.correo" required>
                    </div>
                    <div class="form-group">
                        <label >Telefóno:</label>
                        <input type="text" class="form-control" ng-model="vm.data_form.telefono"  required>
                    </div>
                
                    
                <input type="submit" class="btn btn-primary pull-right" value="Registrarme"/>
                </form>
            </div>
        </div>

	</body>

</html>

