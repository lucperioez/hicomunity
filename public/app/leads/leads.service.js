(function(){

    'use strict'

    function leadsService(config,$http,$cookies,$q){
        

       this.getByModem = function(modem){
           var token = $cookies.get('tkn')   
           var url = '/leads/'+modem;

           return $http({
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'token' : token
            }    
            });
       }

      
    }

    angular
       .module('hicomunity.panel')
       .service('leadsService', leadsService);

}());