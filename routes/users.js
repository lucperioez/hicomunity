var express = require('express');
var router = express.Router();
const db = require('../config/db');
require("../models/M_Users.js");
const mongoose = require('mongoose');
mongoose.connect(db.getConection(), { useNewUrlParser: true });
const User = mongoose.model("Usuarios");
const fs = require("fs");
const path = require('path');
/* GET users listing. */
router.get('/',function(req, res, next) {
  let query = {};
  if(req.user.tipo == 1){
    query = {
      _id : { $ne: req.user._id }
    }
  }else if(req.user.tipo == 2){
    query = {
      agencia : req.user._id 
    }
  }else if(req.user.tipo == 3){
    res.json({succes : false,mensaje:'El cliente no tiene usuarios'});
  }
  User.find(query,'-password').populate('agencia', '-password').exec(function(err,users){
    if(err){
      res.json({succes : false,mensaje:'A ocurrido un error',error:err});
    }else{
      res.json({succes : true,users});
    }
  });
});

router.get('/:id',function(req, res, next) {
  User.findById(req.params.id).populate('agencia', '-password').exec(function(err,user){
    if(err){
      res.json({succes : false,mensaje:'A ocurrido un error',error:err});
    }else{
      if(user){
        user.password = undefined;
        res.json({succes : true,mensaje:'Usuario encontrado',user});
      }else{
        res.json({succes : false,mensaje:'El usuario no se ha encontrado'});
      } 
    }
  });
});

router.post('/',function(req,res,next){
  if(req.body.logo_data){
    let pathImg = `public/images/user/${req.params.id}-${req.body.logo}`;
    let base64Data = req.body.logo_data.split(';base64,').pop();
    fs.writeFileSync(pathImg, base64Data, {encoding: 'base64'});
    req.body.logo = `/images/user/${req.params.id}-${req.body.logo}`;
  }
  let nUser = new User(req.body);
  nUser.save(function(error,user){
    if(error){
      res.json({succes : false,mensaje:'A ocurrido un error',error});
    }else{
      res.json({succes : true,mensaje:'Usuario agregado con exito',user});
    }
  });
});

router.put('/:id',function(req,res,next){
  User.findById(req.params.id).exec(function(err,user){
    if(err){
      res.json({succes : false,mensaje:'A ocurrido un error',error:err});
    }else{
      //user = req.body;
      if(req.body.logo_data){
        if(user.logo){
          let temp_path =  `public${user.logo}`; 
          fs.exists(temp_path, function(exists) {
            if(exists) {
              console.log(temp_path);
              fs.unlinkSync(temp_path);
              console.log('se borro');
            } 
          });
        }
        let pathImg = `public/images/user/${req.params.id}-${req.body.logo}`;
        let base64Data = req.body.logo_data.split(';base64,').pop();
        fs.writeFileSync(pathImg, base64Data, {encoding: 'base64'});
        req.body.logo = `/images/user/${req.params.id}-${req.body.logo}`;
      }
        
      user.pagos = req.body.pagos || user.pagos;
      user.nombre = req.body.nombre || user.nombre;
      user.contacto = req.body.contacto || user.contacto;
      user.rfc = req.body.rfc || user.rfc;
      user.direccion = req.body.direccion || user.direccion;
      user.telefono = req.body.telefono || user.telefono;
      user.email = req.body.email || user.email;
      user.password = req.body.password || user.password;
      user.tipo = req.body.tipo || user.tipo;
      user.estatus = req.body.estatus || user.estatus;
      user.agencia = req.body.agencia || user.agencia;
      user.logo = req.body.logo || user.logo;

      user.save(function(error,usr){
        if(error){
          res.json({succes : false,mensaje:'A ocurrido un error',error});
        }else{
          usr.password = undefined;
          res.json({succes : true,mensaje:'Usuario actualizado con exito',user:usr});
        }
      });
      
    }
  });

});

router.delete('/:id',function(req,res,next){
  User.deleteOne({ _id: req.params.id }, function (err) {
    if(err){
      res.json({success: false, message:'A ocurrido con error', error:err});
    }else{
      res.json({success: true, message:'Usuario eliminado con exito'});
    } 
  });
});

router.put('/cambiarPassword/:id',function(req,res,next){
  User.findById(req.params.id).exec(function(err,user){
    if(err){
      res.json({succes : false,mensaje:'A ocurrido un error',error:err});
    }else{
      //user = req.body;
      user.comparePassword(req.body.password,function(error,sonIguales){
        if(error){
          res.json({succes : false,mensaje:'A ocurrido un error',error});
        }else{
          if(sonIguales){
            //cambiar password
            user.password = req.body.password2;
            user.save(function(error,usr){
              if(error){
                res.json({succes : false,mensaje:'A ocurrido un error',error});
              }else{
                usr.password = undefined;
                res.json({succes : true,mensaje:'Password actualizada con exito',user:usr});
              }
            });
          }else{
            res.json({succes : false,mensaje:'El password actual no corresponde'});
          }
        }
      });
      
    }
  });

});

module.exports = router;
