(function(){

     'use strict'

     function panelStates($stateProvider,$urlRouterProvider){


         $stateProvider

            .state('dashboard', {
                url: '/dashboard',
                templateUrl: 'app/panel/panel.html',
                controller: 'panelCtrl as vm'
            })

            .state('usuarios', {
                url: '/usuarios',
                templateUrl: 'app/usuarios/usuarios.html',
                controller: 'usuariosCtrl as vm'
            })

            .state('encuesta', {
                url: '/encuesta/{id_encuesta}',
                templateUrl: 'app/encuestas/encuesta.html',
                controller: 'encuestaCtrl as vm'
            })

            .state('estadisticas', {
                url: '/estadisticas/{id_encuesta}',
                templateUrl: 'app/encuestas/resultados.html',
                controller: 'resultadosCtrl as vm'
            })

            .state('encuestas', {
                url: '/encuestas',
                templateUrl: 'app/encuestas/listado-encuestas.html',
                controller: 'encuestasCtrl as vm'
            })

            .state('configuracion', {
                url: '/configuracion',
                templateUrl: 'app/configuracion/configuracion.html',
                controller: 'configuracionCtrl as vm'
            })

            .state('splasheditor', {
                url: '/splasheditor/{id_splash}/{id_cliente}',
                params: {
                    id_splash: null,
                    id_cliente: null
                },
                templateUrl: 'app/splashpage/splasheditor.html',
                controller: 'splasheditorCtrl as vm'
            })


            .state('modems', {
                url: '/modems',
                templateUrl: 'app/modem/modems.html',
                controller: 'modemsCtrl as vm'
            })

            .state('splashpages', {
                url: '/splashpages',
                templateUrl: 'app/splashpage/splash-list.html',
                controller: 'splashlistCtrl as vm'
            })

            .state('calendar', {
                url: '/calendar',
                templateUrl: 'app/calendar/calendar.html',
                controller: 'calendarCtrl as vm'
            })

            .state('campain', {
                url: '/campain/{id_campain}/{id_cliente}/{modem}',
                params: {
                    id_campain: null,
                    id_cliente: null,
                    plaza : null
                },
                templateUrl: 'app/calendar/campain.html',
                controller: 'campainCtrl as vm'
            }) 

            .state('email-marketing', {
                url: '/email-marketing',
                templateUrl: 'app/e-marketing/mail-marketing.html',
                controller: 'mailmarketingCtrl as vm'
            })

            .state('mail-editor', {
                url: '/mail-editor/{plantillaId}/{clienteId}',
                templateUrl: 'app/mail_editor/templates/mail-editor.html',
                controller: 'mailEditorCtrl as vm'
            })

            .state('my-templates', {
                url: '/my-templates',
                templateUrl: 'app/mail_editor/templates/template-list.html',
                controller: 'templateListCtrl as vm'
            })

            .state('sms-marketing', {
                url: '/sms-marketing',
                templateUrl: 'app/e-marketing/sms-marketing.html',
                controller: 'smsmarketingCtrl as vm'
            })

            .state('whatsapp-marketing', {
                url: '/whatsapp-marketing',
                templateUrl: 'app/e-marketing/whatsapp-marketing.html',
                controller: 'whatsappmarketingCtrl as vm'
            })

            .state('conexiones', {
                url: '/conexiones',
                templateUrl: 'app/conexiones/conexiones.html',
                controller: 'conexionesCtrl as vm'
            })

            .state('leads', {
                url: '/leads',
                templateUrl: 'app/leads/leads.html',
                controller: 'leadsCtrl as vm'
            })


            $urlRouterProvider.otherwise('/dashboard');


     }

    function auth($cookies,requestService,$state,$rootScope,$http,indexService,$location){
        $rootScope.$on('$locationChangeStart',function($event,next,current,toState){
            if($cookies.get('usr_dta') == undefined && $cookies.get('tkn') == undefined){
                window.location.href="login.html";
            }
            
            /*angular.forEach($http.pendingRequests, function(request) {
                if (request.cancel && request.timeout) {
                   request.cancel.resolve();
                }
             });*/

        });

        /*$rootScope.$on('$routeChangeSuccess', function(evt, toState, absOldUrl) {
            console.log(toState.name);
            indexService.getMenu($cookies.getObject('usr_dta').tipo).then(function successCallback(response){
                let permissions = response.data.permissions;
                if(permissions.indexOf(toState.name) == -1){
                    $event.preventDefault();
                    $state.go('dashboard');
                }
            });
        })*/
    }

     angular
        .module('hicomunity.panel')
        .config(panelStates)
        .run(auth);

 }());
