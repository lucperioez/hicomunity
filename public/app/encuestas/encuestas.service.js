(function(){

    'use strict'

    function encuestasService(config,$http,$cookies,$q,$stateParams){
        

       this.obtenerEncuestas = function(_id){
           var token = $cookies.get('tkn') 
           var user_data = $cookies.getObject('usr_dta');  
           var url = '/encuestas/'+_id;

           return $http({
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'token' : token
            }    
            });
       }

       this.obtenerEncuesta = function(){
            var token = $cookies.get('tkn') 
            var url = '/encuesta/'+$stateParams.id_encuesta;

            return $http({
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'token' : token
            }    
            });
        }

        this.encuestaResultados = function(id_encuesta){
            var token = $cookies.get('tkn') 
            var url = '/encuesta/respuestas/'+id_encuesta;

            return $http({
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'token' : token
            }    
            });
        }

        this.nuevaEncuesta = function(encuesta){
            var token = $cookies.get('tkn') 
            var url = '/encuesta';

            return $http({
            method: 'POST',
            url: url,
            data : encuesta,
            headers: {
                'Content-Type': 'application/json',
                'token' : token
            }    
            });
        }

        this.editarEncuesta = function(encuesta){
            var token = $cookies.get('tkn') 
            var url = '/encuesta/'+encuesta._id;

            return $http({
                method: 'PUT',
                url: url,
                data : encuesta,
                headers: {
                    'Content-Type': 'application/json',
                    'token' : token
                }    
            });
        }
        
        this.eliminarEncuesta = function(encuesta){
            var token = $cookies.get('tkn') 
            var url = '/encuesta/'+encuesta._id;

            return $http({
                method: 'DELETE',
                url: url,
                data : encuesta,
                headers: {
                    'Content-Type': 'application/json',
                    'token' : token
                }    
            });
        }

    }

    angular
       .module('hicomunity.panel')
       .service('encuestasService', encuestasService);

}());