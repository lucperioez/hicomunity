var express = require('express');
var router = express.Router();
const db = require('../config/db');
require("../models/M_Encuestas");
require("../models/M_Leads");
const mongoose = require('mongoose');
mongoose.connect(db.getConection(), { useNewUrlParser: true });
const Encuesta = mongoose.model("Encuestas");
const Lead = mongoose.model("Leads");
var passport = require('passport');
var auth= require('../config/passport');

router.get('/:id',auth.authenticationAPI ,function(req, res, next) {
    Encuesta.findById(req.params.id).populate('lead').exec(function(err,encuesta){
        if(err){
        res.json({succes : false,mensaje:'A ocurrido un error',error:err});
        }else{
            if(encuesta){
                res.json({succes : true,mensaje:'Encuesta encontrada',encuesta});
            }else{
                res.json({succes : false,mensaje:'La encuesta no se ha encontrado'});
            } 
        }
    });
});
router.get('/respuestas/:id',auth.authenticationAPI ,function(req, res, next) {
  Encuesta.findById(req.params.id).exec(async function(err,encuesta){
      if(err){
      res.json({succes : false,mensaje:'A ocurrido un error',error:err});
      }else{
          if(encuesta){
            let mEncuesta = encuesta.toObject();
            for (i in mEncuesta.respuestas) {
              mEncuesta.respuestas[i].lead = await Lead.findOne({mac:mEncuesta.respuestas[i].mac}).exec();
            }
            res.json({succes : true,mensaje:'Encuesta encontrada',encuesta:mEncuesta});
          }else{
              res.json({succes : false,mensaje:'La encuesta no se ha encontrado'});
          } 
      }
  });
});

router.post('/',auth.authenticationAPI ,function(req,res,next){
  let nEncuesta = new Encuesta(req.body);
  nEncuesta.save(function(error,encuesta){
    if(error){
      res.json({succes : false,mensaje:'A ocurrido un error',error});
    }else{
      res.json({succes : true,mensaje:'Encuesta creada con exito',encuesta});
    }
  });
});

router.put('/:id',auth.authenticationAPI ,function(req,res,next){
    Encuesta.update({ _id: req.params.id }, { $set: req.body },function(err,result){
        if(err){
          res.json({success: false, message:'An error has occurred',error:err});
        }else{
          res.json({success: true, message:'Encuesta actualizada con exito',result});
        } 
    });
});
router.post('/:id/respuesta',function(req,res,next){
  Encuesta.update({ _id: req.params.id }, { $push : { respuestas : req.body }},function(err,result){
      if(err){
        res.json({success: false, message:'An error has occurred',error:err});
      }else{
        res.json({success: true, message:'Respuesta agregada con exito',result});
      } 
  });
});

router.delete('/:id',auth.authenticationAPI ,function(req,res,next){
  Encuesta.deleteOne({ _id: req.params.id }, function (err) {
    if(err){
      res.json({success: false, message:'A ocurrido con error', error:err});
    }else{
      res.json({success: true, message:'Encuesta eliminada con exito'});
    } 
  });
});

module.exports = router;
