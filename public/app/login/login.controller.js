(function(){

     'use strict'
     
  function loginCtrl($cookies,loginService){
         
        var ctrl = this;
        ctrl.usuario = "Lucio";
        toastr.options = {
            "positionClass": "toast-top-right",
            "showDuration": "200",
        };
        ctrl.singIn = function(){
            loginService.singIn(ctrl.user,ctrl.password)
            .then(function(response){
                  console.log(response);
                  if(response.data.success){
                        $cookies.putObject('usr_dta', response.data.data);
                        $cookies.put('tkn', response.data.token);
                        setTimeout(function(){
                              window.location.href="index.html";
                        },2000);
                        toastr.success(response.data.message, "");
                  }else{
                        toastr.error(response.data.message, "Intenta nuevamente");
                  }
                  //console.log(response.data.data);
                  //$cookies.putObject('usr_dta', response.data.data);
                  //$cookies.put('tkn', response.data.token);
                  //window.location.href="index.html";
            })
            .catch(function(response){   
                  toastr.error(response.data.message, "Intenta nuevamente");
                  ctrl.password = "";
            });   
        }


  }
        
    angular
        .module('hicomunity.login')
        .controller('loginCtrl',loginCtrl)
 }());
 