(function(){

     'use strict'
     
  function campainCtrl(splashpagesService,encuestasService,$cookies,$stateParams,$state,calendarService,usuariosService){
         
    var ctrl = this;
    ctrl.user_data = $cookies.getObject('usr_dta');
    ctrl.id_campain = $stateParams.id_campain;
    ctrl.id_cliente = $stateParams.id_cliente;
    ctrl.folio_modem = $stateParams.modem;

    ctrl.edit = ctrl.id_campain != null;

    ctrl.splashs = [];
    ctrl.encuestas = [];

    ctrl.campain = {};
    ctrl.campain.cliente = ctrl.id_cliente;
    ctrl.campain.folio_modem = ctrl.folio_modem;
    ctrl.campain.encuestas = [];
    ctrl.campain.splashs = [];
    

    //obtener Splashpages
    splashpagesService.obtenerSplashpages(ctrl.id_cliente).then(function(response){
      console.log(response);
      if(response.data.succes){
        ctrl.splashs = response.data.splashs;
      }
    }).catch(function(response){
      console.log(response);
    });
    //obtener encuestas
    encuestasService.obtenerEncuestas(ctrl.id_cliente).then(function(response){
      console.log(response);
      if(response.data.succes){
        ctrl.encuestas = response.data.encuestas;
      }
    }).catch(function(response){
      console.log(response);
    });


    if(ctrl.edit){
      //atraer campaña
      setTimeout(function(){
        calendarService.getCampain(ctrl.id_campain).then(function(response){
          //console.log(response);
          if(response.data.succes){
            ctrl.campain = response.data.campain;
            ctrl.campain.fecha_inicio = ctrl.convertDate2(ctrl.campain.fecha_inicio);
            ctrl.campain.fecha_fin = ctrl.convertDate2(ctrl.campain.fecha_fin);
            var sp = ctrl.campain.splashs;
            ctrl.campain.splashs = [];
            sp.forEach(s => {
              ctrl.addSplash(s);
            });
            var en = ctrl.campain.encuestas;
            ctrl.campain.encuestas = [];
            en.forEach(e=>{
              ctrl.addEncuesta(e);
            });

          }
        }).catch(function(response){
          console.log(response);
        });
      },1000);
    }else{
      //obtener agencia
      usuariosService.getUsuario(ctrl.id_cliente).then(function(response){
        console.log(response);
        if(response.data.succes){
          ctrl.campain.agencia = response.data.user.agencia._id;
  
        }
      }).catch(function(response){
        console.log(response);
      });
    }
    
    ctrl.convertDate = function(date,h,m,s){
      var parts = date.split('/');
      return new Date(parts[2], parts[1] - 1, parts[0],h,m,s); 
    }
    
    ctrl.convertDate2 = function(date){
      date = new Date(date);
      return date.getDate()+'/'+(date.getMonth()+1)+'/'+date.getFullYear()
    }
  

    ctrl.guardar = function(){
      ctrl.campain.encuestas = ctrl.campain.encuestas.map(e=>e._id);
      ctrl.campain.splashs = ctrl.campain.splashs.map(s=>s._id);

      ctrl.campain.fecha_inicio = ctrl.convertDate(ctrl.campain.fecha_inicio,0,0,0);
      ctrl.campain.fecha_fin = ctrl.convertDate(ctrl.campain.fecha_fin,23, 59,59);
      console.log(ctrl.campain);
      if(ctrl.edit){
        //editar
        calendarService.editarCampain(ctrl.campain).then(function(response){
          console.log(response);
          if(response.data.success){
            $state.go('calendar');
          }
        }).catch(function(response){
          console.log(response);
        });

      }else{
        //nuevo
        calendarService.guardarCampain(ctrl.campain).then(function(response){
          console.log(response);
          if(response.data.succes){
            $state.go('calendar');
          }
        }).catch(function(response){
          console.log(response);
        });
      }
    }

    ctrl.addSplash = function(splash){
      ctrl.campain.splashs.push(splash);
      ctrl.splashs = ctrl.splashs.filter(s=>s._id!=splash._id);
    }
    ctrl.removeSplash = function(splash){
      ctrl.splashs.push(splash);
      ctrl.campain.splashs = ctrl.campain.splashs.filter(s=>s._id!=splash._id);
    }

    ctrl.addEncuesta = function(encuesta){
      ctrl.campain.encuestas.push(encuesta);
      ctrl.encuestas = ctrl.encuestas.filter(e=>e._id!=encuesta._id);
    }
    ctrl.removeEncuesta = function(encuesta){
      ctrl.encuestas.push(encuesta);
      ctrl.campain.encuestas = ctrl.campain.encuestas.filter(e=>e._id!=encuesta._id);
    }
  }
        
    angular
        .module('hicomunity.panel')
        .controller('campainCtrl',campainCtrl)
 }());
