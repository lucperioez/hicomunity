(function(){

     'use strict'

     function panelStates($stateProvider,$urlRouterProvider){


         $stateProvider

            

            .state('splash', {
                url: '/splash/{id_modem}/{id_cliente}',
                params: {
                    id_modem: null,
                    id_cliente: null
                },
                templateUrl: 'app/splash/splash.html',
                controller: 'splashCtrl as vm'
            })


            $urlRouterProvider.otherwise('/splash');


     

   
    }

     angular
        .module('hicomunity.splashpage')
        .config(panelStates)
       


 }());
