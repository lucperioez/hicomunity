var express = require('express');
var router = express.Router();
const db = require('../config/db');
require("../models/M_SplashPages");
const mongoose = require('mongoose');
mongoose.connect(db.getConection(), { useNewUrlParser: true });
const SplashPages = mongoose.model("SplashPages");


router.get('/:id',function(req, res, next) {
    SplashPages.findById(req.params.id).populate('lead').exec(function(err,splash){
        if(err){
        res.json({succes : false,mensaje:'A ocurrido un error',error:err});
        }else{
            if(splash){
                res.json({succes : true,mensaje:'Splashpage encontrado',splash});
            }else{
                res.json({succes : false,mensaje:'El Splashpage no se ha encontrado'});
            } 
        }
    });
});

router.post('/',function(req,res,next){
  let nSplashPages = new SplashPages(req.body);
  nSplashPages.save(function(error,splash){
    if(error){
      res.json({succes : false,mensaje:'A ocurrido un error',error});
    }else{
      res.json({succes : true,mensaje:'SplashPage creado con exito',splash});
    }
  });
});

router.put('/:id',function(req,res,next){
    SplashPages.update({ _id: req.params.id }, { $set: req.body },function(err,result){
        if(err){
          res.json({success: false, message:'An error has occurred',error:err});
        }else{
          res.json({success: true, message:'SplashPage actualizado con exito',result});
        } 
    });
});

router.delete('/:id',function(req,res,next){
    SplashPages.deleteOne({ _id: req.params.id }, function (err) {
        if(err){
        res.json({success: false, message:'A ocurrido con error', error:err});
        }else{
        res.json({success: true, message:'SplashPage eliminado con exito'});
        } 
    });
});

module.exports = router;
