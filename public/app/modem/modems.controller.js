(function(){

     'use strict'
     
  function modemsCtrl(modemsService,NgTableParams,$scope,usuariosService,$cookies){
         
    var ctrl = this;
    ctrl.user_data = $cookies.getObject('usr_dta');
    ctrl.modems = [];
    ctrl.modems_clientes = [];
    ctrl.agencias = [];
    ctrl.clientes = [];
    ctrl.usuarios = [];
    ctrl.agencia = '';
    ctrl.cliente = '';
    ctrl.modem = {};
    ctrl.modem_e = {};
    ctrl.admin = ctrl.user_data.tipo == 1;
    ctrl.url_tanaza = '';
    ctrl.successful = false;

    usuariosService.get().then(function(response){
        console.log(response);
        let data = response.data.users;
        ctrl.usuarios = data;
        if(ctrl.user_data.tipo == 2){
            ctrl.clientes = data;
        }else{  
            ctrl.agencias = data.filter((user)=>user.tipo==2);
        }
    }).catch(function(response){   
        toastr.options = {
        "positionClass": "toast-top-right",
        "showDuration": "200",
        };
        toastr.error(response.data.message, "");
    });

    ctrl.getCliente = function(){
        if(ctrl.agencia != ''){
            ctrl.clientes = ctrl.usuarios.filter((user)=> (user.tipo==3) && (user.agencia._id==ctrl.agencia) );
        }else{
            ctrl.cliente = '';
            ctrl.cliente_nombre = '';
            ctrl.clientes = [];
            ctrl.modems_clientes = [];
        }
        
    }

    

    ctrl.getModems = function(){
        modemsService.obtenerModems(ctrl.user_data._id).then(function(response){
            console.log(response);
            ctrl.modems =response.data.modems.filter((m)=>m.usuario==undefined);
        }).catch(function(response){   
            toastr.options = {
            "positionClass": "toast-top-right",
            "showDuration": "200",
            };
            toastr.error(response.data.message, "");
        });
    }();

    ctrl.getModemsCliente = function(){
        ctrl.cliente_nombre = ctrl.clientes.filter((user)=> (user._id==ctrl.cliente))[0].nombre;
        modemsService.obtenerModems(ctrl.cliente).then(function(response){
            console.log(response);
            ctrl.modems_clientes = response.data.modems;
        }).catch(function(response){   
            toastr.options = {
            "positionClass": "toast-top-right",
            "showDuration": "200",
            };
            toastr.error(response.data.message, "");
        });
    }

    ctrl.agregarModem = function(){
        console.log(ctrl.modem);
        ctrl.modem.folio = ctrl.modem.folio.toUpperCase();
        modemsService.nuevoModem(ctrl.modem).then(function(response) {
            console.log(response);
            if(response.data.succes){
                ctrl.modems.push(response.data.modem);
                $('#m_modal_4').modal('hide');              
                toastr.options = {
                    "positionClass": "toast-top-right",
                    "showDuration": "200",
                };
                toastr.success(response.data.mensaje || response.data.message, "");
            }
        }).catch(function(response){   
            toastr.options = {
            "positionClass": "toast-top-right",
            "showDuration": "200",
            };
            toastr.error(response.data.message, "");
        });
    }
    ctrl.asignar = function(modem){
        ctrl.modem_e = modem;
        $('#modal_edit').modal('show');
    }
    ctrl.asignarModem = function(){
        ctrl.modem_e.usuario = ctrl.cliente;
        ctrl.modem_e.agencia = ctrl.agencia;
        modemsService.asignarModem(ctrl.modem_e).then(function(response){
            console.log(response);
            if(response.data.success){
                ctrl.modems = ctrl.modems.filter((m)=>m._id != ctrl.modem_e._id);
                ctrl.modems_clientes.push(ctrl.modem_e);           
                toastr.options = {
                    "positionClass": "toast-top-right",
                    "showDuration": "200",
                };
                toastr.success(response.data.message, "");
            }
        }).catch(function(response){
            toastr.options = {
            "positionClass": "toast-top-right",
            "showDuration": "200",
            };
            toastr.error(response.data.message, "");
        });
    }
    ctrl.urlTanaza = function (m){
        //ctrl.url_tanaza = window.location.hostname+":"+window.location.port+"/splashpage.html#!/splash/"+m.folio+"/"+m.usuario
        ctrl.url_tanaza = window.location.hostname+":3000/splashpage.html#!/splash/"+m.folio+"/"+m.usuario
        $('#modal_tanaza').modal('show');
    }
    ctrl.cerrarModalTanaza = function(){
        ctrl.url_tanaza = '';
        ctrl.successful = false;
        $('#modal_tanaza').modal('hide');
    }
    ctrl.copied = function(){
        try {
            var emailLink = document.querySelector('#tanaza-url');  
            var range = document.createRange();  
            range.selectNode(emailLink);  
            window.getSelection().addRange(range);
            emailLink.focus();        
            ctrl.successful = document.execCommand('copy');
        } catch (error) {
            ctrl.successful = false;
        }
    }
  }
        
    angular
        .module('hicomunity.panel')
        .controller('modemsCtrl',modemsCtrl)
 }());
