(function(){

     'use strict'

     function loginStates($stateProvider,$urlRouterProvider){


         $stateProvider

            .state('login', {
                url: '/login',
                templateUrl: 'app/login/login.html',
                controller: 'loginCtrl as vm'
            })

            $urlRouterProvider.otherwise('/login');


     }

     function auth($cookies){
        if($cookies.get('usr_dta') == undefined && $cookies.get('tkn') == undefined){
            
        }else{
            window.location.href="index.html";
        }
    }

     angular
        .module('hicomunity.login')
        .config(loginStates)
        .run(auth);

 }());
