var express = require('express');
var router = express.Router();
const db = require('../config/db');
require("../models/M_Conexiones");
require("../models/M_Users");
const mongoose = require('mongoose');
mongoose.connect(db.getConection(), { useNewUrlParser: true });
const Conexiones = mongoose.model("Conexiones");
const Usuarios = mongoose.model("Usuarios");
var passport = require('passport');
var auth= require('../config/passport');

//por cliente y por modem

router.get('/cliente/:id',auth.authenticationAPI,function(req, res, next) {
    let query = {cliente:req.params.id};
    if(req.query.fechaInicio && req.query.fechaFin){
        query.createdAt = { 
            '$lte' : new Date(req.query.fechaFin),
            '$gte' : new Date(req.query.fechaInicio)
        }
    }
    Conexiones.countDocuments(query).exec(function(err,conexiones){
        if(err){
            res.json({succes : false,mensaje:'A ocurrido un error',error:err});
        }else{
            res.json({succes : true,mensaje:'Conexiones por cliente',conexiones});
        }
    });
});
router.get('/modem/:folio',auth.authenticationAPI,function(req, res, next) {
    let query = {folio_modem:req.params.folio};
    if(req.query.fechaInicio && req.query.fechaFin){
        query.createdAt = { 
            '$lte' : new Date(req.query.fechaFin),
            '$gte' : new Date(req.query.fechaInicio)
        }
    }
    Conexiones.countDocuments(query).exec(function(err,conexiones){
        if(err){
            res.json({succes : false,mensaje:'A ocurrido un error',error:err});
        }else{
            res.json({succes : true,mensaje:'Conexiones por modem',conexiones});
        }
    });
});

router.post('/', function(req,res,next){  
  
  const user =  Usuarios.findById(req.body.cliente).exec();
  req.body.agencia = user.agencia;
  
  let nConexiones = new Conexiones(req.body);
  nConexiones.save(function(error,conexion){
    if(error){
      res.json({succes : false,mensaje:'A ocurrido un error',error});
    }else{
      res.json({succes : true,mensaje:'Conexión creada con exito',conexion});
    }
  });
});

module.exports = router;
