(function(){

    'use strict'

    function conexionesService(config,$http,$cookies,$q){
        

       this.getConexionesByModem = function(modem){
           var token = $cookies.get('tkn')   
           var url = '/conexiones/'+modem;

           return $http({
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'token' : token
            }    
            });
       }

       this.bloquearMac = function(mac){
           var token = $cookies.get('tkn')   
           var url = '/blocked';

           return $http({
                method: 'POST',
                url: url,
                data: {mac},
                headers: {
                    'Content-Type': 'application/json',
                    'token' : token
                }    
            });

       }
       
       this.desbloquearMac = function(mac){
        var token = $cookies.get('tkn')   
        var url = '/blocked/'+mac;

        return $http({
             method: 'DELETE',
             url: url,
             headers: {
                 'Content-Type': 'application/json',
                 'token' : token
             }    
         });

    }
    }

    angular
       .module('hicomunity.panel')
       .service('conexionesService', conexionesService);

}());