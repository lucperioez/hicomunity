const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const passport = require('passport');
const db = require('../config/db');

const url = require("url");
const https = require("https");

require("../models/M_Encuestas");
require("../models/M_SplashPages");
require("../models/M_Modems");
require("../models/M_Impactos");
require("../models/M_Conexiones");
require("../models/M_Leads");
require("../models/M_Campain");
require("../models/M_BlockedMacs");
require("../models/M_Templates");

const mongoose = require('mongoose');
mongoose.connect(db.getConection(), { useNewUrlParser: true });
const Encuesta = mongoose.model("Encuestas");
const SplashPages = mongoose.model("SplashPages");
const Modems = mongoose.model("Modems");
const Impactos = mongoose.model("Impactos");
const Conexiones = mongoose.model("Conexiones");
const Leads = mongoose.model("Leads");
const Campain = mongoose.model("Campain");
const BlockedMacs = mongoose.model("BlockedMacs");
const Templates = mongoose.model("Templates");

const auth= require('../config/passport');
const mail = require('../config/mail');
const sms = require('../config/sms');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index');
});

router.post('/login',function(req,res,next){
  passport.authenticate('local',{session : false},function(err,user,info){
    if(err){
      res.json({success:false,error:err});
    }
    if(info.success == false) {
      res.json({success:false,message :info.message});
    }else{
      req.logIn(user,{session:false},(error)=>{
          if(error){
            res.json({success:false,error});
          }else if(!user){
            res.json({success:false,message :info.message});
          }else{
            const token = jwt.sign(user.toJSON(),'$hicomunity.2018');
            info.token = token;
            res.json(info);
          }
        });
    }
  })(req,res,next);
});

router.get('/encuestas/:id', auth.authenticationAPI ,function(req, res, next) {
    Encuesta.find({cliente:req.params.id}).populate('lead').exec(function(err,encuestas){
      if(err){
      res.json({succes : false,mensaje:'A ocurrido un error',error:err});
      }else{
          res.json({succes : true,encuestas});
      }
  });
});

router.get('/splashpages/:id', auth.authenticationAPI ,function(req, res, next) {
  SplashPages.find({usuario:req.params.id}).exec(function(err,splashs){
    if(err){
    res.json({succes : false,mensaje:'A ocurrido un error',error:err});
    }else{
        res.json({succes : true,splashs});
    }
});
});

router.get('/campains/:id', auth.authenticationAPI ,function(req, res, next) {
  Campain.find({cliente:req.params.id}).exec(function(err,campains){
    if(err){
    res.json({succes : false,mensaje:'A ocurrido un error',error:err});
    }else{
        res.json({succes : true,campains});
    }
  });
});
router.get('/campains/:id/:modem', auth.authenticationAPI ,function(req, res, next) {
  Campain.find({cliente:req.params.id,folio_modem:req.params.modem}).exec(function(err,campains){
    if(err){
    res.json({succes : false,mensaje:'A ocurrido un error',error:err});
    }else{
        res.json({succes : true,campains});
    }
  });
});

/*router.get('/leads/:id', auth.authenticationAPI ,function(req, res, next) {
  Leads.find({clientes : { $elemMatch : {cliente : req.params.id} } }).exec(function(err,leads){
    if(err){
      res.json({succes : false,mensaje:'A ocurrido un error',error:err});
    }else{
      res.json({succes : true,leads});
    }
  });
});*/

router.get('/leads/:modem', auth.authenticationAPI ,function(req, res, next) {
  Leads.find({modems : req.params.modem }).exec(function(err,leads){
    if(err){
      res.json({succes : false,mensaje:'A ocurrido un error',error:err});
    }else{
      res.json({succes : true,leads});
    }
  });
});

router.get('/conexiones/:modem', auth.authenticationAPI , function(req, res, next) {
  Conexiones.find({folio_modem : req.params.modem }).exec( async function(err,conexiones){
    if(err){
      res.json({succes : false,mensaje:'A ocurrido un error',error:err});
    }else{
      let result = [];
      let macs = conexiones.map((con)=>con.mac);
      let nmacs = [];
      let macsBloquedas = await BlockedMacs.find({});
      macsBloquedas = macsBloquedas.map((con)=>con.mac);
      //console.log(macsBloquedas);
      /*if(macs.length > 0){
        nmacs.push(macs[0]);
        result.push({mac : macs[0] , data : []});
      }*/
      macs.forEach(function(value,index){
        if(nmacs.indexOf(value) == -1){
          nmacs.push(value);
          if(macsBloquedas.indexOf(value) != -1){
            result.push( { mac : value , data : [] , block : true } );
          }else{
            result.push( { mac : value , data : [] , block : false } );
          }
        }
      });
      nmacs.forEach(function(value,index){
        conexiones.forEach(function(con,conIndex){
          if(value == con.mac){
            result[index].data.push(con);
          }
        });
      });

      res.json({succes : true,conexiones:result});
    }
  });
});

router.get('/modems/:id', auth.authenticationAPI ,function(req, res, next) {
  let query = {};
  if(req.user.tipo == 1 && req.user._id == req.params.id ){
    query = {};
  }else{
    query = {usuario:req.params.id};
  }
  Modems.find(query).populate('user').exec(function(err,modems){
    if(err){
      res.json({succes : false,mensaje:'A ocurrido un error',error:err});
    }else{
        res.json({succes : true,modems});
    }
  });
});

router.get('/modems/:tipo/:id', auth.authenticationAPI ,function(req, res, next) {
  let query = {};
  if(req.params.tipo == 'admin'){
    query = {};
  }else if(req.params.tipo == 'agencia'){
    query = {agencia:req.params.id}
  }else if(req.params.tipo == 'cliente'){
    query = {usuario:req.params.id}
  }
  Modems.find(query).populate('user').exec(function(err,modems){
    if(err){
      res.json({succes : false,mensaje:'A ocurrido un error',error:err});
    }else{
        res.json({succes : true,modems});
    }
  });
});
//dashboard 
router.get('/dashboard/agencia/:id/',auth.authenticationAPI,function(req,res,next){
  let query = {agencia : req.params.id};
  if(req.query.fechaInicio && req.query.fechaFin){
      query.createdAt = { 
          '$lte' : new Date(req.query.fechaFin),
          '$gte' : new Date(req.query.fechaInicio)
      }
  }
  let impactos = 0;
  let conexiones = 0;
  let leads = 0;
  let encuestas = 0;
  Impactos.countDocuments(query).exec(function(error,impResult){
    if(error){
      res.json({succes : false,mensaje:'A ocurrido un error',error});
    }else{
      impactos = impResult;
      Conexiones.countDocuments(query).exec(function(error,conResult){
        if(error){
          res.json({succes : false,mensaje:'A ocurrido un error',error});
        }else{
          conexiones = conResult;
          delete query.agencia;
          query.clientes = { $elemMatch: { agencia : req.params.id } };
          Leads.countDocuments(query).exec(function(error,leadsResult){
            if(error){
              res.json({succes : false,mensaje:'A ocurrido un error',error});
            }else{
              leads = leadsResult;

              delete  query.clientes;
              query.agencia = req.params.id;
              Encuesta.countDocuments(query).where('respuestas').ne([]).exec(function(error,encuestaResult){
                if(error){
                  res.json({succes : false,mensaje:'A ocurrido un error',error});
                }else{
                  encuestas = encuestaResult;
                  res.json({succes : true,impactos,conexiones,leads,encuestas});
                }
              });
            }
          });
        }
      });
    }
  });
});

router.get('/dashboard/cliente/:id',auth.authenticationAPI,function(req,res,next){
  let query = {cliente : req.params.id};
  if(req.query.fechaInicio && req.query.fechaFin){
      query.createdAt = { 
          '$lte' : new Date(req.query.fechaFin),
          '$gte' : new Date(req.query.fechaInicio)
      }
  }
  let impactos = 0;
  let conexiones = 0;
  let leads = 0;
  let encuestas = 0;
  Impactos.countDocuments(query).exec(function(error,impResult){
    if(error){
      res.json({succes : false,mensaje:'A ocurrido un error',error});
    }else{
      impactos = impResult;
      Conexiones.countDocuments(query).exec(function(error,conResult){
        if(error){
          res.json({succes : false,mensaje:'A ocurrido un error',error});
        }else{
          conexiones = conResult;
          delete query.cliente;
          query.clientes = { $elemMatch: { cliente : req.params.id } };
          Leads.countDocuments(query).exec(function(error,leadsResult){
            if(error){
              res.json({succes : false,mensaje:'A ocurrido un error',error});
            }else{
              leads = leadsResult;
              delete  query.clientes;
              query.cliente = req.params.id;
              Encuesta.countDocuments(query).where('respuestas').ne([]).exec(function(error,encuestaResult){
                if(error){
                  res.json({succes : false,mensaje:'A ocurrido un error',error});
                }else{
                  encuestas = encuestaResult;
                  res.json({succes : true,impactos,conexiones,leads,encuestas});
                }
              });
            }
          });
        }
      });
    }
  });
});

router.get('/dashboard/cliente/:id/:modem',auth.authenticationAPI,function(req,res,next){
  let query = {cliente : req.params.id,folio_modem : req.params.modem};
  if(req.query.fechaInicio && req.query.fechaFin){
      query.createdAt = { 
          '$lte' : new Date(req.query.fechaFin),
          '$gte' : new Date(req.query.fechaInicio)
      }
  }
  let impactos = 0;
  let conexiones = 0;
  let leads = 0;
  let encuestas = 0;
  Impactos.countDocuments(query).exec(function(error,impResult){
    if(error){
      res.json({succes : false,mensaje:'A ocurrido un error',error});
    }else{
      impactos = impResult;
      Conexiones.countDocuments(query).exec(function(error,conResult){
        if(error){
          res.json({succes : false,mensaje:'A ocurrido un error',error});
        }else{
          conexiones = conResult;
          delete query.cliente;
          delete query.folio_modem;
          query.modems = { $in : [req.params.modem]};
          query.clientes = { $elemMatch: { cliente : req.params.id } };
          Leads.countDocuments(query).exec(function(error,leadsResult){
            if(error){
              res.json({succes : false,mensaje:'A ocurrido un error',error});
            }else{
              leads = leadsResult;
              delete  query.clientes;
              query.cliente = req.params.id;
              Encuesta.countDocuments(query).where('respuestas').ne([]).exec(function(error,encuestaResult){
                if(error){
                  res.json({succes : false,mensaje:'A ocurrido un error',error});
                }else{
                  encuestas = encuestaResult;
                  res.json({succes : true,impactos,conexiones,leads,encuestas});
                }
              });
              res.json({succes : true,impactos,conexiones,leads});
            }
          });
        }
      });
    }
  });
});

//grafics conexiones

router.get('/graficas/cliente/:id',auth.authenticationAPI,function(req,res,next){
  let query = {cliente : req.params.id};
  if(req.query.fechaInicio && req.query.fechaFin){
      query.createdAt = { 
          '$lte' : new Date(req.query.fechaFin),
          '$gte' : new Date(req.query.fechaInicio)
      }
  }
  Conexiones.find(query).exec(function(error,conResult){
    if(error){
      res.json({succes : false,mensaje:'A ocurrido un error',error});
    }else{
      
      let macs = conResult.map((con)=>con.mac);
      //console.log(macs);
      query.mac = { '$in' : macs};
      delete query.cliente;
      Leads.find(query).exec(function(error,leadsResult){
        if(error){
          res.json({succes : false,mensaje:'A ocurrido un error',error});
        }else{
          let genero = {
            masculino : leadsResult.filter(l=> (l.sexo.toLowerCase() == 'hombre') || (l.sexo.toLowerCase() == 'male') || (l.sexo.toLowerCase() == 'masculino') ).length ,
            femenino : leadsResult.filter(l=> (l.sexo.toLowerCase() == 'mujer') || (l.sexo.toLowerCase() == 'female') || (l.sexo.toLowerCase() == 'femenino') ).length
          };
          let edad = {
             '0-18' : leadsResult.filter(l=> (l.edad <= 18)).length ,
             '19-25' : leadsResult.filter(l=> (l.edad >= 19) && (l.edad <= 25)).length ,
             '26-45' : leadsResult.filter(l=> (l.edad >= 26) && (l.edad <= 45)).length ,
             '46+' : leadsResult.filter(l=> (l.edad >= 46)).length 
          };
          //let data = macs.reduce((b,c)=>((b[b.findIndex(d=>d.mac===c)]||b[b.push({mac:c,count:0})-1]).count++,b),[]);
          let data = macs.reduce((accumulator, currentValue) => {
            (
                accumulator[accumulator.findIndex(item => item.mac === currentValue)]
                ||
                accumulator[accumulator.push({mac: currentValue, count: 0}) - 1] 
            ).count++;
            return accumulator;
        }, []);
          res.json({succes : true,genero,edad,conexiones_repeat:data});
        }
      });

    }
  });
  
});

router.get('/graficas/agencia/:id',auth.authenticationAPI,function(req,res,next){
  let query = {agencia : req.params.id};
  if(req.query.fechaInicio && req.query.fechaFin){
      query.createdAt = { 
          '$lte' : new Date(req.query.fechaFin),
          '$gte' : new Date(req.query.fechaInicio)
      }
  }
  Conexiones.find(query).exec(function(error,conResult){
    if(error){
      res.json({succes : false,mensaje:'A ocurrido un error',error});
    }else{
      
      let macs = conResult.map((con)=>con.mac);
      //console.log(macs);
      query.mac = { '$in' : macs};
      delete query.agencia;
      Leads.find(query).exec(function(error,leadsResult){
        if(error){
          res.json({succes : false,mensaje:'A ocurrido un error',error});
        }else{
          let genero = {
            masculino : leadsResult.filter(l=> (l.sexo.toLowerCase() == 'hombre') || (l.sexo.toLowerCase() == 'male') || (l.sexo.toLowerCase() == 'masculino') ).length ,
            femenino : leadsResult.filter(l=> (l.sexo.toLowerCase() == 'mujer') || (l.sexo.toLowerCase() == 'female') || (l.sexo.toLowerCase() == 'femenino') ).length
          };
          let edad = {
             '0-18' : leadsResult.filter(l=> (l.edad <= 18)).length ,
             '19-25' : leadsResult.filter(l=> (l.edad >= 19) && (l.edad <= 25)).length ,
             '26-45' : leadsResult.filter(l=> (l.edad >= 26) && (l.edad <= 45)).length ,
             '46+' : leadsResult.filter(l=> (l.edad >= 46)).length 
          };
          //let data = macs.reduce((b,c)=>((b[b.findIndex(d=>d.mac===c)]||b[b.push({mac:c,count:0})-1]).count++,b),[]);
          let data = macs.reduce((accumulator, currentValue) => {
            (
                accumulator[accumulator.findIndex(item => item.mac === currentValue)]
                ||
                accumulator[accumulator.push({mac: currentValue, count: 0}) - 1] 
            ).count++;
            return accumulator;
        }, []);
          res.json({succes : true,genero,edad,conexiones_repeat:data});
        }
      });

    }
  });
  
});

router.get('/open',function(req,res,next){
  //abrir acceso a internt
  res.json({});
});

router.post('/sendMail',function(req,res,next){
  mail.sendMail(req.body.to,req.body.asunto,req.body.html, function(error, info){
    if (error) {
      res.json({success : false, message : 'A ocurrido un error al enviar los mail', error });
    } else {
      console.log('Email sent: ' + info.response);
      res.json({succes : true, message : 'Email\'s enviados con exito', result : info.response});
    }
  });  
});


router.get('/templates/:idCliente', auth.authenticationAPI ,function(req, res, next) {
  Templates.find({cliente:req.params.idCliente}).exec(function(err,templates){
    if(err){
      res.json({succes : false,mensaje:'A ocurrido un error',error:err});
    }else{
        res.json({succes : true,templates});
    }
  });
});


router.get('/leads/marketing/:idCliente', auth.authenticationAPI ,function(req, res, next) {
  let query = {};
  query.clientes = { $elemMatch: { cliente : req.params.idCliente } } ;
  if(req.query.sexo != 'todos'){
    if(req.query.sexo == 'masculino'){
      query.$or = [ { sexo : 'male' },{ sexo : 'Masculino' },{ sexo : 'masculino' },{ sexo : 'hombre' },{ sexo : 'Hombre' }];
    }else if(req.query.sexo == 'femenino'){
      query.$or = [ { sexo : 'female' },{ sexo : 'Femenino' },{ sexo : 'femenino' },{ sexo : 'mujer' },{ sexo : 'Mujer' }];
    }
  }
  if(req.query.edad1 && req.query.edad2){
    query.edad = {
      '$lte' : (req.query.edad2),
      '$gte' : (req.query.edad1),
    }
  }
  console.log(query);
  Leads.find(query).exec(function(err,leads){
    console.log(err);
    if(err){
      res.json({succes : false,mensaje:'A ocurrido un error',error:err});
    }else{
      res.json({succes : true,leads});
    }
  });
});

router.post('/sendSMS',function(req,res,next){

  sms.sendSMS(req.body).then(function(message){
    res.json({succes : true,mensaje:'SMS enviados con éxito',message});
  }).catch(function(error){
    res.json({succes : false,mensaje:'A ocurrido un error',error});
  });

});

router.post('/sendCode',function(req,res,next){

  Leads.findOne({mac: req.body.mac}).exec().then(function(result){
    console.log(result);
    let data = {
      body : "Tu codigo de verificación es: " + result.codigo_usuario,
      from : '+13172862788',
      to : '+52'+req.body.telefono
    };
    return sms.sendSMS(data);
  }).then(function(message){
    res.json({succes : true,mensaje:'Code enviado con éxito',message});
  }).catch(function(error){
    res.json({succes : false,mensaje:'A ocurrido un error',error});
  });
  
});

module.exports = router;
