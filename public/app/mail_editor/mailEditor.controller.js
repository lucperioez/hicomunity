(function(){

     'use strict'
     
  function mailEditorCtrl($cookies, mailEditorService,$state,$sce,$scope,$stateParams){
         
        var ctrl = this;

        //ctrl.sessionData = $cookies.getObject('userData');
        //ctrl.admin = (ctrl.sessionData.userId == 1);
        //ctrl.plantillaId = $stateParams.plantillaId;
        toastr.options = {
          "positionClass": "toast-top-right",
          "showDuration": "200",
          };
      //Function to
        ctrl.startEditor = function(idUser){
 
            var request = function(method, url, data, type, callback) {
                var req = new XMLHttpRequest();
                console.log(type);
                req.onreadystatechange = function() {
                  if (req.readyState === 4 && req.status === 200) {
                    var response = JSON.parse(req.responseText);
                    callback(response);
                  }
                };
          
                req.open(method, url, true);
                if (data && type) {
                  if(type === 'multipart/form-data') {
                    var formData = new FormData();
                    for (var key in data) {
                      formData.append(key, data[key]);
                    }
                    data = formData;          
                  }
                  else {
                    req.setRequestHeader('Content-type', type);
                  }
                }
          
                req.send(data);
              };
          
              var save = function(filename, content) {
                saveAs(
                  new Blob([content], {type: 'text/plain;charset=utf-8'}), 
                  filename
                ); 
              };
          
              var specialLinks = [{
                  type: 'unsubscribe',
                  label: 'SpecialLink.Unsubscribe',
                  link: 'http://[unsubscribe]/'
              }, {
                  type: 'subscribe',
                  label: 'SpecialLink.Subscribe',
                  link: 'http://[subscribe]/'
              }];
          
              var mergeTags = [{
                name: 'tag 1',
                value: '[tag1]'
              }, {
                name: 'tag 2',
                value: '[tag2]'
              }];
          
              var mergeContents = [{
                name: 'content 1',
                value: '[content1]'
              }, {
                name: 'content 2',
                value: '[content1]'
              }];
          
              var beeConfig = {  
                uid: 'test1-clientside',
                container: 'bee-plugin-container',
                autosave: 15, 
                language: 'es-ES',
                specialLinks: specialLinks,
                mergeTags: mergeTags,
                mergeContents: mergeContents,
                onSave: function(jsonFile, htmlFile) { 
                  //save('newsletter.html', htmlFile);

                  if($stateParams.plantillaId == 0){
                    var html = htmlFile.replace(new RegExp('"', 'g'),"'");
                        html = html.replace(new RegExp('/\n', 'g'),"");
                     //console.log(html);
                      ctrl.obj = {
                          "cliente":$stateParams.clienteId,
                          "json":JSON.parse(jsonFile),
                          "html":html
                      }
                    mailEditorService.saveData(ctrl.obj).then(function successCallback(response){      
                      //$state.go('mail_list', {}, { reload: true });   
                      if(response.data.succes){
                        toastr.success(response.data.mensaje, "");
                        $state.go('my-templates');
                      }           
                    });
                  }else{
                    var html = htmlFile.replace(new RegExp('"', 'g'),"'");
                        html = html.replace(new RegExp('/\n', 'g'),"");
                    ctrl.obj = {
                          "json":JSON.parse(jsonFile),
                          "html":html,
                          "id":$stateParams.plantillaId
                      }
                    
                    mailEditorService.updateData(ctrl.obj).then(function successCallback(response){ 
                      if(response.data.succes){
                        toastr.success(response.data.mensaje, "");
                        

                      }                
                    });
                  }

                },
                onSaveAsTemplate: function(jsonFile,thumbnail) { // + thumbnail? 
                  //save('lucio.json', jsonFile);
                  console.log(jsonFile);
                  console.log(thumbnail);
      
                },
                onAutoSave: function(jsonFile) { // + thumbnail? 
                  /*console.log(new Date().toISOString() + ' autosaving...');
                  window.localStorage.setItem('newsletter.autosave', jsonFile);*/
                },
                onSend: function(htmlFile) {
                  //write your send test function here
                },
                onError: function(errorMessage) { 
                  console.log('onError ', errorMessage);
                }
              };
          
              var bee = null;
          
              var loadTemplate = function(e) {
                var templateFile = e.target.files[0];
                var reader = new FileReader();
          
                reader.onload = function() {
                  var templateString = reader.result;
                  var template = JSON.parse(templateString);
                  bee.load(template);
                };
          
                reader.readAsText(templateFile);
              };
          
              
          
              request(
                'POST', 
                'https://auth.getbee.io/apiauth',
                'grant_type=password&client_id=f06d390d-333e-40d7-a077-b742652b8261&client_secret=i7Bxw83wHIHNvIF59YwNkkzz2a2c14SrRoUXHzS04IUjH6LKTFB',
                'application/x-www-form-urlencoded',
                function(token) {
                  BeePlugin.create(token, beeConfig, function(beePluginInstance) {
                    bee = beePluginInstance;
                        if($stateParams.plantillaId == 0){
                         
                          request(
                            'GET', 
                            'app/mail_editor/templates/conectaliaDefaultTemplate.json',
                            null,
                            null,
                            function(template) {
                              bee.start(template);
                              setTimeout(function(){ 
   
                               }, 5000);
                            })

                        }else{
                          /*ctrl.obj = {
                            "model":"mail",
                            "submodel": "mail_editor",
                            "action":"obtenerPlantilla",
                            "data": {
                                //"empresa_id":ctrl.sessionData.userId,
                                "plantilla_id":$stateParams.plantillaId
                            }
                  
                          };*/
                          
                          mailEditorService.getData($stateParams.plantillaId).then(function successCallback(response){
        
                            ctrl.plantilla = response.data.template;
                
                            //var templateString = ctrl.plantilla.json;
                            //var template = JSON.parse(templateString);
                            var template = ctrl.plantilla.json;
                            bee.start(template);
                            setTimeout(function(){ 

                             }, 3000);
                            
                          });
                        }
                        
                      });
          
                });
            
        }

        ctrl.llenaCombo = function(id){
          ctrl.llenarComboObj = {
              "model":"panel",
              "submodel": "splashpages",
              "action":"obtenerMisClientes",
              "data": {
                usuario : id
              }
          };
          mailEditorService.get(ctrl.llenarComboObj).then(function successCallback(response){
              ctrl.paquetesSelect = response.data;

          });
      }
      ctrl.llenaAgencia = function(){
          ctrl.agenciaObj = {
              "model":"panel",
              "submodel": "splashpages",
              "action":"obtenerMisAgencias",
          };
          mailEditorService.get(ctrl.agenciaObj).then(function successCallback(response){
              ctrl.paqueteAgencia = response.data;

          });
      }

      ctrl.cambiaBandera = function(id){
        ctrl.flagAgency = 1;
        ctrl.llenaCombo(id)
      }

      ctrl.Inicial = function(){
        if($stateParams.plantillaId != 0){
          ctrl.startEditor(ctrl.sessionData.userId)
        }else{
          if(ctrl.sessionData.userType == 1){
            ctrl.llenaAgencia()
          }else if(ctrl.sessionData.userType == 2){
            ctrl.comboClientes = ctrl.sessionData.userId
            ctrl.llenaCombo(ctrl.comboClientes)
          }else {
            ctrl.startEditor(ctrl.sessionData.userId);
          }
        }
         
      }

     
      ctrl.showEditor = function(idUser){
        if(idUser){
          ctrl.startEditor(idUser);
        }
      }

    //Document ready calls  
    $scope.$on('$routeChangeUpdate', 
   // ctrl.Inicial() ,
      ctrl.startEditor()  
    );
  }
        
    angular
        .module('hicomunity.panel')
        .controller('mailEditorCtrl',mailEditorCtrl)

 }());