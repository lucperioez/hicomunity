(function(){

     'use strict'
     
  function conexionesCtrl(conexionesService,NgTableParams,$scope,modemsService,$cookies){
         
      
      var ctrl = this;
      ctrl.user_data = $cookies.getObject('usr_dta');
      ctrl.plazas = {};
      ctrl.plaza = '';

      ctrl.admin = ctrl.user_data.tipo == 1;
    
      toastr.options = {
        "positionClass": "toast-top-right",
        "showDuration": "200",
      };

      if(ctrl.admin){
        //atraer todos los modems asignados
        modemsService.obtenerModemsTipo('admin').then(function(response){
            console.log(response);
            ctrl.plazas = response.data.modems.filter((m)=>m.usuario!=undefined);
        }).catch(function(response){
                toastr.error(response.data.message, "");
        });
      }else if(ctrl.user_data.tipo == 2){
        //todos los modems con agencia
        modemsService.obtenerModemsTipo('agencia',ctrl.user_data._id).then(function(response){
            console.log(response);
            ctrl.plazas = response.data.modems;
        }).catch(function(response){
                toastr.error(response.data.message, "");
        });
      }else if(ctrl.user_data.tipo == 3){
        //todos los modems con agencia
        modemsService.obtenerModemsTipo('cliente',ctrl.user_data._id).then(function(response){
            console.log(response);
            ctrl.plazas = response.data.modems;
        }).catch(function(response){
                toastr.error(response.data.message, "");
        });
      }
      ctrl.getConexiones = function(){
        conexionesService.getConexionesByModem(ctrl.plaza).then(function(response){
            console.log(response);
            if(response.data.succes){
                
                ctrl.tableParams = new NgTableParams({}, { dataset:  response.data.conexiones});
            }
        }).catch(function(response){
            toastr.error(response.data.message, "");
        });
      }
      ctrl.checkTipoConexion = function(val){
        if(val == 1 ){
            return 'Facebook';
        }else if(val == 2){
            return 'Formulario';
        }
      }
      ctrl.bloquedMac = function(status,mac){
          //console.log();
        if(status){
            conexionesService.bloquearMac(mac).then(function(response){
                console.log(response);
                if(response.data.success || response.data.succes){
                    toastr.success(response.data.message, "");
                }
            }).catch(function(response){
                toastr.error(response.data.message, "");
            });
        }else{
            conexionesService.desbloquearMac(mac).then(function(response){
                console.log(response);
                if(response.data.success || response.data.succes){
                    toastr.success(response.data.message, "");
                }
            }).catch(function(response){   
                toastr.error(response.data.message, "");
            });
        }
      }
      
  }
        
    angular
        .module('hicomunity.panel')
        .controller('conexionesCtrl',conexionesCtrl)
 }());
 