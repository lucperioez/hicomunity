var express = require('express');
var router = express.Router();
const db = require('../config/db');
require("../models/M_Leads");
require("../models/M_Users");
const mongoose = require('mongoose');
mongoose.connect(db.getConection(), { useNewUrlParser: true });
const Leads = mongoose.model("Leads");
const Usuarios = mongoose.model("Usuarios");

var passport = require('passport');
var auth= require('../config/passport');

router.get('/:id',auth.authenticationAPI,function(req, res, next) {
    Leads.findById(req.params.id).exec(function(err,lead){
        if(err){
            res.json({succes : false,mensaje:'A ocurrido un error',error:err});
        }else{
            if(lead){
                res.json({succes : true,mensaje:'Lead encontrado',lead});
            }else{
                res.json({succes : true,mensaje:'No se ha encontrado el Lead'});
            }
        }
    });
    
});
/*router.post('/fb/',function(req,res,next){
    Leads.findOne({identificador:req.body.identificador}).exec(function(err,lead){
        if(err){
            res.json({succes : false,mensaje:'A ocurrido un error',error:err});
        }else{
            if(lead){
                //si existe agregar el modem
                if(!lead.modems.includes(req.body.modem)){
                    lead.modems.push(req.body.modem);
                }

                if(lead.clientes.filter((element)=>element.cliente==req.body.cliente) == 0){
                    lead.clientes.push({cliente:req.body.cliente,agencia:req.body.agencia})
                }

                lead.nombre = req.body.nombre || lead.nombre;
                lead.sexo = req.body.sexo || lead.sexo;
                lead.edad = req.body.edad || lead.edad;
                lead.correo = req.body.correo || lead.correo;
                lead.telefono = req.body.telefono || lead.telefono;
                lead.mac = req.body.mac || lead.mac;
                lead.url_imagen = req.body.url_imagen || lead.url_imagen;

                lead.save(function(error,lead){
                    if(error){
                        res.json({succes : false,mensaje:'A ocurrido un error',error});
                    }else{
                        res.json({succes : true,mensaje:'Lead actualizado con exito',lead});
                    }
                });
                
            }else{
                //si no existe agregarlo
                req.body.modems = [req.body.modem];
                req.body.clientes = [{cliente:req.body.cliente,agencia:req.body.agencia}];
                let nLead = new Leads(req.body);
                nLead.save(function(error,lead){
                    if(error){
                        res.json({succes : false,mensaje:'A ocurrido un error',error});
                    }else{
                        res.json({succes : true,mensaje:'Lead creado con exito',lead});
                    }
                });
            }
        }
    });
});*/

router.post('/',function(req,res,next){

    const user = Usuarios.findById(req.body.cliente).exec();
    req.body.agencia = user.agencia;
  
    Leads.findOne({mac:req.body.mac}).exec(function(err,lead){
        if(err){
            res.json({succes : false,mensaje:'A ocurrido un error',error:err});
        }else{
            if(lead){
                //si existe agregar el modem
                if(!lead.modems.includes(req.body.modem)){
                    lead.modems.push(req.body.modem);
                }
                
                if(lead.clientes.filter((element)=>element.cliente==req.body.cliente) == 0){
                    lead.clientes.push({cliente:req.body.cliente,agencia:req.body.agencia})
                }

                lead.nombre = req.body.nombre || lead.nombre;
                lead.sexo = req.body.sexo || lead.sexo;
                lead.edad = req.body.edad || lead.edad;
                lead.correo = req.body.correo || lead.correo;
                lead.telefono = req.body.telefono || lead.telefono;
                lead.mac = req.body.mac || lead.mac;
                lead.url_imagen = req.body.url_imagen || lead.url_imagen;

                lead.save(function(error,lead){
                    if(error){
                        res.json({succes : false,mensaje:'A ocurrido un error',error});
                    }else{
                        res.json({succes : true,mensaje:'Lead actualizado con exito',lead});
                    }
                });
                
            }else{
                //si no existe agregarlo
                req.body.modems = [req.body.modem];
                req.body.clientes = [{cliente:req.body.cliente,agencia:req.body.agencia}];

                let nLead = new Leads(req.body);
                nLead.save(function(error,lead){
                    if(error){
                        res.json({succes : false,mensaje:'A ocurrido un error',error});
                    }else{
                        res.json({succes : true,mensaje:'Lead creado con exito',lead});
                    }
                });
            }
        }
    });
});

router.get('/:mac/:codigo/:telefono',function(req,res,next){
    Leads.findOne({mac:req.params.mac,codigo_usuario:req.params.codigo}).exec().then(function(lead){
        if(lead){
            lead.telefono = req.params.telefono;
            lead.telefono_validado = true;
            lead.save();
            res.json({succes : true,mensaje:'El código es válido'});
        }else{
            res.json({succes : false,mensaje:'El código es inválido'});
        }
    }).catch(function(error){
        res.json({succes : false,mensaje:'A ocurrido un error',error});
    });
});

module.exports = router;
