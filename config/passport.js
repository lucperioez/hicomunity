const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const passportJWT = require('passport-jwt');
const ExtractJWT = passportJWT.ExtractJwt;
const JWTStrategy = passportJWT.Strategy;

const db = require('../config/db');

require("../models/M_Users.js");
const mongoose = require('mongoose');
mongoose.connect(db.getConection(), { useNewUrlParser: true });
const User = mongoose.model("Usuarios");

passport.serializeUser((user,done)=>{
    done(null,user._id);
});

passport.deserializeUser((id,done)=>{
    User.findById(id,(err,result)=>{
        done(err,result);
    });
});

passport.use(new LocalStrategy(
    {usernameField : 'usuario', passwordField : 'password'},
    (usuario,password,done) => {
        if(usuario == '' || usuario == undefined ){
            return done(null,false,{success:false, message : 'Credenciales vacias'});
        }
        if(password == '' || password == undefined ){
            return done(null,false,{success:false, message : 'Credenciales vacias'});
        }
        User.findOne({usuario},(err,user)=>{
            if (!user){ 
                return done(null,false,{success:false, message : 'El usuario no existe.'});
            }
            user.comparePassword(password,(err,sonIguales)=>{
                if(sonIguales) {
                    user.password = undefined;
                    return done(null,user,{
                        success:true,
                        message:'Inicio de sesión exitoso',
                        data : user
                    });
                }
                return done(null,false,{success:false ,message:'El usuario y/o la contraseña no corresponden'});
            });
        });
    }
));
passport.use(new JWTStrategy({
    jwtFromRequest : ExtractJWT.fromHeader('token'),
    secretOrKey : '$hicomunity.2018'
    },
    function(jwtPayload,done){
      User.findById(jwtPayload._id).exec(function(err,user){
        if(err)
            done(err, false);
        if(user) {
            /*var expirationDate = new Date(jwtPayload.exp * 1000)
            if(expirationDate < new Date()) {
                return done(null, false,{success : false , message: 'El token a expirado'});
            }else{*/
                return done(null, user);
            //}
        }else{
            done(null, false, {success : false, message: "El token es invalido" });
        }

      });
    }
));
exports.authenticationAPI = (req,res,next) => {
    passport.authenticate('jwt', {session : false},(error, user, info) => {
        if (error){
            next(error); // It is null
        }else if(!Object.is(info,Object) && info == 'undefined'){
                res.status(403).json({success : false , message: 'No auth token'});
        }else if (!user && Object.is(info,Object)){
            res.status(403).json(info);
        }else if(!user){
            res.status(403).json({success: false, message : "Unable to access the resource"});
        }else{
            req.user=user;
            next();
        }
    })(req, res, next);
};