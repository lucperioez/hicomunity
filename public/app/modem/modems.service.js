(function(){

    'use strict'

    function modemsService(config,$http,$cookies,$q,$stateParams){
        

       this.obtenerModems = function(_id){
           var token = $cookies.get('tkn') 
           var user_data = $cookies.getObject('usr_dta');  
           var url = '/modems/'+_id;

           return $http({
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'token' : token
            }    
            });
       }

       this.obtenerModemsTipo = function(tipo,_id){
            var token = $cookies.get('tkn') 
            var user_data = $cookies.getObject('usr_dta');  
            var url = '/modems/'+tipo+'/'+_id;

            return $http({
            method: 'GET',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'token' : token
            }    
            });
        }
       
        this.nuevoModem = function(modem){
            var token = $cookies.get('tkn') 
            var url = '/modem';

            return $http({
                method: 'POST',
                url: url,
                data : modem,
                headers: {
                    'Content-Type': 'application/json',
                    'token' : token
                }    
            });
        }
        this.asignarModem = function(modem){
            var token = $cookies.get('tkn') 
            var url = '/modem/'+modem._id;

            return $http({
                method: 'PUT',
                url: url,
                data : modem,
                headers: {
                    'Content-Type': 'application/json',
                    'token' : token
                }    
            });
        }

    }

    angular
       .module('hicomunity.panel')
       .service('modemsService', modemsService);

}());