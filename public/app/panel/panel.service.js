(function(){

    'use strict'

    function panelService($http,$cookies){
        

        this.getDataAgencia = function(id){

            var url = '/dashboard/agencia/'+id;
            var token = $cookies.get('tkn');

            return $http({
                method: 'GET',
                url: url,
                headers: {
                        'Content-Type': 'application/json',
                        'token' : token
                    }
            });
        }
        this.getDataCliente = function(id){

            var url = '/dashboard/cliente/'+id;
            var token = $cookies.get('tkn');

            return $http({
                method: 'GET',
                url: url,
                headers: {
                        'Content-Type': 'application/json',
                        'token' : token
                    }
            });
        }
        this.getDataClienteModem = function(id,modem){

            var url = '/dashboard/cliente/'+id+'/'+modem;
            var token = $cookies.get('tkn');

            return $http({
                method: 'GET',
                url: url,
                headers: {
                        'Content-Type': 'application/json',
                        'token' : token
                    }
            });
        }
        this.getGraficasAgencia = function(id){

            var url = '/graficas/agencia/'+id;
            var token = $cookies.get('tkn');

            return $http({
                method: 'GET',
                url: url,
                headers: {
                        'Content-Type': 'application/json',
                        'token' : token
                    }
            });
        }
        this.getGraficasCliente = function(id){

            var url = '/graficas/cliente/'+id;
            var token = $cookies.get('tkn');

            return $http({
                method: 'GET',
                url: url,
                headers: {
                        'Content-Type': 'application/json',
                        'token' : token
                    }
            });
        }
        
        this.getPlazas = function(id){

            var url = '/modems/'+id;
            var token = $cookies.get('tkn');

            return $http({
                method: 'GET',
                url: url,
                headers: {
                        'Content-Type': 'application/json',
                        'token' : token
                    }
            });
        }
    }

    angular
       .module('hicomunity.panel')
       .service('panelService', panelService);

}());