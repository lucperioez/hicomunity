var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');

var UsuarioSchema = new Schema({
    nombre : String,
    contacto : String,
    rfc : String,
    direccion : String,
    telefono : String,
    email : String,
    pagos : {
        contacto : String,
        telefono : String,
        email : String
    },
    usuario : { type : String , index: true, unique: true}, 
    password : { type : String },
    tipo : Number,
    estatus : Number,
    agencia : {type: Schema.ObjectId, ref: "Usuarios" },
    logo : String
    
}, {timestamps: true});

UsuarioSchema.pre('save', function(next) {
    console.log(this.isModified('password'));
	if(!this.isModified('password')) return next();
	bcrypt.genSalt(10,(err,salt)=>{
		if(err) return next(err);
		bcrypt.hash(this.password,salt,null,(err,hash)=>{
			if(err) return next(err);
			this.password = hash;
			next();
		});
	});
});


UsuarioSchema.methods.comparePassword = function(password, fn){
	bcrypt.compare(password, this.password, (err,result)=>{
		if(err) return fn(err);
		return fn(null,result);
	});
}
/*UsuarioSchema.post('find', function(users) {
	users.forEach(user => {
		user.password = undefined;
	});
});*/
mongoose.model("Usuarios", UsuarioSchema,"Usuarios");
