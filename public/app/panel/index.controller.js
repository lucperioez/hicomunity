(function(){

    'use strict'
    
 function indexCtrl($cookies, indexService,$state,$sce,$scope){
        
       var ctrl = this;

       ctrl.sessionData = $cookies.getObject('usr_dta');
    
       ctrl.img = ctrl.sessionData.logo || 'assets/app/media/img/users/default-user.png';
       
       /*$scope.realImage = ctrl.sessionData.logo;
       $scope.myDefaultImage = '../../backend/users_assets/tinytrans.jpg';
       $scope.configImage = ctrl.sessionData.logo;
       $scope.mensaje = '../../backend/users_assets/sinImagen.png';*/
       
       indexService.getMenu(ctrl.sessionData.tipo).then(function successCallback(response){
           console.log(response);

           ctrl.menu = response.data.items;
       });//End service
      
       ctrl.logOut = function(){
           $cookies.remove('usr_dta', {path: "/"});
           $cookies.remove('tkn', {path: "/"});
           location.href = 'login.html';
       }

       ctrl.abrirUrl = function(url){
           if(url != ''){
               location.href = '#!'+url;
           }
           
       }



 }
       
   angular
       .module('hicomunity.panel')
       .controller('indexCtrl',indexCtrl)
       .directive('errSrc', function () {

           return {

               link: function (scope, element, attrs) {

                   element.bind('error', function () {

                       if (attrs.src != attrs.errSrc) {

                           attrs.$set('src', attrs.errSrc);

                       }

                   });



                   attrs.$observe('ngSrc', function (value) {

                       if (!value && attrs.errSrc) {

                           attrs.$set('src', attrs.errSrc);

                       }

                   });

               }

           }

       });
}());
