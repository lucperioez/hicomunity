var mongoose = require("mongoose");
var Schema = mongoose.Schema;

/*
    Tipo Conexion:
        1 -> Facebook
        2 -> Formulario 
*/

var ConexionesSchema = new Schema({
    tipo_conexion : Number,
    splash : {type: Schema.ObjectId, ref: "SplashPages" },
    cliente : {type: Schema.ObjectId, ref: "Usuarios" },
    agencia : {type: Schema.ObjectId, ref: "Usuarios" },
    navegador : String,
    dispositivo : String,
    mac : String,
    wifi_name : String,
    folio_modem : String
}, {timestamps : true});

mongoose.model("Conexiones", ConexionesSchema,"Conexiones");
