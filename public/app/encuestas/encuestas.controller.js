(function(){

     'use strict'
     
  function encuestasCtrl(encuestasService,NgTableParams,$scope,usuariosService,$cookies){
         
    var ctrl = this;
    ctrl.user_data = $cookies.getObject('usr_dta');
    ctrl.agencias = [];
    ctrl.clientes = [];
    ctrl.usuarios = [];
    ctrl.agencia = '';
    ctrl.cliente = '';
    ctrl.cliente_nombre = '';
    ctrl.encuesta = {};
    ctrl.admin = ctrl.user_data.tipo == 1;
    ctrl.encuesta_d = {};
    ctrl.encuesta_copy = {};

    if(ctrl.user_data.tipo == 3){
        encuestasService.obtenerEncuestas(ctrl.user_data._id).then(function(response){
            //console.log(response);
            ctrl.encuestas = response.data.encuestas;
            ctrl.tableParams = new NgTableParams({}, { dataset:  ctrl.encuestas});
        }).catch(function(response){   
            toastr.options = {
                "positionClass": "toast-top-right",
                "showDuration": "200",
            };
            toastr.error(response.data.message, "");
        });
    }else{
        usuariosService.get().then(function(response){
            console.log(response);
            let data = response.data.users;
            ctrl.usuarios = data;
          if(ctrl.user_data.tipo == 2){
                ctrl.clientes = data;
          }else{  
                ctrl.agencias = data.filter((user)=>user.tipo==2);
          }
        }).catch(function(response){   
            toastr.options = {
            "positionClass": "toast-top-right",
            "showDuration": "200",
            };
            toastr.error(response.data.message, "");
        });
    }

    ctrl.getCliente = function(){
        if(ctrl.agencia != ''){
            ctrl.clientes = ctrl.usuarios.filter((user)=> (user.tipo==3) && (user.agencia._id==ctrl.agencia) );
        }else{
            ctrl.cliente = '';
            ctrl.cliente_nombre = '';
            ctrl.clientes = [];
            ctrl.encuestas = [];
            ctrl.tableParams = new NgTableParams({}, { dataset:  ctrl.encuestas});
            ctrl.tableParams.reload();
        }
        
    }
    
    ctrl.getEncuestas = function(){
        if(ctrl.cliente != ''){
            
            ctrl.cliente_nombre = ctrl.clientes.filter((user)=> (user._id==ctrl.cliente))[0].nombre;
            encuestasService.obtenerEncuestas(ctrl.cliente).then(function(response){
                console.log(response);
                ctrl.encuestas = response.data.encuestas;
                ctrl.tableParams = new NgTableParams({}, { dataset:  ctrl.encuestas});
                ctrl.tableParams.reload();
            }).catch(function(response){   
                toastr.options = {
                "positionClass": "toast-top-right",
                "showDuration": "200",
                };
                toastr.error(response.data.message, "");
            });
        }else{
            ctrl.encuestas = [];
            ctrl.tableParams = new NgTableParams({}, { dataset:  ctrl.encuestas});
            ctrl.tableParams.reload();
        }
    }
    ctrl.agregarEncuesta = function(){
        //console.log(ctrl.encuesta);
        if(ctrl.admin){
            ctrl.encuesta.cliente = ctrl.cliente;
            ctrl.encuesta.agencia = ctrl.agencia;
            encuestasService.nuevaEncuesta(ctrl.encuesta).then(function(response) {
                //console.log(response);
                if(response.data.succes){
                    ctrl.encuestas.push(response.data.encuesta);
                    ctrl.tableParams = new NgTableParams({}, { dataset: ctrl.encuestas });
                    ctrl.tableParams.reload();
                    $('#m_modal_4').modal('hide');              
                    toastr.options = {
                        "positionClass": "toast-top-right",
                        "showDuration": "200",
                    };
                    toastr.success(response.data.mensaje || response.data.message, "");
                }
            }).catch(function(response){   
                toastr.options = {
                "positionClass": "toast-top-right",
                "showDuration": "200",
                };
                toastr.error(response.data.message, "");
            });
        }else if(ctrl.user_data.tipo == 2){
            ctrl.encuesta.cliente = ctrl.cliente;
            ctrl.encuesta.agencia = ctrl.user_data._id;
            encuestasService.nuevaEncuesta(ctrl.encuesta).then(function(response) {
                //console.log(response);
                if(response.data.succes){
                    ctrl.encuestas.push(response.data.encuesta);
                    ctrl.tableParams = new NgTableParams({}, { dataset: ctrl.encuestas });
                    ctrl.tableParams.reload();
                    $('#m_modal_4').modal('hide');              
                    toastr.options = {
                        "positionClass": "toast-top-right",
                        "showDuration": "200",
                    };
                    toastr.success(response.data.mensaje || response.data.message, "");
                }
            }).catch(function(response){   
                toastr.options = {
                "positionClass": "toast-top-right",
                "showDuration": "200",
                };
                toastr.error(response.data.message, "");
            });
        }else if(ctrl.user_data.tipo == 3){
            ctrl.encuesta.cliente = ctrl.user_data._id;
            ctrl.encuesta.agencia = ctrl.user_data.agencia;
            encuestasService.nuevaEncuesta(ctrl.encuesta).then(function(response) {
                //console.log(response);
                if(response.data.succes){
                    ctrl.encuestas.push(response.data.encuesta);
                    ctrl.tableParams = new NgTableParams({}, { dataset: ctrl.encuestas });
                    ctrl.tableParams.reload();
                    $('#m_modal_4').modal('hide');              
                    toastr.options = {
                        "positionClass": "toast-top-right",
                        "showDuration": "200",
                    };
                    toastr.success(response.data.mensaje || response.data.message, "");
                }
            }).catch(function(response){   
                toastr.options = {
                "positionClass": "toast-top-right",
                "showDuration": "200",
                };
                toastr.error(response.data.message, "");
            });
        }
        
    }
    ctrl.eliminar = function(encuesta){
        ctrl.encuesta_d = encuesta;
        $('#modal-delete').modal('show');
    }
    ctrl.delete = function(){
        encuestasService.eliminarEncuesta(ctrl.encuesta_d).then(function(response) {
            //console.log(response);
            if(response.data.success){
                if(ctrl.user_data.tipo == 3){
                    ctrl.encuestas = ctrl.encuestas.filter((encuesta)=>encuesta._id!=ctrl.encuesta_d._id);
                    ctrl.tableParams = new NgTableParams({}, { dataset: ctrl.encuestas });
                    ctrl.tableParams.reload();
                }else{
                    ctrl.getEncuestas();
                }              
                ctrl.encuesta_d = {};
                toastr.options = {
                    "positionClass": "toast-top-right",
                    "showDuration": "200",
                };
                toastr.success(response.data.mensaje || response.data.message, "");
            }
          }).catch(function(response){
            console.log(response);
          });
    }

    ctrl.copy = function(encuesta){
        ctrl.encuesta_copy = encuesta;
        $('#modal-copy').modal('show');
    }
    ctrl.copiarEncuesta = function(){
        let newE = {
            cliente : ctrl.encuesta_copy.cliente,
            agencia : ctrl.encuesta_copy.agencia,
            nombre : ctrl.encuesta_copy.nombre,
            preguntas : ctrl.encuesta_copy.preguntas,
        };
        console.log(newE);
        encuestasService.nuevaEncuesta(newE).then(function(response) {
            //console.log(response);
            if(response.data.succes){
                ctrl.encuestas.push(response.data.encuesta);
                ctrl.tableParams = new NgTableParams({}, { dataset: ctrl.encuestas });
                ctrl.tableParams.reload();
                $('#modal-copy').modal('hide');           
                toastr.options = {
                    "positionClass": "toast-top-right",
                    "showDuration": "200",
                };
                toastr.success(response.data.mensaje || response.data.message, "");
            }
        }).catch(function(response){   
            toastr.options = {
            "positionClass": "toast-top-right",
            "showDuration": "200",
            };
            toastr.error(response.data.message, "");
        });
    }
  }
        
    angular
        .module('hicomunity.panel')
        .controller('encuestasCtrl',encuestasCtrl)
 }());
