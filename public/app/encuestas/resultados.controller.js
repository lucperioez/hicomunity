(function(){

     'use strict'
     
  function resultadosCtrl(encuestasService,$state,$scope,$stateParams){
         
    var ctrl = this;
    ctrl.encuesta = {};
    ctrl.graficas_data = {
        genero : {
            masculino : 0,
            femenino : 0
        },
        edad : {
            ['0-18'] : 0,
            ['0-18'] : 0,
            ['19-25'] : 0,
            ['26-45'] : 0,
            ['46+'] : 0
        }
    }
    encuestasService.encuestaResultados($stateParams.id_encuesta).then(function(response){
        console.log(response);
        if(response.data.succes){
            ctrl.encuesta = response.data.encuesta;
            ctrl.encuesta.respuestas.forEach(element => {
                if(element.lead){
                    if(element.lead.sexo.toLowerCase() == 'hombre' || element.lead.sexo.toLowerCase() == 'male' || element.lead.sexo.toLowerCase() == 'masculino'){
                        ctrl.graficas_data.genero.masculino++;
                    }else if(element.lead.sexo.toLowerCase() == 'mujer' || element.lead.sexo.toLowerCase() == 'female' || element.lead.sexo.toLowerCase() == 'femenino'){
                        ctrl.graficas_data.genero.femenino++;
                    }
                    element.lead.edad = Number(element.lead.edad);
                    if(element.lead.edad <= 18){
                        ctrl.graficas_data.edad['0-18']++;
                    }else if(element.lead.edad >= 19 && element.lead.edad <= 25){
                        ctrl.graficas_data.edad['19-25']++;
                    }else if(element.lead.edad >= 26 && element.lead.edad <= 45){
                        ctrl.graficas_data.edad['26-45']++;
                    }else if(element.lead.edad >= 46){
                        ctrl.graficas_data.edad['46+']++;
                    }
                }
            });
            console.log(ctrl.graficas_data);
            ctrl.graficas();
        }
    }).catch(function(response){

    });

    ctrl.graficas = function(){
        // Make monochrome colors
        var pieColors = ['#2980b9','#8e44ad','#e8eaf6'];
                        
        // Build the chart
        Highcharts.chart('container', {
            chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
            },
            title: {
            text: 'GENÉRO',
            style: {
                display: 'none'
            }
            },
            tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                colors: pieColors,
                dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b><br>{point.percentage:.1f} %',
                        distance: -50,
                        filter: {
                        property: 'percentage',
                        operator: '>',
                        value: 4
                        }
                }
            }
            },
            series: [{
            name: 'Share',
            data: [
                { name: 'Hombres', y: ctrl.graficas_data.genero.masculino },
                { name: 'Mujeres', y: ctrl.graficas_data.genero.femenino }
            ]
            }]
        });


        // Build the chart
        Highcharts.chart('container2', {
            chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
            },
            title: {
            text: 'GENÉRO',
            style: {
                display: 'none'
            }
            },
            tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                colors: ['#16a085','#27ae60','#bdc3c7','#8e44ad','#e67e22'],
                dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b><br>{point.percentage:.1f} %',
                        distance: -50,
                        filter: {
                        property: 'percentage',
                        operator: '>',
                        value: 4
                        }
                }
            }
            },
            series: [{
            name: 'Share',
            data: [
                { name: '-18', y:  ctrl.graficas_data.edad['0-18'] || 0 },
                { name: '19 - 25', y: ctrl.graficas_data.edad['19-25'] || 0 },
                { name: '26 - 45', y: ctrl.graficas_data.edad['26-45'] || 0 },
                { name: '46+', y: ctrl.graficas_data.edad['46+'] || 0 },
            ]
            }]
        });
    }
  }
        
    angular
        .module('hicomunity.panel')
        .controller('resultadosCtrl',resultadosCtrl)
 }());
 