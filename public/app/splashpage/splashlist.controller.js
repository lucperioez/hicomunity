(function(){

     'use strict'
     
  function splashlistCtrl($cookies,splashpagesService,usuariosService ){
         
	var ctrl = this;
    ctrl.user_data = $cookies.getObject('usr_dta');
    ctrl.agencias = [];
    ctrl.clientes = [];
    ctrl.usuarios = [];
    ctrl.agencia = '';
    ctrl.cliente = '';
	ctrl.cliente_nombre = '';
	ctrl.admin = ctrl.user_data.tipo == 1;
	


    ctrl.obtener = function(){
        if(ctrl.user_data.tipo == 3){
 
            splashpagesService.obtenerSplashpages(ctrl.user_data._id)
            .then(function(response){
                console.log(response.data);
                ctrl.splashpages = response.data.splashs;
              //ctrl.encuesta = response.data.encuesta;
             
            }).catch(function(response){   
                toastr.options = {
                    "positionClass": "toast-top-right",
                    "showDuration": "200",
                };
                toastr.error(response.data.message, "");
            });
        }else{
            usuariosService.get().then(function(response){
                console.log(response);
                let data = response.data.users;
                ctrl.usuarios = data;
              if(ctrl.user_data.tipo == 2){
                    ctrl.clientes = data;
              }else{  
                    ctrl.agencias = data.filter((user)=>user.tipo==2);
              }
            }).catch(function(response){   
                toastr.options = {
                "positionClass": "toast-top-right",
                "showDuration": "200",
                };
                toastr.error(response.data.message, "");
            });
        }
    }

    ctrl.obtener();

    ctrl.getCliente = function(){
        if(ctrl.agencia != ''){
			ctrl.clientes = ctrl.usuarios.filter((user)=> (user.tipo==3) && (user.agencia._id==ctrl.agencia) );
			ctrl.getSplashpages();
        }else{
            ctrl.splashpages = [];
           
        }
        
	}

	ctrl.getSplashpages = function(){
        if(ctrl.cliente != ''){
            
            ctrl.cliente_nombre = ctrl.clientes.filter((user)=> (user._id==ctrl.cliente))[0].nombre;
            splashpagesService.obtenerSplashpages(ctrl.cliente)
            .then(function(response){
                console.log(response.data);
                ctrl.splashpages = response.data.splashs;
             
            }).catch(function(response){   
                toastr.options = {
                    "positionClass": "toast-top-right",
                    "showDuration": "200",
                };
                toastr.error(response.data.message, "");
            });
        }else{
            ctrl.slashpages = [];
           
        }
    }
	
	ctrl.eliminar = function(id){
		splashpagesService.eliminarSplashpage(id)
			.then(function(){
			toastr.options = {
				"positionClass": "toast-top-right",
				"showDuration": "200",
			};
			toastr.success("El splashpage ha sido eliminado correctamente", "");
			ctrl.getSplashpages();
			

		}).catch(function(response){   
			toastr.options = {
				"positionClass": "toast-top-right",
				"showDuration": "200",
			};
			toastr.error(response.data.message, "");
		});
    }
    
   
  }
        
    angular
        .module('hicomunity.panel')
        .controller('splashlistCtrl',splashlistCtrl);

 }());
 