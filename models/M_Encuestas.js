var mongoose = require("mongoose");
var Schema = mongoose.Schema;

/*  Tipo 
        1 : Texto
        2 : Opcion Multiple
        3 : Check list
*/

/*
    Tipo texto
        Texto
        Numerico
        Alfanumerico
        email 
*/

/* Opcion multiple
    1 : Continuar
    2 : Terminar encuesta
    3 : Ocultar pregunta
*/
var EncuestaSchema = new Schema({
    cliente : {type: Schema.ObjectId, ref: "Usuarios" },
    agencia : {type: Schema.ObjectId, ref: "Usuarios" },
    nombre : String,
    estilo :{
        color_boton:String,
        color_fondo_logo:String,
        color_fondo_pregunta:String,
        color_texto_pregunta:String,
        color_fondo_opciones:String,
        color_texto_opciones:String,
        imagen:String
    },
    preguntas : [
        {
            _id:false,
            numero_pregunta : Number,
            pregunta : String,
            tipo :  Number,
            opciones : [{
                
                _id:false,
                opcion : String,
                accion : { type: Number, default : 1},
                data : [Number]
            }],
            config : {
                max : Number,
                min : Number,
                tipo : String,
                requerido : Boolean
            }
        }
    ],
    respuestas : [
        {
            _id:false,
            mac : String,
            fecha : { type : Date , default : Date.now },
            respuesta : [
                {
                    numero_pregunta : Number,
                    tipo :  Number,
                    opciones : [String]
                }
            ],
            modem : String,
        }
    ]

}, {timestamps: true});

mongoose.model("Encuestas", EncuestaSchema,"Encuestas");