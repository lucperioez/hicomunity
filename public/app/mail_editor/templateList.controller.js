(function(){

     'use strict'
     
  function templateListCtrl($cookies, mailEditorService,usuariosService,$state,$sce,$scope){
         
        var ctrl = this;
        ctrl.user_data = $cookies.getObject('usr_dta');
        ctrl.agencias = [];
        ctrl.clientes = [];
        ctrl.usuarios = [];
        ctrl.agencia = '';
        ctrl.cliente = '';

        ctrl.cliente_nombre = '';
        ctrl.admin = ctrl.user_data.tipo == 1;
        ctrl.sessionData = $cookies.getObject('userData');
        ctrl.comboClientes = "";
        ctrl.flagAgency = 0;
      
        ctrl.clienteId = ctrl.cliente;
        ctrl.obtener = function(){
          if(ctrl.user_data.tipo == 3){
            ctrl.clienteId = ctrl.user_data._id;
              splashpagesService.obtenerSplashpages(ctrl.user_data._id)
              .then(function(response){
     
                  ctrl.splashpages = response.data.splashs;
                //ctrl.encuesta = response.data.encuesta;
               
              }).catch(function(response){   
                  toastr.options = {
                      "positionClass": "toast-top-right",
                      "showDuration": "200",
                  };
                  toastr.error(response.data.message, "");
              });
          }else{
           
            ctrl.clienteId = ctrl.cliente;
              usuariosService.get().then(function(response){
       
                  let data = response.data.users;
                  ctrl.usuarios = data;
                if(ctrl.user_data.tipo == 2){
                      ctrl.clientes = data;
                }else{  
                      ctrl.agencias = data.filter((user)=>user.tipo==2);
                }
              }).catch(function(response){   
                  toastr.options = {
                  "positionClass": "toast-top-right",
                  "showDuration": "200",
                  };
                  toastr.error(response.data.message, "");
              });
          }
      }

      ctrl.obtener();

      ctrl.getCliente = function(){
          if(ctrl.agencia != ''){
        ctrl.clientes = ctrl.usuarios.filter((user)=> (user.tipo==3) && (user.agencia._id==ctrl.agencia) );
        ctrl.getSplashpages();
          }else{
              ctrl.splashpages = [];
             
          }
          
    }

    ctrl.getSplashpages = function(){
      if(ctrl.cliente != ''){
          
          ctrl.cliente_nombre = ctrl.clientes.filter((user)=> (user._id==ctrl.cliente))[0].nombre;
          mailEditorService.obtenerPlantillas(ctrl.cliente)
          .then(function(response){
       
              ctrl.plantillas = response.data.templates;

          
           
          }).catch(function(response){   
              toastr.options = {
                  "positionClass": "toast-top-right",
                  "showDuration": "200",
              };
              toastr.error(response.data.message, "");
          });
      }else{
          ctrl.slashpages = [];
         
      }
  }
  ctrl.deleteTemplate = function(id){
    var r = confirm("¿Seguro de eliminar la plantilla?");
    if (r == true) {
        mailEditorService.deleteData(id)
          .then(function(response){
            toastr.options = {
                "positionClass": "toast-top-right",
                "showDuration": "200",
            };

            toastr.success("Se elimino correctamente la plantilla.", "");
            ctrl.getSplashpages()
          }).catch(function(response){   
              toastr.options = {
                  "positionClass": "toast-top-right",
                  "showDuration": "200",
              };
              toastr.error(response.data.message, "");
          });
    } 
  }

  
}  
    angular
        .module('hicomunity.panel')
        .controller('templateListCtrl',templateListCtrl)

 }());