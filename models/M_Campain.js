var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var CampainSchema = new Schema({
    nombre : String,
    fecha_inicio : Date,
    fecha_fin : Date,
    turno_splash : { type : Number , default : 0 },
    splashs : [{ type: Schema.ObjectId, ref: "SplashPages" }],
    turno_encuesta : { type : Number , default : 0 },
    encuestas : [{ type: Schema.ObjectId, ref: "Encuestas" }],
    splash_actual : { type: Schema.ObjectId, ref: "SplashPages" },
    encuesta_actual : { type: Schema.ObjectId, ref: "Encuestas" },
    folio_modem : String,
    cliente : { type: Schema.ObjectId, ref: "Usuarios" },
    agencia : { type: Schema.ObjectId, ref: "Usuarios" },
}, {timestamps : true});

mongoose.model("Campain", CampainSchema,"Campain");
