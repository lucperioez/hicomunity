(function(){

     'use strict'

     function mailEditorService(config,$http,$cookies){
  
        this.obtenerPlantillas = function(_id){
            
            var token = $cookies.get('tkn') 
            var user_data = $cookies.getObject('usr_dta');  
            var url = '/templates/'+_id;
 
            return $http({
             method: 'GET',
             url: url,
             headers: {
                 'Content-Type': 'application/json',
                 'token' : token
             }    
             });
        }

        this.saveData = function( objectData ){
                    
            var token = $cookies.get('tkn') 
            var user_data = $cookies.getObject('usr_dta');  
            var url = '/template';

            return $http({
                    method: 'POST',
                    url: url,
                    data: objectData,
                    headers: {
                        'Content-Type': 'application/json',
                        'token' : token
                    }
                });
        }
        
        this.getData = function( id ){
                    
            var token = $cookies.get('tkn') 
            var user_data = $cookies.getObject('usr_dta');  
            var url = '/template/'+id;

            return $http({
                    method: 'GET',
                    url: url,
                    headers: {
                        'Content-Type': 'application/json',
                        'token' : token
                    }
                });
        }

        this.updateData = function( objectData ){
                    
            var token = $cookies.get('tkn') 
            var user_data = $cookies.getObject('usr_dta');  
            var url = '/template/'+objectData.id;

            return $http({
                    method: 'PUT',
                    url: url,
                    data: objectData,
                    headers: {
                        'Content-Type': 'application/json',
                        'token' : token
                    }
                });
        }


        this.deleteData = function( id ){
                    
            var token = $cookies.get('tkn') 
        
            var url = '/template/'+id;

            return $http({
                    method: 'DELETE',
                    url:url,
                    headers: {
                        'Content-Type': 'application/json',
                        'token' : token
                    }
                });
        }

      

     }

     angular
        .module('hicomunity.panel')
        .service('mailEditorService', mailEditorService);

 }());