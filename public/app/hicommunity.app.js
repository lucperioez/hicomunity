(function(){

     'use strict'
     

     
     angular.module('hicomunity.panel', [
         'config',
         'ui.router',
         'ngCookies',
         '720kb.datepicker',
         'ngTable',
         'common.fabric',
         'common.fabric.utilities',
         'common.fabric.constants',
         'ngAnimate',
         'ngMaterial',
         'mdColorPicker',
         'rzModule'
     ]);


    angular.module('hicomunity.login', [
        'config',
        'ui.router',
        'ngCookies'
    ]);

    angular.module('hicomunity.splashpage', [
        'config',
        'ui.router'
    ]);

     angular.module('config',[])
            .constant('config',{
                'apiUri': '../../backend/'
            })

 }());