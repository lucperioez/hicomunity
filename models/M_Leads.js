var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var LeadsSchema = new Schema({
    
    identificador : String,
    nombre : String,
    sexo : String,
    edad : String,
    correo : String,
    telefono : String,
    url_imagen : String,
    mac : String,
    codigo_usuario : Number,
    telefono_validado : { type : Boolean , default : false },
    modems : [String],
    clientes : [{
        _id : false,
        cliente : Schema.Types.ObjectId,
        agencia : Schema.Types.ObjectId,
        fecha_registro : { type : Date , default : Date.now }
    }],

}, {timestamps : true});

LeadsSchema.pre('save', function(next) {
    if(!this.codigo_usuario){
        let d = new Date().getTime().toString();
        this.codigo_usuario = Number.parseInt(d.substring(d.length-4, d.length));
    }
    next();
});

mongoose.model("Leads", LeadsSchema,"Leads");
