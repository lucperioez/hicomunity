(function(){

    'use strict'

    function splashService($http){
        
        this.getSplash = function(modem,cliente){

            var url = 'backend/getSplash.php?modem='+modem+'&cliente='+cliente;
            return $http({
                method: 'GET',
                url: url,
                headers: {
                        'Content-Type': 'application/json'
                    }
            });
        }
       
        this.impacto = function(data){
            var url = 'backend/impacto.php';

            return $http({
                method: 'POST',
                url: url,
                data:data,
                headers: {
                        'Content-Type': 'application/json'
                    }
            });
        }
        
        this.conexion = function(data){
            var url = 'backend/conexion.php';

            return $http({
                method: 'POST',
                url: url,
                data:data,
                headers: {
                        'Content-Type': 'application/json'
                    }
            });
        }

        this.lead = function(data){
            var url = 'backend/lead.php';
           
            return $http({
                method: 'POST',
                url: url,
                data:data,
                headers: {
                        'Content-Type': 'application/json'
                    }
            });
        }

        this.addRespuesta = function(id,data){
            data.idEncuesta=id;
            var url = 'backend/addRespuesta.php';
            return $http({
                method: 'POST',
                url: url,
                data:data,
                headers: {
                        'Content-Type': 'application/json',
                        "Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Headers": "X-Requested-With",
                        "Access-Control-Allow-Methods": "GET, POST,PUT"
                    }
            });
        }
        this.getMacBloqued = function(mac){
            var url = 'backend/getMacBloqued.php?mac='+mac;
            return $http({
                method: 'GET',
                url: url,
                headers: {
                        'Content-Type': 'application/json'
                    }
            });
        }
    }

    angular
       .module('hicomunity.splashpage')
       .service('splashService', splashService);

}());