(function(){

     'use strict'
     
  function encuestaCtrl(encuestasService,$state,$scope){
         
      var ctrl = this;
      ctrl.nombre_encuesta = '';
      ctrl.selectedTab = 1;
      ctrl.respuesta = 0;
      ctrl.step = 0;
      ctrl.slider = {
        value: 16,
        options: {
            floor: 16,
            ceil: 20,
        },
    }
        ctrl.edit_data = true;
      encuestasService.obtenerEncuesta()
      .then(function(response){
        ctrl.encuesta = response.data.encuesta;
        
        ctrl.scopeVariable.color = ctrl.encuesta.estilo.color_fondo_logo;
        ctrl.scopeVariable2.color = ctrl.encuesta.estilo.color_fondo_pregunta;
        ctrl.scopeVariable5.color = ctrl.encuesta.estilo.color_texto_pregunta;
        ctrl.scopeVariable3.color = ctrl.encuesta.estilo.color_fondo_opciones; 
        ctrl.scopeVariable4.color = ctrl.encuesta.estilo.color_texto_opciones; 
        ctrl.scopeVariable6.color = ctrl.encuesta.estilo.color_boton; 
        ctrl.encuesta.estilo.imagen = $('#preview-image').attr('src',ctrl.encuesta.estilo.imagen);

        ctrl.nombre_encuesta = ctrl.encuesta.nombre;
        if(ctrl.encuesta.respuestas.length > 0){
            ctrl.edit_data = false; 
            ctrl.selectedTab = 2;
            //window.location.href = '#!/encuestas';
        }
        ctrl.encuesta.preguntas.forEach(p => {
            if(p.tipo == 2){
                p.opciones.forEach(o => {
                    o.respuesta = 0;
                    o.accion = o.accion.toString();
                    for (const i in o.data) {
                        o.data[i] = o.data[i].toString();
                    }
                });
            }
            p.tipo = p.tipo.toString();    
        });
        console.log(ctrl.encuesta);
      })
      .catch(function(response){   
          toastr.options = {
          "positionClass": "toast-top-right",
          "showDuration": "200",
          };
          toastr.error(response.data.message, "");
      });

      
      $scope.file_changed = function(input, $scope) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                 reader.onload = function (e) {
                    
					 $('#preview-image').attr('src',  e.target.result );
					 $('#btn-agregar').show();
                     $('#preview-image:not([src=""])').show();
                  };
                 reader.readAsDataURL(input.files[0]);

            
                }
                
        } ;

      ctrl.modalOption = function(option,pIndex,oIndex){
        if(option == ""){
            ctrl.accion = "insert-option";  
            ctrl.option = "";
            ctrl.preguntaIndex = pIndex;
            ctrl.opcionIndex = oIndex;

          }else{

            ctrl.accion = "update-option";
            ctrl.option = option;
            ctrl.preguntaIndex = pIndex;
            ctrl.opcionIndex = oIndex;

          }
          $('#modal-option').modal();
      }

      ctrl.saveOption = function(){
        toastr.options = {
            "positionClass": "toast-top-right",
            "showDuration": "200",
        };
        if(ctrl.accion == "insert-option"){
            ctrl.encuesta.preguntas[ctrl.preguntaIndex].opciones.push({opcion:ctrl.option,action:'1'});
            toastr.success("La opcion se agrego satisfactoriamente", "");
          }else{
            ctrl.encuesta.preguntas[ctrl.preguntaIndex].opciones[ctrl.opcionIndex].opcion = ctrl.option;
            toastr.success("La opcion se agrego satisfactoriamente", "");
          }
          $('#modal-option').modal('hide');
          
      }

      ctrl.modalAsk = function(ask,pIndex){

        if(ask == ""){
            ctrl.accion = "insert-ask";  
            ctrl.ask = "";
            ctrl.preguntaIndex = pIndex;

          }else{

            ctrl.accion = "update-ask";
            ctrl.ask = ask;
            ctrl.preguntaIndex = pIndex;

          }
          $('#modal-ask').modal();
      }

      ctrl.saveAsk = function(){
        toastr.options = {
            "positionClass": "toast-top-right",
            "showDuration": "200",
        };
        if(ctrl.accion == "insert-ask"){
            ctrl.encuesta.preguntas.push(
                {
                    "config" : {
                        "requerido" : true,
                        "tipo" : "Texto"
                    },
                    "opciones" : [],
                    "numero_pregunta" : 1,
                    "pregunta" : ctrl.ask,
                    "tipo" : "1"
                }
                
            );
            toastr.success("La pregunta se agrego satisfactoriamente", "");
          }else{
            
            ctrl.encuesta.preguntas[ctrl.preguntaIndex].pregunta = ctrl.ask;
            toastr.success("La pregunta se agrego satisfactoriamente", "");
          }
          $('#modal-ask').modal('hide');
          
      }
      ctrl.guardarEncuesta = function(){
        ctrl.checkEncuesta();
        ctrl.encuesta.estilo = {};
        ctrl.encuesta.estilo.color_fondo_logo = ctrl.scopeVariable.color;;
        ctrl.encuesta.estilo.color_fondo_pregunta = ctrl.scopeVariable2.color;
        ctrl.encuesta.estilo.color_texto_pregunta = ctrl.scopeVariable5.color;
        ctrl.encuesta.estilo.color_fondo_opciones = ctrl.scopeVariable3.color;
        ctrl.encuesta.estilo.color_texto_opciones = ctrl.scopeVariable4.color;
        ctrl.encuesta.estilo.color_boton = ctrl.scopeVariable6.color;
        ctrl.encuesta.estilo.imagen = $('#preview-image').attr('src');
        console.log(ctrl.encuesta);
        encuestasService.editarEncuesta(ctrl.encuesta).then(function(response){
            console.log(response);
            if(response.data.success){
                toastr.options = {
                    "positionClass": "toast-top-right",
                    "showDuration": "200",
                };
                toastr.success(response.data.message, "");   
                //$state.go('encuestas'); 
            }
        }).catch(function(response){   
            toastr.options = {
            "positionClass": "toast-top-right",
            "showDuration": "200",
            };
            toastr.error(response.data.message, "");
        });
      }
      
      ctrl.cambiarNombre = function(){
        ctrl.encuesta.nombre = ctrl.nombre_encuesta;
      }

      ctrl.checkEncuesta = function(){
        ctrl.encuesta.preguntas.forEach((p,index) => {
            if(p.tipo == "1"){
                p.opciones = [];
            }else if(p.tipo == "2"){
                p.config.max = undefined;
                p.config.min = undefined;
                p.config.tipo = undefined;
            }else if(p.tipo == "3"){
                p.config.max = undefined;
                p.config.min = undefined;
                p.config.tipo = undefined;
            }
            p.numero_pregunta = (index+1);
        });
      }
      ctrl.deleteOpcion = function(preguntasIndex,opcionesIndex){
        ctrl.encuesta.preguntas[preguntasIndex].opciones.splice(opcionesIndex, 1);
      }
      ctrl.moveUpPregunta = function(preguntasIndex){
          let actual = angular.copy(ctrl.encuesta.preguntas[preguntasIndex])
          let nuevo = angular.copy(ctrl.encuesta.preguntas[preguntasIndex-1])
          ctrl.encuesta.preguntas[preguntasIndex-1] = actual;
          ctrl.encuesta.preguntas[preguntasIndex] = nuevo;
      }

      ctrl.moveDownPregunta = function(preguntasIndex){
        let actual = angular.copy(ctrl.encuesta.preguntas[preguntasIndex])
        let nuevo = angular.copy(ctrl.encuesta.preguntas[preguntasIndex+1])
        ctrl.encuesta.preguntas[preguntasIndex+1] = actual;
        ctrl.encuesta.preguntas[preguntasIndex] = nuevo;
      }
      ctrl.scopeVariable = {};
      ctrl.scopeVariable.options = {
		label: "Elige el color",
		icon: "brush",
		genericPalette: false,
		history: false
	};
	ctrl.scopeVariable.color ="rgb(4,100,103)";
    ctrl.scopeVariable2 = {};
    ctrl.scopeVariable2.options = {
      label: "Elige el color",
      icon: "brush",
      genericPalette: false,
      history: false
    };
    ctrl.scopeVariable2.color ="rgb(40,100,10)";

    ctrl.scopeVariable3 = {};
    ctrl.scopeVariable3.options = {
        label: "Elige el color",
        icon: "brush",
        genericPalette: false,
        history: false
    };
    ctrl.scopeVariable3.color ="rgb(40,40,40)";

    ctrl.scopeVariable4 = {};
    
    ctrl.scopeVariable4.options = {
        label: "Elige el color",
        icon: "brush",
        genericPalette: false,
        history: false
    };
    ctrl.scopeVariable4.color ="rgb(40,40,40)";

    ctrl.scopeVariable5 = {};
    ctrl.scopeVariable5.options = {
        label: "Elige el color",
        icon: "brush",
        genericPalette: false,
        history: false
    };
    ctrl.scopeVariable5.color ="rgb(40,40,40)";

    ctrl.scopeVariable6 = {};
    ctrl.scopeVariable6.options = {
        label: "Elige el color",
        icon: "brush",
        genericPalette: false,
        history: false
    };
    ctrl.scopeVariable6.color ="rgb(40,40,40)";

    ctrl.accion = function(){

        if(ctrl.respuesta == 1){
            ctrl.step ++;
        }else if(ctrl.respuesta == 2){
            ctrl.step = ctrl.encuesta.preguntas.length;
        }
    }
  }
        
    angular
        .module('hicomunity.panel')
        .controller('encuestaCtrl',encuestaCtrl)
 }());
 