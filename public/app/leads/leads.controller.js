(function(){

     'use strict'
     
  function leadsCtrl(leadsService,NgTableParams,$scope,modemsService,$cookies){
         
      
      var ctrl = this;
      ctrl.user_data = $cookies.getObject('usr_dta');
      ctrl.plazas = {};
      ctrl.plaza = '';

      ctrl.admin = ctrl.user_data.tipo == 1;

      if(ctrl.admin){
        //atraer todos los modems asignados
        modemsService.obtenerModemsTipo('admin').then(function(response){
            console.log(response);
            ctrl.plazas = response.data.modems.filter((m)=>m.usuario!=undefined);
        }).catch(function(response){   
                toastr.options = {
                "positionClass": "toast-top-right",
                "showDuration": "200",
                };
                toastr.error(response.data.message, "");
        });
      }else if(ctrl.user_data.tipo == 2){
        //todos los modems con agencia
        modemsService.obtenerModemsTipo('agencia',ctrl.user_data._id).then(function(response){
            console.log(response);
            ctrl.plazas = response.data.modems;
        }).catch(function(response){   
                toastr.options = {
                "positionClass": "toast-top-right",
                "showDuration": "200",
                };
                toastr.error(response.data.message, "");
        });
      }else if(ctrl.user_data.tipo == 3){
        //todos los modems con agencia
        modemsService.obtenerModemsTipo('cliente',ctrl.user_data._id).then(function(response){
            console.log(response);
            ctrl.plazas = response.data.modems;
        }).catch(function(response){   
                toastr.options = {
                "positionClass": "toast-top-right",
                "showDuration": "200",
                };
                toastr.error(response.data.message, "");
        });
      }
      ctrl.getLeads = function(){
        leadsService.getByModem(ctrl.plaza).then(function(response){
            console.log(response);
            if(response.data.succes){
                
                ctrl.tableParams = new NgTableParams({}, { dataset:  response.data.leads});
            }
        }).catch(function(response){   
            toastr.options = {
            "positionClass": "toast-top-right",
            "showDuration": "200",
            };
            toastr.error(response.data.message, "");
        });
      }
      ctrl.checkSexo = function(s){
          if(s == 'male'){
            return 'Masculino';
          }else if(s == 'female'){
            return 'Femenino';
          }
          return s;
      }
  }
        
    angular
        .module('hicomunity.panel')
        .controller('leadsCtrl',leadsCtrl)
 }());
 